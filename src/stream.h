/*******************************************************************************
 * PiBox service daemon
 *
 * stream.h:  Start and stop video streams.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef STREAM_H
#define STREAM_H

/*
 * ========================================================================
 * Typedefs
 * =======================================================================
 */
#define S_WEBCAM        "webcam"

/* 1920x1280 */
#define S_FULLRES       "input_uvc.so -d /dev/video0 -r FHD -f 33"

/* 1280x1024 */
#define S_HIRES         "input_uvc.so -d /dev/video0 -r SXGA -f 33"

/* 640x480 */
#define S_MEDRES        "input_uvc.so -d /dev/video0 -r VGA -f 25"

/* 320x200 */
#define S_LOWRES        "input_uvc.so -d /dev/video0 -r CGA -f 15"

/*
 * ========================================================================
 * Globals
 * =======================================================================
 */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef STREAM_C
extern pid_t    spawnStream( char *filename, int msgAction );
extern void     killStream( pid_t pid );
extern void     killStreams( void );
#endif /* STREAM_C */

#endif /* !STREAM_H */
