/*******************************************************************************
 * PiBox service daemon
 *
 * queueProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define QUEUEPROCESSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include <uuid/uuid.h>
#include <pibox/utils.h>
#include <pibox/parson.h>
#include <pibox/pmsg.h>

#include "piboxd.h"

static int queueIsRunning = 0;
static pthread_mutex_t queueProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t queueProcessorThread;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Message Type Handlers
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   handleStream
 * Prototype:  int handleStream( PIBOX_MSG_T * )
 *
 * Description:
 * Process a request to start or stop a video stream.
 *========================================================================*/
static void
handleStream( PIBOX_MSG_T *msg )
{
    int msgAction = -1;
    int timeout = -1;
    pid_t procID = -1;
    PIBOX_MSG_T *procMsg = NULL;

    // Safety check.
    if ( msg == NULL )
    {
        piboxLogger(LOG_ERROR, "No msg provided for stream.  Dropping request.\n");
        freeMsgNode( msg );
        return;
    }

    // What action should be taken?
    msgAction = (msg->header>>8) & 0x000000ff;

    // Error check the action field.
    switch(msgAction)
    {
        case MA_START:
        case MA_START_LOW:
        case MA_START_MED:
        case MA_START_FULL:
        case MA_END:
            break;
        default:
            piboxLogger(LOG_ERROR, "Invalid stream action.  Dropping request.\n");
            freeMsgNode( msg );
            return;
    }

    // Select a timeout based on which file is being streamed
    if ( msg->payload == NULL )
    {
        piboxLogger(LOG_ERROR, "No filename provided for stream.  Dropping request.\n");
        freeMsgNode( msg );
        return;
    }
    else if ( strcasecmp(msg->payload, "webcam") == 0 )
    {
        if ( cliOptions.timeout != STREAM_TO )
            timeout = cliOptions.timeout;
        else
            timeout = 60*10;
    }
    else
        timeout = getVideoLength(msg->payload);
    if ( timeout == -1 )
    {
        piboxLogger(LOG_ERROR, "Can't set timeout for stream.  Dropping request.\n");
        freeMsgNode( msg );
        return;
    }

    switch(msgAction)
    {
        case MA_START:
        case MA_START_LOW:
        case MA_START_MED:
        case MA_START_FULL:

            /* Test if we're already running a stream at the same resolution. */
            procMsg = popWebcamProcQueue();
            if ( procMsg != NULL )
            {
                /* Kill the running stream. */
                killStream(procMsg->pid);
                piboxLogger(LOG_INFO, "Killed stream process: %d\n", (int)procMsg->pid);
                freeMsgNode(procMsg);
                sleep(3);
            }

            // Start the stream, if possible.
            procID = spawnStream(msg->payload, msgAction);
            if ( procID != -1 )
            {
                msg->pid = procID;
                msg->active = 1;
                msg->timestamp = time( NULL );
                msg->timeout = timeout;
                piboxLogger(LOG_INFO, "Started stream process: %d\n", (int)msg->pid);

                // Save the message
                piboxLogger(LOG_INFO, "Queueing processed msg: procID = %d\n", procID);
                queueProc( msg );
            }
            else
            {
                // If we didn't start it, we might need to just reset the timeout
                if ( (strcmp(msg->payload, S_WEBCAM) == 0) && (getProcQueueByFilename(msg->payload) != NULL) )
                {
                    // Repeated webcam requests will reset the timestamp so it plays for another 10 minutes.
                    setWebcamTimestamp( time( NULL ) );
                }
                piboxLogger(LOG_INFO, "Failed msg processing for %s. Stream not started.\n", msg->payload);
                freeMsgNode( msg );
            }
            break;

        case MA_END:
            procMsg = popWebcamProcQueue();
            if ( procMsg != NULL )
            {
                killStream(procMsg->pid);
                piboxLogger(LOG_INFO, "Killed stream process: %d\n", (int)procMsg->pid);
                freeMsgNode(procMsg);
            }
            else
                piboxLogger(LOG_ERROR, "No webcam stream is running.\n");

            // Inbound message doesn't end up on process queue, so clean it up here.
            freeMsgNode( msg );
            break;

        default:
            piboxLogger(LOG_ERROR, "Unknown stream action: %08x\n", msgAction);
            freeMsgNode( msg );
            break;
    }
}

/*========================================================================
 * Name:   handleHeartbeat
 * Prototype:  int handleHeartbeat( PIBOX_MSG_T * )
 *
 * Description:
 * Update heartbeat time for a running process.
 *========================================================================*/
static void
handleHeartbeat( PIBOX_MSG_T *msg )
{
    time_t timestamp = (time_t) 0;

    // Safety check.
    if ( msg == NULL )
        return;

    // Get timestamp
    timestamp = time( NULL );
    if ( timestamp == -1 )
    {
        piboxLogger(LOG_ERROR, "Failed to get a timestamp: %s\n", strerror(errno));
        return;
    }

    // Update matching process message.
    setProcQueueTimestamp(msg->tag, timestamp);

    // Cleanup 
    freeMsgNode( msg );
}

/*========================================================================
 * Name:   handleNet
 * Prototype:  int handleNet( PIBOX_MSG_T * )
 *
 * Description:
 * Process a get or set request related to network configuration.
 *========================================================================*/
static void
handleNet( PIBOX_MSG_T *msg )
{
    int msgAction = -1;
    int msgSubAction = -1;

    // Safety check.
    if ( msg == NULL )
        return;

    // What action should be taken?
    msgAction = (msg->header>>8) & 0x000000ff;

    switch(msgAction)
    {
        case MA_GETIFLIST:
            getIFList(msg);
            break;
        case MA_GETIF:
            getIF(msg);
            break;
        case MA_GETMAC:
            getMAC(msg);
            break;
        case MA_GETIP:
            getIP(msg);
            break;
        case MA_GETDNS:
            getDNS(msg);
            break;
        case MA_GETNETTYPE:
            getNetType(msg);
            break;
        case MA_SETNETTYPE:
            setNetType(msg);
            break;
        case MA_GETAP:
            getAP(msg);
            break;
        case MA_GETWIRELESS:
            getWireless(msg);
            break;
        case MA_SETIPV4:
            setIPV4(msg);
            break;
        case MA_SETAP:
            setAP(msg);
            break;
        case MA_SETWIRELESS:
            setWireless(msg);
            break;
        case MA_MFLIST:
            msgSubAction = (msg->header>>16) & 0x000000ff;
            switch(msgSubAction)
            {
                case MS_GET:
                    getMACList(msg);
                    break;
                case MS_SET:
                    setMACList(msg);
                    break;
            }
            break;
        case MA_RESTART:
            networkRestart();
            break;
        default:
            piboxLogger(LOG_ERROR, "Unknown action type: %d - ignoring message.\n", msgAction);
            break;
    }

    // If insd is not -1, then we need to close the connection.
    if ( msg->insd != -1 )
        close (msg->insd);

    // Cleanup 
    freeMsgNode( msg );
}

/*========================================================================
 * Name:   handlePW
 * Prototype:  int handlePW( PIBOX_MSG_T * )
 *
 * Description:
 * Handle a web service user password update.
 *========================================================================*/
static void
handlePW( PIBOX_MSG_T *msg )
{
    int msgAction = -1;
    char *buf = NULL;
    char *newbuf = NULL;
    char *username, *pw;
    char *saveptr = NULL;
    char *tok;
    char *ptr;
    char line[128];
    int length;
    int bytes;
    int status;
    FILE *fd;

    piboxLogger(LOG_INFO, "Entered handlePW.\n");

    // Safety check.
    if ( msg == NULL )
        return;

    // What action should be taken?
    msgAction = (msg->header>>8) & 0x000000ff;

    if ( msgAction == MA_SAVE )
    {
        // Check for the payload
        if ( msg->payload == NULL )
        {
            piboxLogger(LOG_ERROR, "No username data provided.  Dropping SAVE request.\n");
            goto handlePWcleanup;
        }

        piboxLogger(LOG_INFO, "Saving PW.\n");
        username = strtok_r(msg->payload, ":", &saveptr);
        pw = strtok_r(NULL, ":", &saveptr);
        if ( (username == NULL) || (pw == NULL) )
        {
            piboxLogger(LOG_ERROR, "Incomplete username data provided.  Dropping SAVE request.\n");
            goto handlePWcleanup;
        }
    
        // Update the web service database
        length = strlen(MK_PASSWD) + strlen(username) + strlen(pw) + strlen(REALM) + strlen(PW_F) + 1;
        buf = (char *)malloc(length);
        memset(buf, 0, length);
        sprintf(buf, MK_PASSWD, username, pw, REALM, PW_F);
        piboxLogger(LOG_INFO, "PW update command: %s\n", buf);
        status = system(buf);
        free(buf);
        if( WEXITSTATUS(status) != 0 )
        {
            piboxLogger(LOG_ERROR, "Failed to update database.  Dropping SAVE request.\n");
            goto handlePWcleanup;
        }
    
        // Restart the web service - is this necessary?
        length = strlen(WEBSVC) + 1;
        buf = (char *)malloc(length);
        memset(buf, 0, length);
        sprintf(buf, WEBSVC);
        piboxLogger(LOG_INFO, "Web service restart command: %s\n", buf);
        status = system(buf);
        free(buf);
        if( WEXITSTATUS(status) != 0 )
            piboxLogger(LOG_ERROR, "Failed to restart web service.\n");

        if ( strcasecmp(username, "admin") == 0 )
        {
            // Create the stamp file saying we've updated the admin user at least once.
            fd = fopen(PWSTAMP_F, "w");
            if ( fd != NULL )
                fclose(fd);
            else
                piboxLogger(LOG_ERROR, "Failed to create PW stamp file: %s\n", strerror(errno));
        }
    }
    else if ( msgAction == MA_GET )
    {
        piboxLogger(LOG_INFO, "Getting users.\n");
        fd = fopen(PW_F, "r");
        if ( fd == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to open password file: %s - %s\n", PW_F, strerror(errno));
            goto handlePWcleanup;
        }

        // Parse the file to build a list of usernames separated by newlines.
        memset(line, 0, 128);
        while( fgets(line, 127, fd) != NULL )
        {
            // Ignore comments
            if ( line[0] == '#' )
                continue;

            // Strip leading white space
            ptr = line;
            ptr = piboxTrim(ptr);

            // Ignore blank lines
            if ( strlen(ptr) == 0 )
                continue;

            // Strip newline
            piboxStripNewline(ptr);

            // Grab username
            tok = strtok_r(ptr, ":", &saveptr);

            if ( buf == NULL )
            {
                length = strlen(tok) + 2;
                buf = (char *)malloc(length);
                memset(buf, 0, length);
                sprintf(buf, "%s\n", tok);
            }
            else
            {
                length = strlen(buf) + strlen(tok) + 2;
                newbuf = (char *)malloc(length);
                memset(newbuf, 0, length);
                sprintf(newbuf, "%s%s\n", buf,tok);
                free(buf);
                buf = newbuf;
            }
            piboxLogger(LOG_INFO, "Current user list: \n%s\n", buf);
        }
        fclose(fd);

        // Default to no users
        if ( buf == NULL )
        {
            buf = (char *)malloc(1);
            memset(buf, 0, 1);
        }

        // Retrieve users from database and return to caller.
        if ( msg->insd != -1 )
        {   
            bytes = write( msg->insd, (char *)buf, strlen(buf) );
            if ( bytes != strlen(buf) )
                piboxLogger(LOG_ERROR, "write to socket failed.\n");
        }
        else
            piboxLogger(LOG_INFO, "Skipping write to socket.\n");

        if ( buf != NULL )
            free(buf);
    }
    else if ( msgAction == MA_DEL )
    {
        // Check for the payload
        if ( msg->payload == NULL )
        {
            piboxLogger(LOG_ERROR, "No username data provided.  Dropping delete request.\n");
            goto handlePWcleanup;
        }
        piboxLogger(LOG_INFO, "Deleting user.\n");

        // We cheat here - use sed to do the dirty work.
        buf = (char *)malloc( strlen(PWSED) + strlen(msg->payload) + strlen(PW_F));
        memset( buf, 0, strlen(PWSED) + strlen(msg->payload) + strlen(PW_F));
        sprintf(buf, PWSED, msg->payload, PW_F);
        system(buf);
        free(buf);

        // Restart the web service - is this necessary?
        length = strlen(WEBSVC) + 1;
        buf = (char *)malloc(length);
        memset(buf, 0, length);
        sprintf(buf, WEBSVC);
        piboxLogger(LOG_INFO, "Web service restart command: %s\n", buf);
        status = system(buf);
        free(buf);
        if( WEXITSTATUS(status) != 0 )
            piboxLogger(LOG_ERROR, "Failed to restart web service.\n");
    }

handlePWcleanup:

    // If insd is not -1, then we need to close the connection.
    if ( msg->insd != -1 )
        close (msg->insd);

    // Cleanup 
    freeMsgNode( msg );
}

/*========================================================================
 * Name:   handleMulticast
 * Prototype:  int handleMulticast( PIBOX_MSG_T * )
 *
 * Description:
 * Process Multicast message from IoT devices.
 *========================================================================*/
static void
handleMulticast( PIBOX_MSG_T *msg )
{
    int                 sockfd;
    struct addrinfo     hints, *serverinfo;
    char                *ipaddr;
    char                port[5];;
    char                buf[128];;
    int                 rv, n;
    int                 msgAction = -1;

    // Safety check.
    if ( msg == NULL )
        return;

    // What action should be taken?
    msgAction = (msg->header>>8) & 0x000000ff;
    piboxLogger(LOG_INFO, "Multicast action: %d\n", msgAction);

    switch(msgAction)
    {
        case MA_PAIR_JARVIS:
            piboxLogger(LOG_INFO, "Multicast Jarvis Pair\n");

            // Get IP of sender
            ipaddr = msg->tag;
            piboxLogger(LOG_INFO, "Multicast server address: %s\n", ipaddr);

            // Get Port from message (msg->payload cast to int)
            sprintf(port, "%d", *((int *)msg->payload));
            piboxLogger(LOG_INFO, "Multicast server port: %s\n", port);

            // Get our description (from imwww data dir)
            // TBD:  this needs to change when imwww adds descriptor field.
            sprintf(buf, "Unknown location");

            /* Setup client connection to original sender */
            memset(&hints, 0, sizeof hints);
            hints.ai_family = AF_INET;
            hints.ai_socktype = SOCK_DGRAM;

            if ((rv = getaddrinfo(ipaddr, port, &hints, &serverinfo)) != 0)
            {
                piboxLogger(LOG_ERROR, "getaddrinfo: %s\n", gai_strerror(rv));
                break;
            }

            if ((sockfd = socket(serverinfo->ai_family,
                                 serverinfo->ai_socktype,
                                 serverinfo->ai_protocol)) == -1)
            {
                piboxLogger(LOG_ERROR, "socket failure: %s\n", strerror(errno));
                break;
            }

            /* Send the message to the server */
            n = sendto(sockfd, buf, strlen(buf), 0, serverinfo->ai_addr, serverinfo->ai_addrlen);
            if (n < 0)
            {
                piboxLogger(LOG_ERROR, "failure writing to socket: %s\n", strerror(errno));
            }
            piboxLogger(LOG_INFO, "Sent ACK to Jarvis\n");

            // Close connection to the remote end.
            close(sockfd);

            // Cleanup
            freeaddrinfo(serverinfo);
            break;

        case MA_PAIR_IOT:
            piboxLogger(LOG_INFO, "Multicast IoT Pair\n");
            piboxLogger(LOG_INFO, "header: %08x\n", msg->header);
            piboxLogger(LOG_INFO, "ipaddr: %s\n", msg->tag);
            registerDevice(msg);
            break;

        default:
            piboxLogger(LOG_INFO, "Multicast unknown\n");
            break;
    }

    // Cleanup
    freeMsgNode( msg );
}

/*========================================================================
 * Name:   handleSys
 * Prototype:  int handleSys( PIBOX_MSG_T * )
 *
 * Description:
 * Process messages related to system operation.
 *========================================================================*/
static void
handleSys( PIBOX_MSG_T *msg )
{
    int     bytes;
    char    buf[256];
    int     rc;
    guint   resultSize=0;
    int     msgAction = -1;

    // Safety check.
    if ( msg == NULL )
        return;

    // What action should be taken?
    msgAction = (msg->header>>8) & 0x000000ff;

    switch(msgAction)
    {
        case MA_REBOOT:
            reboot();
            break;
        case MA_KEYB:
            keyboard();
            break;
        case MA_ROTATE:
            displayRotate();
            break;
        default:
            piboxLogger(LOG_ERROR, "Unknown MT_SYS action: %d.\n", msgAction);
            break;
    }

    // Cleanup
    freeMsgNode( msg );
}

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isQueueProcessorRunning
 * Prototype:  int isQueueProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of queueIsRunning variable.
 *========================================================================*/
static int
isQueueProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &queueProcessorMutex );
    status = queueIsRunning;
    pthread_mutex_unlock( &queueProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setQueueProcessorRunning
 * Prototype:  int setQueueProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of queueIsRunning variable.
 *========================================================================*/
static void
setQueueProcessorRunning( int val )
{
    pthread_mutex_lock( &queueProcessorMutex );
    queueIsRunning = val;
    pthread_mutex_unlock( &queueProcessorMutex );
}

/*========================================================================
 * Name:   queueProcessor
 * Prototype:  void queueProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from queue and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * Sleep between empty reads defaults to 100 milliseconds.
 *========================================================================*/
static void *
queueProcessor( void *arg )
{
    PIBOX_MSG_T *msg = NULL;
    int msgType = -1;

    /* Spin, looking for messages to process. */
    setQueueProcessorRunning(1);
    while( isCLIFlagSet(CLI_SERVER) )
    {
        /* Block waiting for at least one message to be added to the queue. */
        sem_wait(&queueSem);

        /* 
         * A posted semaphore is used to break out of this loop.
         * Double check if we're being told to exit.
         */
        if( ! isCLIFlagSet(CLI_SERVER) )
            break;

        /* Grab a message. */
        if ( (msg = popMsgQueue()) != NULL )
        {
            // Check the flags
            if ( (msg->flags & PIBOX_MULTICAST) == PIBOX_MULTICAST )
            {
                // Route message to multicast handler.
                piboxLogger(LOG_INFO, "Processing multicast message.\n");
                handleMulticast(msg);
                continue;
            }

            // Get the type
            msgType = msg->header & 0x000000ff;
            piboxLogger(LOG_TRACE1, "msgType: 0x%08x\n", msgType);

            // Process the message.
            switch(msgType)
            {
                case MT_STREAM:
                    if ( cliOptions.flags & CLI_STREAM )
                    {
                        handleStream(msg);
                    }
                    else
                    {
                        freeMsgNode( msg );
                    }
                    break;

                case MT_HEARTBEAT:
                    handleHeartbeat(msg);
                    break;

                case MT_NET:
                    handleNet(msg);
                    break;

                case MT_PW:
                    handlePW(msg);
                    break;

                case MT_SYS:
                    handleSys(msg);
                    break;

                default:
                    piboxLogger(LOG_ERROR, "Unknown message type: \n", msgType);
                    freeMsgNode( msg );
                    break;
            }
        }
    }

    piboxLogger(LOG_INFO, "%s: Processor thread is exiting.\n", PROG);
    setQueueProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startQueueProcessor
 * Prototype:  void startQueueProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownQueueProcessor().
 *========================================================================*/
void
startQueueProcessor( void )
{
    int rc;

    /* Set up the semaphore used to process inbound messages. */
    if (sem_init(&queueSem, 0, 0) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to get queue processor semaphore: %s\n", strerror(errno));
        return;
    }

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&queueProcessorThread, NULL, &queueProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create processor thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started process thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownQueueProcessor
 * Prototype:  void shutdownQueueProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownQueueProcessor( void )
{
    int timeOut = 1;

    /* Wake sleeping processor, if necessary. */
    if ( sem_post(&queueSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post queueSem semaphore.\n");

    while ( isQueueProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_INFO, "%s: Timed out waiting on processor thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(queueProcessorThread);

    /* Clean up the semaphore used for this loop. */
    sem_destroy(&queueSem);

    piboxLogger(LOG_INFO, "%s: queueProcessor has shut down.\n", PROG);
}

