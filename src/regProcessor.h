/*******************************************************************************
 * PiBox service daemon
 *
 * regProcessor.h:  Process inbound multicast registration messages 
 *                  from IoT devices
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef REGPROCESSOR_H
#define REGPROCESSOR_H

#include <pthread.h>

#define MULTICAST_IP            "234.1.42.3"
#define MULTICAST_PORT          13911

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef REGPROCESSOR_C

extern pthread_mutex_t regMutex;

#endif /* !REGPROCESSOR_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef REGPROCESSOR_C

extern void startRegProcessor( void );
extern void shutdownRegProcessor( void );

#endif /* !REGPROCESSOR_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !REGPROCESSOR_H */
