/*******************************************************************************
 * PiBox service daemon
 *
 * eth.c:  Wake on eth plug events to enable/disable ports.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define ETH_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/select.h>
#include <sys/timerfd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <uuid/uuid.h>

#include "piboxd.h"

static int ethIsRunning = 0;
static int ethIsEnabled = 0;
static pthread_mutex_t ethProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t ethProcessorThread;
static int timerfd = -1;
static int lastState  = -1;         // Last port state for carrier detect.

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isEthEnabled
 * Prototype:  int isEthEnabled( void )
 *
 * Description:
 * Thread-safe read of ethIsEnabled variable.
 *========================================================================*/
static int
isEthEnabled( void )
{
    int status;
    pthread_mutex_lock( &ethProcessorMutex );
    status = ethIsEnabled;
    pthread_mutex_unlock( &ethProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setEthEnabled
 * Prototype:  int setEthEnabled( int value )
 *
 * Description:
 * Thread-safe set of ethIsEnabled variable.
 *========================================================================*/
static void
setEthEnabled( int value )
{
    pthread_mutex_lock( &ethProcessorMutex );
    ethIsEnabled = value;
    pthread_mutex_unlock( &ethProcessorMutex );
}

/*========================================================================
 * Name:   isEthProcessorRunning
 * Prototype:  int isEthProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of ethIsRunning variable.
 *========================================================================*/
static int
isEthProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &ethProcessorMutex );
    status = ethIsRunning;
    pthread_mutex_unlock( &ethProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setEthProcessorRunning
 * Prototype:  int setEthProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of ethIsRunning variable.
 *========================================================================*/
static void
setEthProcessorRunning( int val )
{
    pthread_mutex_lock( &ethProcessorMutex );
    ethIsRunning = val;
    pthread_mutex_unlock( &ethProcessorMutex );
}

/*
 * ========================================================================
 * Name:   timerSetup
 * Prototype:  void timerSetup( void )
 *
 * Description:
 * Setup timer to watch for carrier state changes.
 *
 * Notes:
 * Select() on timerfd to know when to run the processor.
 * ========================================================================
 */
static void
timerSetup()
{
    /* Expire timer every two seconds. */
    struct itimerspec timer = {
        {2,0},
        {2,0}
    };

    /* Start the timer. */
    timerfd = timerfd_create(CLOCK_REALTIME, 0);
    if (timerfd == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to create timer fd: %s\n", strerror(errno));
        return;
    }
    piboxLogger(LOG_TRACE1, "Created timer.\n");

    if (timerfd_settime(timerfd, 0, &timer, NULL) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to setup timer: %s\n", strerror(errno));
    }
    piboxLogger(LOG_INFO, "Initialized timer.\n");
}

/*
 * ========================================================================
 * Name:   timerTeardown
 * Prototype:  void timerTeardown( void )
 *
 * Description:
 * Teardown timer that handles watcher.
 * ========================================================================
 */
static void
timerTeardown()
{
    struct itimerspec timer;

    if ( timerfd == -1 )
        return;

    /* Clear timer configuration. */
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_nsec = 0;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_nsec = 0;

    if (timerfd_settime(timerfd, 0, &timer, NULL) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to teardown timer: %s\n", strerror(errno));
    }

    timerfd = -1;
}

/*
 * ========================================================================
 * Name:   getPortValue
 * Prototype:  void getPortValue( char *path )
 *
 * Description:
 * Read a numeric value, maximum of 8 digits, from the specified file.
 * ========================================================================
 */
int 
getPortValue ( char *path )
{
    int         fd;
    char        buf[16];
    int         value;

    fd = open(path, O_RDONLY);
    if (fd < 0)
    {
        piboxLogger(LOG_ERROR, "open failed on %s: %s\n", path, strerror(errno));
        return -1;
    }

    memset(buf, 0, 16);
    read(fd, buf, 15);
    close(fd);
    if ( strlen(buf) == 0 )
    {
        piboxLogger(LOG_ERROR, "Failed to read anything from %s\n", path);
        return -1;
    }
    // piboxLogger(LOG_INFO, "Port value: %s\n", buf);
    value = atoi(buf);
    return value;
}

/*
 * ========================================================================
 * Name:   getPortPID
 * Prototype:  void getPortPID( void )
 *
 * Description:
 * Read PID of udhcpd process managing the port.
 *
 * Returns
 * -1 if the value can't be read.
 * >0 if the value is a PID.
 * ========================================================================
 */
int 
getPortPID ( void )
{
    return getPortValue(ETH0_STATE_PID);
}

/*
 * ========================================================================
 * Name:   getPortState
 * Prototype:  void getPortState( void )
 *
 * Description:
 * Read initial state of port.
 *
 * Returns
 * -1 if the value can't be read.
 * 0 if the carrier is not detected on the port.
 * 1 if the carrier is detected on the port.
 * ========================================================================
 */
int 
getPortState ( void )
{
    return getPortValue(ETH0_STATE_PATH);
}

/*
 * ========================================================================
 * Name:   handlePortEvents
 * Prototype:  void handlePortEvents( void )
 *
 * Description:
 * Identify plug events on eth0 and manage network accordingly.
 * ========================================================================
 */
void 
handlePortEvents ( void )
{
    int         carrierDetect;
    int         pid;

    /* Read ETH0_STATE_PATH */
    carrierDetect = getPortState();
    if ( carrierDetect == -1 )
    {
        piboxLogger(LOG_TRACE1, "Error reading port state.\n");
        return;
    }

    if ( carrierDetect == lastState )
    {
        piboxLogger(LOG_TRACE1, "No port state change.\n");
        return;
    }

    /* Don't update lastState unless we've moved to a state we handle. */
    if ( carrierDetect )
    {
        /* Port is up: start dhcp client on eth0 */
        piboxLogger(LOG_INFO, "Carrier detected on eth0\n");
        pid = getPortPID();
        if (pid != -1 )
        {
            piboxLogger(LOG_INFO, "Killing PID %d\n", pid);
            kill((pid_t)pid, SIGINT);
            unlink(ETH0_STATE_PID);
        }
        system( DHCP_ETH0 );
        lastState = carrierDetect;
    }

    else if ( !carrierDetect )
    {
        /* Post is down: kill dhcp client on eth0 */
        piboxLogger(LOG_INFO, "Carrier lost on eth0\n");
        pid = getPortPID();
        if (pid != -1 )
        {
            piboxLogger(LOG_INFO, "Killing PID %d\n", pid);
            kill((pid_t)pid, SIGINT);
            unlink(ETH0_STATE_PID);
        }
        lastState = carrierDetect;
    }
    else
        piboxLogger(LOG_ERROR, "Invalid carrier state: %d\n", carrierDetect);
}

/*========================================================================
 * Name:   ethProcessor
 * Prototype:  void ethProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from eth and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * This loop runs every 100ms but only runs the queue every 3 seconds.
 * This allows the thread to exit quickly when the daemon is shutting down
 * while giving remote users plenty of time to send heartbeats for their streams.
 *========================================================================*/
static void *
ethProcessor( void *arg )
{
    fd_set          rfds;
    struct timeval  tv;
    int             retval;
    char            buf[8];

    piboxLogger(LOG_TRACE1, "Entered.\n");

    /* Initialize port state. */
    lastState = getPortState();
    if ( lastState == -1 )
    {
        piboxLogger(LOG_TRACE1, "Error reading port state.\n");
        return(0);
    }

    /* Create a timer to run the processor. */
    timerSetup();
    if ( timerfd == -1 )
    {
        piboxLogger(LOG_ERROR, "timerd is not set, can't run timerProcessor.\n");
        return(0);
    }

    setEthProcessorRunning(1);
    setEthEnabled(1);
    while( isEthEnabled() )
    {
        /* Watch the timer fd for activity. */
        FD_ZERO(&rfds);
        FD_SET(timerfd, &rfds);

        /*
         * Timer will spawn this, but we add a timeout just in case.
         */
        tv.tv_sec = 3;
        tv.tv_usec = 5;
        retval = select(timerfd+1, &rfds, NULL, NULL, &tv);

        /* Ignore select errors or timer not expired. */
        if ( retval < 0 )
        {
            piboxLogger(LOG_TRACE5, "select: %s\n", strerror(errno));
            continue;
        }

        /* Clear the timer */
        read(timerfd, buf, 8);

        /* Check for changes. */
        handlePortEvents();
    }

    piboxLogger(LOG_INFO, "%s: Eth thread is exiting.\n", PROG);
    setEthProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startEthProcessor
 * Prototype:  void startEthProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownEthProcessor().
 *========================================================================*/
void
startEthProcessor( void )
{
    int rc;

    piboxLogger(LOG_INFO, "Starting eth.\n");

    /* Create a thread to expire streams. */
    rc = pthread_create(&ethProcessorThread, NULL, &ethProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create eth thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }

    piboxLogger(LOG_INFO, "%s: Started eth thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownEthProcessor
 * Prototype:  void shutdownEthProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownEthProcessor( void )
{
    int timeOut = 1;

    timerTeardown();
    setEthEnabled(0);
    while ( isEthProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_WARN, "%s: Timed out waiting on eth thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(ethProcessorThread);
    piboxLogger(LOG_INFO, "%s: ethProcessor shut down.\n", PROG);
}
