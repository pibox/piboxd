/*******************************************************************************
 * PiBox service daemon
 *
 * msgQueue.h:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MSGQUEUE_H
#define MSGQUEUE_H

/*
 * ========================================================================
 * Typedefs
 * =======================================================================
 */

/*
 * ========================================================================
 * Globals
 * =======================================================================
 */
#ifdef MSGQUEUE_C
pthread_mutex_t queueMutex = PTHREAD_MUTEX_INITIALIZER;
#else
extern pthread_mutex_t queueMutex;
#endif /* MSGQUEUE_C */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef MSGQUEUE_C
extern int         queueSize( int );
extern void        freeMsgNode( PIBOX_MSG_T * );
extern void        setProcQueueTimestamp( char *tag, time_t timestamp );
extern void        setWebcamTimestamp( time_t timestamp );
extern PIBOX_MSG_T *popWebcamProcQueue( void );
extern PIBOX_MSG_T *getProcQueue( char *tag );
extern PIBOX_MSG_T *popProcQueueByTag( char *tag );
extern PIBOX_MSG_T *popProcQueue( void );
extern PIBOX_MSG_T *getProcQueueByFilename( char *filename );
extern PIBOX_MSG_T *popMsgQueue( void );
extern void        queueMsg( int, int, char *, char *, int, int );
extern void        queueProc( PIBOX_MSG_T *msg );
extern void        clearMsgQueue( void );
extern void        expireProcQueue( void );
#endif /* MSGQUEUE_C */

#endif /* !MSGQUEUE_H */
