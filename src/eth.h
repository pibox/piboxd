/*******************************************************************************
 * PiBox service daemon
 *
 * eth.c:  Process ethd messages (re: start/stop processes).
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef ETH_H
#define ETH_H

#include <pthread.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/
#define ETH0_STATE_PATH   "/sys/class/net/eth0/carrier"
#define ETH0_STATE_PID    "/var/run/udhcpc.eth0.pid"
#define DHCP_ETH0         "udhcpc -R -n -p /var/run/udhcpc.eth0.pid -i eth0" // Same as ifup

/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef ETH_C

extern pthread_mutex_t ethMutex;

#endif /* !ETH_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef ETH_C

extern void startEthProcessor( void );
extern void shutdownEthProcessor( void );

#endif /* !ETH_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !ETH_H */
