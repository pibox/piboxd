/*******************************************************************************
 * PiBox service daemon
 *
 * msgQueue.c:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGQUEUE_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "piboxd.h"

// The head of the message queues.
PIBOX_MSG_T *inQueue = NULL;    // Inbound messages, yet to be processed.
PIBOX_MSG_T *procQueue = NULL;  // Processed messages, but not yet expired.

/*
 *========================================================================
 *========================================================================
 *
 * Queue Management
 *
 *========================================================================
 *========================================================================
 */

/*========================================================================
 * Name:   queueSize
 * Prototype:  int queueSize( int )
 *
 * Description:
 * Retrieve the current size of the queue.
 *
 * Arguments:
 * int queueID      Either Q_IN or Q_PROC
 *
 * Returns:
 * The size of the queue.
 *========================================================================*/
int
queueSize( int queueID )
{
    int             count = 0;
    PIBOX_MSG_T     *ptr  = NULL;

    pthread_mutex_lock( &queueMutex );
    if ( queueID == Q_IN )
        ptr  = inQueue;
    else
        ptr  = procQueue;
    while ( ptr!=NULL )
    {
        count++;
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );
    return count;
}

/*========================================================================
 * Name:   freeMsgNode
 * Prototype:  void freeMsgNode( PIBOX_MSG_T * )
 *
 * Description:
 * Free a message queue node.  It should already have been popped from its queue.
 *
 * Input Arguments:
 * PIBOX_MSG_T *node     Node to free.
 *========================================================================*/
void
freeMsgNode( PIBOX_MSG_T *node )
{
    if ( node == NULL )
        return;
    if ( node->payload != NULL )
        free(node->payload);
    free(node);
}

/*========================================================================
 * Name:   setProcQueueTimestamp
 * Prototype:  void setProcQueueTimestamp( char * )
 *
 * Description:
 * Update the timestamp for the message specified by the tag, if possible. 
 *
 * Input Arguments:
 * char *tag             The tag to search for in the list.
 *========================================================================*/
void
setProcQueueTimestamp( char *tag, time_t timestamp )
{
    PIBOX_MSG_T    *ptr = NULL;
    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( ptr!=NULL )
    {
        if ( (ptr->tag != NULL) && (strcmp(ptr->tag, tag) == 0) )
            break;
        ptr = ptr->next;
    }

    if ( ptr != NULL )
    {
        piboxLogger(LOG_INFO, "Set timestamp for: %s\n", tag);
        ptr->timestamp = timestamp;
    }
    else
        piboxLogger(LOG_ERROR, "Failed to set timestamp, no such tag: %s\n", tag);
    pthread_mutex_unlock( &queueMutex );
}

/*========================================================================
 * Name:   setWebcamTimestamp
 * Prototype:  void setWebcamTimestamp( time_t timestamp )
 *
 * Description:
 * Update the timestamp for the message associated with the webcam.
 *========================================================================*/
void
setWebcamTimestamp( time_t timestamp )
{
    PIBOX_MSG_T    *ptr = NULL;
    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( ptr!=NULL )
    {
        if ( (ptr->payload != NULL) && (strcasecmp(ptr->payload, "webcam") == 0) )
            break;
        ptr = ptr->next;
    }

    if ( ptr != NULL )
    {
        piboxLogger(LOG_INFO, "Set timestamp for: webcam\n");
        ptr->timestamp = timestamp;
    }
    else
        piboxLogger(LOG_ERROR, "Failed to set timestamp for webcam.\n");
    pthread_mutex_unlock( &queueMutex );
}

/*========================================================================
 * Name:   popWebcamProcQueue
 * Prototype:  PIBOX_MSG_T *popWebcamProcQueue( void )
 *
 * Description:
 * Retrieve the webcam queue entry, if any.
 *
 * Notes:
 * There can only be 1.
 *========================================================================*/
PIBOX_MSG_T *
popWebcamProcQueue( )
{
    PIBOX_MSG_T    *last = NULL, *ptr = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( ptr!=NULL )
    {
        if ( (ptr->payload != NULL) && (strcasecmp(ptr->payload, "webcam") == 0) )
            break;
        last = ptr;
        ptr = ptr->next;
    }

    /* If found, remove entry from queue. */
    if ( ptr != NULL )
    {
        if ( ptr == procQueue )
            procQueue = ptr->next;
        else 
            last->next = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );

    /* This is either a msg or NULL */
    return (ptr);
}

/*========================================================================
 * Name:   expireProcQueue
 * Prototype:  void expireProcQueue( void )
 *
 * Description:
 * Run through the process queue and expire streams, if necessary.
 *========================================================================*/
void
expireProcQueue( void )
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *next = NULL;
    PIBOX_MSG_T    *last = NULL;
    time_t          currentTime = 0;
    time_t          expiredTime = 0;
    time_t          timeout = -1;

    piboxLogger(LOG_TRACE1, "Entered.\n");

    // Get current time.
    currentTime = time( NULL );

    if ( procQueue==NULL )
    {
        piboxLogger(LOG_TRACE1, "Queue is empty.\n");
        return;
    }

    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( ptr!=NULL )
    {
        expiredTime = currentTime - ptr->timestamp;
        next = ptr->next;

        // Select which timeout to use.
        if ( ptr->timeout != -1 )
        {
            piboxLogger(LOG_INFO, "Using queue entry timeout\n");
            timeout = ptr->timeout;
        }
        else
        {
            piboxLogger(LOG_INFO, "Using default timeout\n");
            timeout = cliOptions.timeout;
        }

        piboxLogger(LOG_INFO, "expiredTime vs timeout: %d / %d\n", expiredTime, timeout);
        if ( expiredTime > timeout )
        {
            piboxLogger(LOG_INFO, "Expiring stream: %s\n", ptr->tag);
            // Remove it
            if ( ptr == procQueue )
                procQueue = next;
            else 
                last->next = next;
            freeMsgNode( ptr );
        }
        else
            last = ptr;
        ptr = next;
    }
    pthread_mutex_unlock( &queueMutex );
}

/*========================================================================
 * Name:   getProcQueue
 * Prototype:  PIBOX_MSG_T *getProcQueue( char * )
 *
 * Description:
 * Pull the specified message from the processes queue, if possible.  The node is not
 * removed from the queue.
 *
 * Input Arguments:
 * char *tag             The tag to search for in the list.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.
 *========================================================================*/
PIBOX_MSG_T *
getProcQueue( char *tag )
{
    PIBOX_MSG_T    *ptr = NULL;
    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( ptr!=NULL )
    {
        if ( (ptr->tag != NULL) && (strcmp(ptr->tag, tag) == 0) )
            break;
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );

    return ( ptr );
}

/*========================================================================
 * Name:   getProcQueueByFilename
 * Prototype:  PIBOX_MSG_T *getProcQueueByFilename( char * )
 *
 * Description:
 * Pull the specified message from the processes queue based on filename, if possible.  The node is not
 * removed from the queue.
 *
 * Input Arguments:
 * char *filename             The filename to search for in the list.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.
 *========================================================================*/
PIBOX_MSG_T *
getProcQueueByFilename( char *filename )
{
    PIBOX_MSG_T    *ptr = NULL;
    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( ptr!=NULL )
    {
        piboxLogger(LOG_TRACE2, "payload / filename: %s / %s\n", ptr->payload, filename);
        if ( (ptr->payload != NULL) && (strcmp(ptr->payload, filename) == 0) )
            break;
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );

    return ( ptr );
}

/*========================================================================
 * Name:   popProcQueueByTag
 * Prototype:  PIBOX_MSG_T *popProcQueueByTag( char * )
 *
 * Description:
 * Pull the specified message by tag from the processes queue, if possible.  The node is 
 * removed from the queue.
 *
 * Input Arguments:
 * char *tag             The tag to search for in the list.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.
 *========================================================================*/
PIBOX_MSG_T *
popProcQueueByTag( char *tag )
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *last = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while ( (ptr!=NULL) && (strcmp(ptr->tag, tag) != 0) )
    {
        last = ptr;
        ptr = ptr->next;
    }
    if ( ptr != NULL )
    {
        if ( ptr == procQueue )
            procQueue = ptr->next;
        else 
            last->next = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );
    return ( ptr );
}

/*========================================================================
 * Name:   popProcQueue
 * Prototype:  PIBOX_MSG_T *popProcQueue( void )
 *
 * Description:
 * Pull the top message from the processes queue, if possible.  The node is 
 * removed from the queue.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.
 *========================================================================*/
PIBOX_MSG_T *
popProcQueue( void )
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *next = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    if ( ptr != NULL )
        next = ptr->next;
    procQueue = next;
    pthread_mutex_unlock( &queueMutex );
    return ( ptr );
}

/*========================================================================
 * Name:   popMsgQueue
 * Prototype:  PIBOX_MSG_T *popMsgQueue( void )
 *
 * Description:
 * Pull the next message from the inbound queue, if possible.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.  If found, caller is responsible
 * for freeing the node using freeMsgNode().
 *========================================================================*/
PIBOX_MSG_T *
popMsgQueue( void )
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *next = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    if ( ptr != NULL )
        next = ptr->next;
    inQueue = next;
    pthread_mutex_unlock( &queueMutex );
    return ( ptr );
}

/*========================================================================
 * Name:   queueMsg
 * Prototype:  void queueMsg( int, int, char *, char *)
 *
 * Description:
 * Queue a message for processing.
 * The new message goes on the end of the list.
 *
 * Input Arguments:
 * int queueID           Either Q_IN or Q_PROC
 * int header            Header for the message, which defines action, type, etc.
 * int size              Size of the payload.
 * char *tag             The unique tag for this message.
 * char *payload         Payload pulled from message.
 * int  insd             Socket that we're reading and/or writing.
 * int  flags            Control flags for packets.
 *========================================================================*/
void 
queueMsg( int header, int size, char *tag, char *payload, int insd, int flags)
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *last = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    while (ptr != NULL)
    {
        last = ptr;
        ptr = ptr->next;
    }
    ptr = (PIBOX_MSG_T *)malloc(sizeof(PIBOX_MSG_T));
    ptr->next = NULL;
    ptr->header = header;
    ptr->size = size;
    ptr->insd = insd;
    memset(ptr->tag, 0, 37);
    if ( tag!=NULL )
    {
        strncpy(ptr->tag, tag, 36);
    }
    ptr->payload = payload;
    ptr->flags = flags;

    if ( last == NULL )
        inQueue = ptr;
    else
        last->next = ptr;

    pthread_mutex_unlock( &queueMutex );
    piboxLogger(LOG_INFO, "Added to inbound queue, new size: %d\n", queueSize(Q_IN));

    /* Wake the processor */
    if ( sem_post(&queueSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post queueSem semaphore.\n");
}

/*========================================================================
 * Name:   queueProc
 * Prototype:  void queueProc( PIBOX_MSG_T * )
 *
 * Description:
 * Queue a message after processing.  This queue is used to track running processes.
 * The new message goes on the end of the list.
 *
 * Input Arguments:
 * PIBOX_MSG_T *msg        The message that caused a process to be run.
 *========================================================================*/
void 
queueProc( PIBOX_MSG_T *msg)
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *last = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = procQueue;
    while (ptr != NULL)
    {
        last = ptr;
        ptr = ptr->next;
    }
    if ( last == NULL )
        procQueue = msg;
    else
        last->next = msg;

    // Apply the timestamp
    msg->timestamp = time( NULL );

    // Reset any next pointer remaining from the inbound queue.
    msg->next = NULL;
    pthread_mutex_unlock( &queueMutex );
    piboxLogger(LOG_INFO, "Process queue size: %d\n", queueSize(Q_PROC));
}

/*========================================================================
 * Name:   clearQueue
 * Prototype:  clearQueue( void )
 *
 * Description:
 * Clear the inbound message queue.
 *========================================================================*/
void
clearMsgQueue( void )
{
    PIBOX_MSG_T *msg = NULL;
    while ( (msg=popMsgQueue()) != NULL )
        freeMsgNode(msg);
}

