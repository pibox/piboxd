/*******************************************************************************
 * PiBox service daemon
 *
 * smb.c:  Handler automount/unmount of smb shares
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define SMB_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <piboxnet/pnc.h>
#include <pibox/utils.h>

#include "piboxd.h"

// Default mount command
// #define MOUNTCMD    "mount -t cifs //%s/media /media/smb/%s -o username=guest,guest,sec=ntlm,vers=1.0"
#define MOUNTCMD    "mount -t cifs //%s/media /media/smb/%s -o username=guest,guest"

static int smbIsRunning = 0;
static int smbEnabled = 0;
static pthread_mutex_t smbProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t smbCondMutex      = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  smbCond           = PTHREAD_COND_INITIALIZER;
static pthread_t smbProcessorThread;

GSList *mounts = NULL;
GSList *nmb = NULL;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Local functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*
 *========================================================================
 * Name:   isMounted
 * Prototype:  gint isMounted( gconstpointer, gconstpointer )
 *
 * Description:
 * Test if a mount point contains a specific IP address.
 *========================================================================
 */
gint 
isMounted( gconstpointer item, gconstpointer user_data )
{
    if ( strstr( (char *)item, (char *)user_data) != NULL )
        return 0;
    return 1;
}

/*
 *========================================================================
 * Name:   isAvailable
 * Prototype:  gint isAvailable( gconstpointer, gconstpointer )
 *
 * Description:
 * Test a mount point has a matching IP address.
 *========================================================================
 */
gint 
isAvailable( gconstpointer item, gconstpointer user_data )
{
    if ( strstr( (char *)user_data, (char *)item) != NULL )
        return 0;
    return 1;
}

/*========================================================================
 * Name:   mountIt
 * Prototype:  void mountIt( gpointer, gpointer )
 *
 * Description:
 * Mount new SMB hosts. This iterates over the list of nmblookup results
 * (re: IP Addresses) and checks if the IP is already mounted.  If not
 * it is mounted.
 *========================================================================*/
void 
mountIt( gpointer item, gpointer user_data )
{
    char *cmd = NULL;
    char buf[1024];
    GSList *ptr = NULL;

    if ( cliOptions.smbMount == NULL )
        cmd = MOUNTCMD;
    else
        cmd = cliOptions.smbMount;

    // Look for the current item in the current mount list.
    piboxLogger(LOG_INFO, "Looking for %s in mounts\n", (char *)item);
    ptr = g_slist_find_custom(mounts, (gconstpointer)item, (GCompareFunc)isMounted);

    // Not there?  Mount it.
    if ( ptr == NULL )
    {
        // try to mount it - cheap and quick but no check on failure
        piboxLogger(LOG_INFO, "Mounting /media/smb/%s\n", item);
        memset(buf, 0, 1024);
        sprintf(buf, "/media/smb/%s", (char *)item);
        mkdir(buf, 0755);

        memset(buf, 0, 1024);
        sprintf(buf, cmd, (char *)item, (char *)item);
        system( buf );
    }
    else
        piboxLogger(LOG_INFO, "Found a match in mounts\n");
}

/*========================================================================
 * Name:   unmountIt
 * Prototype:  void unmountIt( gpointer, gpointer )
 *
 * Description:
 * Unmount SMB shares.  This runs through the list of mount points and
 * checks if the mount point's IP address is in the list of addresses
 * reported by the nmblookup.
 *========================================================================*/
void 
unmountIt( gpointer item, gpointer user_data )
{
    char *cmd = "umount %s";
    char buf[1024];
    GSList *ptr = NULL;

    // Look for the current item in the current mount list.
    piboxLogger(LOG_INFO, "Looking for %s in published SMB shares\n", (char *)item);
    ptr = g_slist_find_custom(nmb, (gconstpointer)item, (GCompareFunc)isAvailable);

    // Not there?  Unmount it.
    if ( ptr == NULL )
    {
        // try to unmount it - cheap and quick but no check on failure
        piboxLogger(LOG_INFO, "Not found - unmounting %s\n", (char *)item);
        memset(buf, 0, 1024);
        sprintf(buf, cmd, (char *)item);
        system( buf );
        rmdir( (char *)item );
    }
}

/*========================================================================
 * Name:   unmountAll
 * Prototype:  void unmountAll( gpointer, gpointer )
 *
 * Description:
 * Unmount all SMB shares.  This runs through the list of mount points and
 * unmounts any that are found.
 *========================================================================*/
void 
unmountAll( gpointer item, gpointer user_data )
{
    char *cmd = "umount %s";
    char buf[1024];

    // try to unmount it - cheap and quick but no check on failure
    piboxLogger(LOG_INFO, "Unmounting %s\n", (char *)item);
    memset(buf, 0, 1024);
    sprintf(buf, cmd, (char *)item);
    system( buf );
    rmdir( (char *)item );
}

/*========================================================================
 * Name:   printList
 * Prototype:  void printList( gpointer, gpointer )
 *
 * Description:
 * Print a current list.
 *========================================================================*/
void 
printList( gpointer item, gpointer user_data )
{
    if ( user_data )
        piboxLogger(LOG_INFO, "SMB shares: %s\n", (char *)item);
    else
        piboxLogger(LOG_INFO, "SMB mount: %s\n", (char *)item);
}

/*========================================================================
 * Name:   freeList
 * Prototype:  void freeList( gpointer, gpointer )
 *
 * Description:
 * Print a current list.
 *========================================================================*/
void 
freeList( gpointer item, gpointer user_data )
{
    if ( item != NULL )
        g_free(item);
}

/*========================================================================
 * Name:   getMountList
 * Prototype:  int getMountList( void )
 *
 * Description:
 * Retrieve a list of mount points under /media/smb
 *========================================================================*/
static void
getMountList( void )
{
    FILE    *pd;
    char    readbuf[1024];
    char    *dup;

    // It's a cheat but its quick.
    pd = popen("cat /proc/mounts | cut -f2 -d\" \"", "r");

    while( fgets(readbuf, 1024, pd) )
    {
        if ( strncmp(readbuf, "/media/smb", 10) == 0 )
        {
            piboxStripNewline(readbuf);
            dup = g_strdup( readbuf );
            piboxLogger(LOG_INFO, "getMountList: %s\n", dup);
            mounts = g_slist_append(mounts, dup);
        }
    }
    pclose(pd);
}

/*========================================================================
 * Name:   getNMBList
 * Prototype:  int getNMBList( void )
 *
 * Description:
 * Thread-safe read of smbIsRunning variable.
 *========================================================================*/
static void
getNMBList( void )
{
    FILE    *pd;
    char    readbuf[1024];
    char    *dup;
    int     head = 1;
    GSList  *ptr;

    // It's a cheat but its quick.
    pd = popen("nmblookup -O \"IPTOS_LOWDELAY,TCP_NODELAY\" 'PIBOXGROUP' | cut -f1 -d\" \"", "r");

    while( fgets(readbuf, 1024, pd) )
    {
        piboxStripNewline(readbuf);

        // Skip first line.
        if ( head )
        {
            head = 0;
            continue;
        }

        // Verify this isn't our IP - if so, skip it.
        if ( isThisMyIP( readbuf ) )
            continue;

        ptr = g_slist_find_custom(nmb, (gconstpointer)readbuf, (GCompareFunc)isMounted);
        if ( ptr == NULL )
        {
            dup = g_strdup( readbuf );
            piboxLogger(LOG_INFO, "getNMBList: %s\n", dup);
            nmb = g_slist_append(nmb, dup);
        }
    }
    pclose(pd);
}

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isSMBProcessorRunning
 * Prototype:  int isSMBProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of smbIsRunning variable.
 *========================================================================*/
static int
isSMBProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &smbProcessorMutex );
    status = smbIsRunning;
    pthread_mutex_unlock( &smbProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setSMBProcessorRunning
 * Prototype:  int setSMBProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of smbIsRunning variable.
 *========================================================================*/
static void
setSMBProcessorRunning( int val )
{
    pthread_mutex_lock( &smbProcessorMutex );
    smbIsRunning = val;
    pthread_mutex_unlock( &smbProcessorMutex );
}

/*========================================================================
 * Name:   smbProcessor
 * Prototype:  void smbProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from smb and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * This loop runs every 100ms but only runs the queue every 3 seconds.
 * This allows the thread to exit quickly when the daemon is shutting down
 * while giving remote users plenty of time to send heartbeats for their streams.
 *========================================================================*/
static void *
smbProcessor( void *arg )
{
    struct stat stat_buf;
    struct dirent *d;
    DIR *dir;
    char buf[270];

    // Create mount point parent directories if needed
    if ( stat( "/media", &stat_buf) != 0 )
        mkdir("/media", 0755);
    else if ( stat( "/media/smb", &stat_buf) != 0 )
        mkdir("/media/smb", 0755);

    // Remove any empty directories - these may be from previous runs of piboxd.
    dir = opendir("/media/smb");
    if (dir == NULL)
    {
        piboxLogger(LOG_ERROR, "%s is missing.  Can't run SMB manager.\n", "/media/smb");
        setSMBProcessorRunning(0);
        return(0);
    }
    /* Try to remove any entries.  rmdir() will fail on non-empty or already mounted directories. */
    while ((d = readdir(dir)) != NULL)
    {
        if ( *d->d_name == '.')
            continue;
        sprintf(buf, "/media/smb/%s", d->d_name);
        piboxLogger(LOG_INFO, "Removing %s.\n", buf);
        rmdir(buf);
    }
    closedir(dir);

    /* Run the queue periodically to expire abandoned streams. */
    setSMBProcessorRunning(1);
    while( isCLIFlagSet(CLI_SERVER) )
    {
        /* Wait for scheduled timer to wake us. */
        pthread_mutex_lock( &smbCondMutex );
        while ( smbEnabled == 0 )
        {   
            pthread_cond_wait( &smbCond, &smbCondMutex );
        }
        smbEnabled = 0;
        pthread_mutex_unlock( &smbCondMutex );

        /* Make sure we didn't get disabled while we were waiting to be signaled. */
        if ( ! isCLIFlagSet(CLI_SERVER) )
            continue;

        // Get current mount list
        piboxLogger(LOG_TRACE1, "Calling getMountList\n");
        getMountList();
        g_slist_foreach(mounts, printList, NULL);

        // Run nmblookup to get new list
        // Run it multiple times with a slight delay to deal with
        // Network hiccups.
        piboxLogger(LOG_TRACE1, "Calling getNMBList\n");
        getNMBList();
        usleep(1000000);
        getNMBList();
        usleep(1000000);
        getNMBList();
        g_slist_foreach(nmb, printList, (gpointer)1);

        // If new list not in current list, mount it.
        piboxLogger(LOG_TRACE1, "Calling mountIt\n");
        g_slist_foreach(nmb, mountIt, NULL);

        // If current list not in new list, unmount it.
        piboxLogger(LOG_TRACE1, "Calling unmountIt\n");
        g_slist_foreach(mounts, unmountIt, NULL);

        // Clean up;
        piboxLogger(LOG_TRACE1, "cleanup\n");
        g_slist_foreach(nmb, freeList, NULL);
        g_slist_free(nmb);
        nmb = NULL;
        g_slist_foreach(mounts, freeList, NULL);
        g_slist_free(mounts);
        mounts = NULL;
        piboxLogger(LOG_TRACE1, "done.\n");
    }

    piboxLogger(LOG_INFO, "%s: SMB thread is exiting.\n", PROG);
    setSMBProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startSMBProcessor
 * Prototype:  void startSMBProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownSMBProcessor().
 *========================================================================*/
void
startSMBProcessor( void )
{
    int rc;

    /* Create a thread to expire streams. */
    rc = pthread_create(&smbProcessorThread, NULL, &smbProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create smb thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started smb thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   wakeSMBProcessor
 * Prototype:  void wakeSMBProcessor( void )
 *
 * Description:
 * Wakes up the waiting processor loop.
 *========================================================================*/
void
wakeSMBProcessor()
{
    /* Wake the processor */
    pthread_mutex_lock( &smbCondMutex );
    smbEnabled = 1;
    pthread_cond_signal( &smbCond );
    pthread_mutex_unlock( &smbCondMutex );
}

/*========================================================================
 * Name:   shutdownSMBProcessor
 * Prototype:  void shutdownSMBProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownSMBProcessor( void )
{
    int timeOut = 1;

    /* Wake the waiting SMB processor. */
    wakeSMBProcessor();

    while ( isSMBProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_INFO, "%s: Timed out waiting on smb thread to shut down.\n", PROG);
            return;
        }
    }

    // Unmount any smb mounts
    getMountList();
    g_slist_foreach(mounts, unmountAll, NULL);
    g_slist_foreach(mounts, freeList, NULL);
    g_slist_free(mounts);
    mounts = NULL;

    pthread_detach(smbProcessorThread);
    piboxLogger(LOG_INFO, "%s: smbProcessor shut down.\n", PROG);
}

/*========================================================================
 * Name:   enableSMB
 * Prototype:  void enableSMB( int )
 *
 * Description:
 * Enable SMB, if possible.
 *========================================================================*/
void
enableSMB( int sig )
{
    /* If we're currently processing, skip this iteration. */
    if ( smbEnabled == 1 )
    {
        return;
    }
    wakeSMBProcessor();
}

