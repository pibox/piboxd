/*******************************************************************************
 * PiBox service daemon
 *
 * timer.c:  Process timerd messages (re: start/stop processes).
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef TIMER_H
#define TIMER_H

#include <pthread.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef TIMER_C

extern pthread_mutex_t timerMutex;

#endif /* !TIMER_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef TIMER_C

extern void startTimerProcessor( void );
extern void shutdownTimerProcessor( void );

#endif /* !TIMER_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !TIMER_H */
