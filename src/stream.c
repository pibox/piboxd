/*******************************************************************************
 * PiBox service daemon
 *
 * stream.c:  Start and stop video streams.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define STREAM_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pibox/pmsg.h>

#include "piboxd.h"

/*========================================================================
 * Name:   spawnStream
 * Prototype:  int spawnStream( char *filename, int msgAction )
 *
 * Description:
 * Start an stream for the named file.
 * Currently only supports webcam source.
 *
 * Returns:
 * The process id of the started stream or -1 if the stream cannot be started.
 *========================================================================*/
pid_t
spawnStream( char *filename, int msgAction )
{
    pid_t   childPid;
    char    *args[64];
    char    *str;
    int     idx, i, size;
    char    *ptr, *buf;

    // Arguments to launch mjpg-streamer for the webcam.  These are only used if the "webcam:" option 
    // is NOT specified in the /etc/piboxd.cfg file.
    // mjpg_streamer -i "input_uvc.so -d /dev/video0 -r 640x480 -f 10" -o "output_http.so -p 9090"
    // See https://support.brightcove.com/optimal-video-dimensions
    char *webcamArgs[] = {
        "mjpg_streamer",
        "-i", NULL,
        "-o", "output_http.so -p 9090",
        (char *)NULL
        };
    char *input_uvc = NULL;

    // Safety check
    if ( filename == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing filename, can't spawn stream.\n");
        return -1;
    }

    // Currently only support streaming the webcam.
    if ( strcmp(filename, S_WEBCAM) != 0 )
    {
        piboxLogger(LOG_ERROR, "Unsupported file stream: %s\n", filename);
        return -1;
    }

    // Don't allow starting more than one webcam stream at the same resolution.
    piboxLogger(LOG_TRACE1, "filename: %s - S_WEBCAM: %s\n", filename, S_WEBCAM);
    if ( (strcmp(filename, S_WEBCAM) == 0) && (getProcQueueByFilename(filename) != NULL) )
    {
        piboxLogger(LOG_ERROR, "A webcam stream is already running.\n");
        return -1;
    }

    // Create command line that will run the process.
    piboxLogger(LOG_INFO, "Starting stream for %s.\n", filename);
    childPid = fork();
    if ( childPid == 0 )
    {   
        if ( cliOptions.webcam == NULL )
        {
            // Choose the hi/low resolution.
            if ( msgAction == MA_START_LOW )
            {
                piboxLogger(LOG_INFO, "Setting up low-res webcam.\n");
                input_uvc = S_LOWRES;
            }
            else if ( msgAction == MA_START_MED )
            {
                piboxLogger(LOG_INFO, "Setting up medium-res webcam.\n");
                input_uvc = S_MEDRES;
            }
            else if ( msgAction == MA_START_FULL )
            {
                piboxLogger(LOG_INFO, "Setting up full-res webcam.\n");
                input_uvc = S_FULLRES;
            }
            else
            {
                piboxLogger(LOG_INFO, "Setting up hi-res webcam.\n");
                input_uvc = S_HIRES;
            }
            webcamArgs[2] = input_uvc;

            /* Log the command we're about to run. */
            size=0;
            for(i=0; webcamArgs[i]!=NULL; i++)
                size += strlen(webcamArgs[i]) + 1;
            buf=(char *)calloc(1,size+1);
            ptr=buf;
            for(i=0; webcamArgs[i]!=NULL; i++)
            {
                strcpy(ptr, webcamArgs[i]);
                ptr += strlen(webcamArgs[i]);
                strcpy(ptr, " ");
                ptr++;
            }
            piboxLogger(LOG_INFO, "Webcam command: %s\n", buf);
            free(buf);

            // Child: start an mjpg-streamer session
            execvp("mjpg_streamer", webcamArgs);
            fprintf(stderr, "Failed to spawn mjpg-streamer stream for %s\n", filename);
            exit(1);
        }
        else
        {
            piboxLogger(LOG_INFO, "Webcam preset: %s\n", cliOptions.webcam);
            str = g_strdup(cliOptions.webcam);
            idx=parse(str, args, 64);
            for(i=0; i<idx; i++)
            {
                piboxLogger(LOG_INFO, "arg[%d]: %s\n", i, args[i]);
            }

            execvp(args[0], args);
            piboxLogger(LOG_ERROR, "Failed to spawn webcam cmd: %s: reason = %s\n", 
                cliOptions.webcam, strerror(errno));
            exit(1);
        }
    }

    return childPid;
}

/*========================================================================
 * Name:   killStream
 * Prototype:  int killStream( pid_t pid)
 *
 * Description:
 * Kill an ffmpeg stream based on the process ID and reap the child process.
 *========================================================================*/
void
killStream( pid_t pid )
{
    int status;
    kill(pid, SIGTERM);
    pid = waitpid( pid, &status, 0);
    if ( WIFEXITED(status) )
        piboxLogger(LOG_INFO, "Stream exited normally with rc %d\n", WEXITSTATUS(status));
    else
        piboxLogger(LOG_INFO, "Stream exited abnormally with rc %d\n", WEXITSTATUS(status));
}

/*========================================================================
 * Name:   killStreams
 * Prototype:  killStreams( void )
 *
 * Description:
 * Kill all active streams, reap child processes and clean out process queue.
 *========================================================================*/
void
killStreams( void )
{
    PIBOX_MSG_T *msg = NULL;
    while ( (msg=popProcQueue()) != NULL )
    {
        killStream(msg->pid);
        freeMsgNode(msg);
    }
}

