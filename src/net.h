/*******************************************************************************
 * piboxd
 *
 * net.h:  Functions for reading data files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define NET_H

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

#define NETSVC          "/etc/init.d/S40network restart"
#define HOSTAPACCEPT_F  "/etc/hostapd.accept"
#define HOSTAPACCEPT_FD "data/hostapd.accept"
#define WPA_RELOAD      "wpa_cli -i wlan0 reconfigure"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef NET_C
extern void getIFList( PIBOX_MSG_T *msg );
extern void getIF( PIBOX_MSG_T *msg );
extern void getMAC( PIBOX_MSG_T *msg );
extern void getIP( PIBOX_MSG_T *msg );
extern void buildDNSString( gpointer item, gpointer user_data );
extern void getDNS( PIBOX_MSG_T *msg );
extern void getNetType( PIBOX_MSG_T *msg );
extern void setNetType( PIBOX_MSG_T *msg );
extern void setIPV4( PIBOX_MSG_T *msg );
extern void getAP( PIBOX_MSG_T *msg );
extern void setAP( PIBOX_MSG_T *msg );
extern void getWireless( PIBOX_MSG_T *msg );
extern void setWireless( PIBOX_MSG_T *msg );
extern void getMACList(PIBOX_MSG_T *msg);
extern void setMACList(PIBOX_MSG_T *msg);
extern void networkRestart(void );
#endif
