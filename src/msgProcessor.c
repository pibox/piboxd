/*******************************************************************************
 * PiBox service daemon
 *
 * msgProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGPROCESSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include <uuid/uuid.h>
#include <pibox/pmsg.h>

#include "piboxd.h"

static int serverIsRunning = 0;
static pthread_mutex_t msgProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t msgProcessorThread;

struct ifaddrs *ifaddr = NULL;

/*========================================================================
 * Name:   isMsgProcessorRunning
 * Prototype:  int isMsgProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of serverIsRunning variable.
 *========================================================================*/
static int
isMsgProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &msgProcessorMutex );
    status = serverIsRunning;
    pthread_mutex_unlock( &msgProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setMsgProcessorRunning
 * Prototype:  int setMsgProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of serverIsRunning variable.
 *========================================================================*/
static void
setMsgProcessorRunning( int val )
{
    pthread_mutex_lock( &msgProcessorMutex );
    serverIsRunning = val;
    pthread_mutex_unlock( &msgProcessorMutex );
}

/*========================================================================
 * Name:   thisHostMatches
 * Prototype:  int thisHostMatches( char * )
 *
 * Description:
 * Test if the specified IP address matches our own address.
 * 
 * Returns:
 * 0 if the addresses do not match.
 * 1 if the addresses do match.
 *========================================================================*/
static int
thisHostMatches( char *ipaddr )
{
    struct ifaddrs *ifa;
    int family, rc;
    char host[NI_MAXHOST];

    if ( ipaddr == NULL )
        return 0;
    if ( ifaddr == NULL )
        return 0;

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;
 
        family = ifa->ifa_addr->sa_family;
        if (family == AF_INET) 
        {
            rc = getnameinfo(ifa->ifa_addr,
                    (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                          sizeof(struct sockaddr_in6),
                    host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if (rc != 0) 
            {
                piboxLogger(LOG_ERROR, "getnameinfo() failed: %s\n", gai_strerror(rc));
                continue;
            }
            if (strcmp(host, ipaddr) == 0 )
                return 1;
        }
    }
    return 0;
}

/*========================================================================
 * Name:   getMyIP
 * Prototype:  void getMyIP( void )
 *
 * Description:
 * Find my IPv4 addresses.
 *========================================================================*/
static void
getMyIP( void )
{
    if (getifaddrs(&ifaddr) == -1) 
    {
        piboxLogger(LOG_ERROR, "Failed to get my IP addresses.\n");
        return;
    }
}

/*========================================================================
 * Name:   msgProcessor
 * Prototype:  void msgProcessor( CLI_T * )
 *
 * Description:
 * Accept and process (or queue) inbound message.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * Timeout for a completed read on the inbound channel is set to 100 milliseconds.
 *========================================================================*/
static void *
msgProcessor( void *arg )
{
    int                 sd, insd;
    struct sockaddr_in  cliAddr, servAddr;
    char                ipaddr[INET_ADDRSTRLEN]; 
    socklen_t           cliLen = sizeof(cliAddr);
    int                 header;
    int                 payloadSize;
    char                tag[37];
    char                *payload;
    void                *ptr;
    int                 len;
    int                 bytesLeft;
    int                 timeOut;
    int                 flags;
    int                 msgType;
    int                 msgAction;
    int                 msgSubAction;
    int                 getTag;
    int                 moreFields;
    int                 keepSD;
    u_int               yes=1;

    /* create socket */
    sd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if(sd<0) {
        piboxLogger(LOG_ERROR, "Cannot open socket.\n");
        pthread_exit(NULL);
    }

    /* Allow multiple sockets to use the same PORT number */
    if (setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) 
    {
        piboxLogger(LOG_ERROR, "Cannot reuse msgProcessor address.\n");
        close(sd);
        pthread_exit(NULL);
    }
 
    /* bind server port */
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(INPORT);

    piboxLogger(LOG_INFO, "Listening on port %d\n", INPORT);
 
    if(bind(sd, (struct sockaddr *) &servAddr, sizeof(servAddr))<0)
    {
        piboxLogger(LOG_ERROR, "cannot bind port: %s\n", strerror(errno));
        pthread_exit(NULL);
    }

    /* Set up to listen on the port. */
    listen(sd,5);

    /* Spin, waiting for connections. */
    setMsgProcessorRunning(1);
    while( isCLIFlagSet(CLI_SERVER) ) 
    {
        /* Wait for a connection. */
        memset(tag, 0, 37);
        memset( &cliAddr, 0, cliLen );
        insd = accept(sd, (struct sockaddr *) &cliAddr, &cliLen);
        if(insd<0) 
        {
            if ( errno == EAGAIN )
            {
                /* No message? Wait a bit and try again. */
                usleep(1000);
                continue;
            }
            piboxLogger(LOG_ERROR, "%s(%d): Cannot accept connection.\n", __FUNCTION__, __LINE__ );
            pthread_exit(NULL);
        }
        piboxLogger(LOG_INFO, "=== Inbound message received.\n");

        /* Find out who sent the message */
        if ( inet_ntop( AF_INET, &cliAddr.sin_addr, ipaddr, INET_ADDRSTRLEN ) == NULL )
        { 
            piboxLogger(LOG_ERROR, "%s(%d): Can't find senders IPV4 address - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }
        piboxLogger( LOG_INFO, 
            "%s(%d): Accepted server connection from [%s:%d]\n", __FUNCTION__, __LINE__, 
            ipaddr, ntohs( cliAddr.sin_port ));

        /* If it's not from this host, drop the connection. */
        if ( !thisHostMatches(ipaddr) )
        { 
            piboxLogger(LOG_ERROR, 
                "%s(%d): Not connected to host that sent data message - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }

        // Set the new socket to non-blocking reads, so we can timeout on them.
        flags = fcntl(insd, F_GETFL, 0);
        fcntl(insd, F_SETFL, flags | O_NONBLOCK);

        /* init header and size */
        header = 0;
        payloadSize = 0;

        /* Read the header. */
        ptr = (void *)&header;
        bytesLeft = 4;
        timeOut = 0;
        while ( (timeOut < 200) && bytesLeft )
        {
            len=read(insd, ptr, bytesLeft);
            if ( len > 0 )
            {
                if ( (bytesLeft-len) >= 0 )
                {
                    ptr += len;
                    bytesLeft -= len;
                }
            }
            usleep(500);
            timeOut ++;
        }
        if (bytesLeft)
        {
            piboxLogger(LOG_ERROR, "%s(%d): Failed to get header - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }
        piboxLogger(LOG_INFO, "header = %08x\n", header);

        /* Decide if additional fields need to be read. */
        moreFields = 0;
        getTag = 0;
        keepSD = 0;
        msgType = header & 0x000000ff;
        msgAction = (header>>8) & 0x000000ff;
        switch(msgType)
        {
            case MT_PW:
                switch( msgAction )
                {
                    case MA_SAVE:
                        moreFields = 1;
                        keepSD = 1;
                        break;
                    case MA_GET:
                        keepSD = 1;
                        break;
                    case MA_DEL:
                        moreFields = 1;
                        break;
                }
                break;

            case MT_STREAM:
            case MT_HEARTBEAT:
                moreFields = 1;
                getTag = 1;
                break;

            case MT_NET:
                switch( msgAction )
                {
                    case MA_GETIF:
                    case MA_GETMAC:
                    case MA_GETIP:
                        moreFields = 1;
                        keepSD = 1;
                        break;

                    case MA_MFLIST:
                        msgSubAction = (header>>16) & 0x000000ff;
                        switch( msgSubAction )
                        {
                            case MS_GET:
                                break;
                            case MS_SET:
                                moreFields = 1;
                                break;
                        }
                        keepSD = 1;
                        break;

                    case MA_GETIFLIST:
                    case MA_GETDNS:
                    case MA_GETNETTYPE:
                    case MA_GETWIRELESS:
                    case MA_GETAP:
                        keepSD = 1;
                        break;

                    case MA_SETIPV4:
                    case MA_SETNETTYPE:
                    case MA_SETAP:
                    case MA_SETWIRELESS:
                        moreFields = 1;
                        break;

                    case MA_RESTART:
                    default:
                        moreFields = 0;
                        keepSD = 0;
                        break;
                }
                break;

            case MT_SYS:
                switch( msgAction )
                {
                    case MA_ROTATE:
                    case MA_REBOOT:
                    case MA_KEYB:
                    default:
                        moreFields = 0;
                        keepSD = 0;
                        break;
                }
                break;

            default:
                piboxLogger(LOG_ERROR, "Invalid msg type (%d) - dropping message.\n", 
                    __FUNCTION__, __LINE__, msgType ); 
                close(insd);
                continue;
                break;
        }
        if ( !moreFields )
        {
            payloadSize = 0;
            payload = NULL;
            goto skipFields;
        }

        /* Read the size. */
        ptr = (void *)&payloadSize;
        bytesLeft = 4;
        timeOut = 0;
        while ( (timeOut < 200) && bytesLeft )
        {
            len=read(insd, ptr, bytesLeft);
            if ( len > 0 )
            {
                if ( (bytesLeft-len) >= 0 )
                {
                    ptr += len;
                    bytesLeft -= len;
                }
            }
            usleep(500);
            timeOut ++;
        }
        if (bytesLeft)
        {
            piboxLogger(LOG_ERROR, "%s(%d): Failed to get size field - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }
        piboxLogger(LOG_INFO, "payload size = %08x\n", payloadSize);

        /* Read the tag */
        if ( getTag )
        {
            ptr = (void *)&tag;
            bytesLeft = 36;
            timeOut = 0;
            while ( (timeOut < 200) && bytesLeft )
            {
                len=read(insd, ptr, bytesLeft);
                if ( len > 0 )
                {
                    if ( (bytesLeft-len) >= 0 )
                    {
                        ptr += len;
                        bytesLeft -= len;
                    }
                }
                usleep(500);
                timeOut ++;
            }
            if (bytesLeft)
            {
                piboxLogger(LOG_ERROR, "%s(%d): Failed to read tag - dropping message.\n", 
                    __FUNCTION__, __LINE__ ); 
                close(insd);
                continue;
            }
            piboxLogger(LOG_INFO, "tag = %s\n", tag);
        }

        /* Allocate a buffer for the payload */
        payload = NULL;
        if ( payloadSize > 0 )
        {
            payload = (char *)malloc(payloadSize+1);
            if ( payload == NULL )
            {
                piboxLogger(LOG_ERROR, "%s(%d): Failed to allocate payload buffer - dropping message.\n",
                    __FUNCTION__, __LINE__ ); 
                close(insd);
                continue;
            }
            memset(payload, 0, payloadSize+1);
    
            /* Grab the payload */
            ptr = (void *)payload;
            bytesLeft = payloadSize;
            timeOut = 0;
            while ( (timeOut < 200) && bytesLeft )
            {
                len=read(insd, ptr, bytesLeft);
                if ( len > 0 )
                {
                    if ( (bytesLeft-len) >= 0 )
                    {
                        ptr += len;
                        bytesLeft -= len;
                    }
                }
                usleep(500);
                timeOut ++;
            }
            if (bytesLeft)
            {
                piboxLogger(LOG_ERROR, "%s(%d): Failed to read payload - dropping message.\n", 
                    __FUNCTION__, __LINE__ ); 
                close(insd);
                free(payload);
                continue;
            }
            piboxLogger(LOG_INFO, "payload = %s\n", payload);
        }
        else
        {
            piboxLogger(LOG_ERROR, "%s(%d): Payload size is zero - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }

skipFields:
        if ( !keepSD )
        {
            piboxLogger(LOG_INFO, "Closing socket\n");
            close(insd);
            insd = -1;
        }
        else
            piboxLogger(LOG_INFO, "Holding socket: %d\n", insd);

        /* Queue the message. */
        piboxLogger(LOG_INFO, "Queueing Inbound Msg\n");
        queueMsg( header, payloadSize, tag, payload, insd, 0);
    }

    close(sd);
    piboxLogger(LOG_INFO, "%s: msgProcessor thread is exiting.\n", PROG);
    setMsgProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startMsgProcessor
 * Prototype:  void startMsgProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownMsgProcessor().
 *========================================================================*/
void
startMsgProcessor( void )
{
    int rc;

    /* Get my IP address before starting to process inbound message */
    getMyIP();

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&msgProcessorThread, NULL, &msgProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create msgProcessor thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started msgProcessor thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownMsgProcessor
 * Prototype:  void shutdownMsgProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownMsgProcessor( void )
{
    int timeOut = 1;

    // Free up my IP address list.
    if ( ifaddr != NULL )
        freeifaddrs(ifaddr);

    while ( isMsgProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_INFO, "%s: Timed out waiting on msgProcessor thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(msgProcessorThread);
    piboxLogger(LOG_INFO, "%s: msgProcessor has shut down.\n", PROG);
}

