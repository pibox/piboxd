/*******************************************************************************
 * PiBox service daemon
 *
 * regProcessor.c:  Process inbound multicast registration messages 
 *                  from IoT devices
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define REGPROCESSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "piboxd.h"

#define MAX_LEN  1024   /* maximum receive string size */

static int serverIsRunning = 0;
static pthread_mutex_t regProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t regProcessorThread;

static struct ifaddrs *regifaddr = NULL;

/*========================================================================
 * Name:   isRegProcessorRunning
 * Prototype:  int isRegProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of serverIsRunning variable.
 *========================================================================*/
static int
isRegProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &regProcessorMutex );
    status = serverIsRunning;
    pthread_mutex_unlock( &regProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setRegProcessorRunning
 * Prototype:  int setRegProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of serverIsRunning variable.
 *========================================================================*/
static void
setRegProcessorRunning( int val )
{
    pthread_mutex_lock( &regProcessorMutex );
    serverIsRunning = val;
    pthread_mutex_unlock( &regProcessorMutex );
}

/*========================================================================
 * Name:   getMyIP
 * Prototype:  void getMyIP( void )
 *
 * Description:
 * Find my IPv4 addresses.
 *========================================================================*/
static void
getMyIP( void )
{
    if (getifaddrs(&regifaddr) == -1) 
    {
        piboxLogger(LOG_ERROR, "Failed to get my IP addresses.\n");
        return;
    }
}

/*========================================================================
 * Name:   regProcessor
 * Prototype:  void regProcessor( CLI_T * )
 *
 * Description:
 * Accept and queue inbound multicast messages.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *========================================================================*/
static void *
regProcessor( void *arg )
{
    struct sockaddr_in      addr;
    struct sockaddr_in      srcaddr;
    char                    ipaddr[INET_ADDRSTRLEN]; 
    socklen_t               srcaddrlen;
    int                     sd;
    struct ip_mreq          mreq;
    u_int                   yes=1;
    unsigned char           buf[MAX_LEN+1];
    int                     payloadSize;
    char                    *payload;
    void                    *ptr;
    int                     header;
    int                     flags = 0;
    int                     bytesread;
    fd_set                  rfds;
    struct timeval          tv;
    int                     retval;

    struct ifaddrs          *ifaddr, *ifa;
    struct in_addr          ip_addr;
    int                     family;

    /* create socket */
    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sd<0) 
    {
        piboxLogger(LOG_ERROR, "Multicast cannot open socket: %s\n", strerror(errno));
        pthread_exit(NULL);
    }

    /* Allow multiple sockets to use the same PORT number */
    if (setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) 
    {
        piboxLogger(LOG_ERROR, "Multicast cannot reused address.\n");
        close(sd);
        pthread_exit(NULL);
    }

    /* Set up destination address */
    memset(&addr,0,sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_addr.s_addr=htonl(INADDR_ANY); /* N.B.: differs from sender */
    addr.sin_port=htons(MULTICAST_PORT);
 
    /* bind to receive address */
    if (bind(sd,(struct sockaddr *) &addr,sizeof(addr)) < 0) 
    {
        piboxLogger(LOG_ERROR, "Multicast bind failure: %s\n", strerror(errno));
        close(sd);
        pthread_exit(NULL);
    }
     
    /* use setsockopt() to request that available network interfaces join a multicast group */
    piboxLogger(LOG_INFO, "Adding interfaces to multicast group %s:%d\n", MULTICAST_IP, MULTICAST_PORT);
    mreq.imr_multiaddr.s_addr=inet_addr(MULTICAST_IP);
    if (getifaddrs(&ifaddr) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to get network interfaces list: %s\n", strerror(errno));
        close(sd);
        pthread_exit(NULL);
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        /* Skip interfaces that aren't configured */
        if (ifa->ifa_addr == NULL)
        {
            piboxLogger(LOG_TRACE1, "Skipping %s: not configured.\n", ifa->ifa_name);
            continue;
        }

        /* We only use IPv4 currently (home networks don't need IPv6) */
        family = ifa->ifa_addr->sa_family;
        if ( family != AF_INET )
        {
            piboxLogger(LOG_TRACE1, "Skipping %s: not IPv4.\n", ifa->ifa_name);
            continue;
        }

        /* Skip the loopback interface */
        if ( strncmp(ifa->ifa_name, "lo", 2) == 0 )
        {
            piboxLogger(LOG_TRACE1, "Skipping loopback %s.\n", ifa->ifa_name);
            continue;
        }

        /* Get the IPv4 address for the interface. */
        ip_addr = ((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;

        /* Add the interface to the multicast group. */
        mreq.imr_interface.s_addr=ip_addr.s_addr;
        if (setsockopt(sd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) 
        {
            piboxLogger(LOG_ERROR, "Multicast failed to joing group: %s\n", strerror(errno));
            close(sd);
            pthread_exit(NULL);
        }
        piboxLogger(LOG_INFO, "%s joined multicast group %s:%d\n", ifa->ifa_name, MULTICAST_IP, MULTICAST_PORT);
    }
    freeifaddrs(ifaddr);

    /* Setup for select() */
    FD_ZERO(&rfds);
    FD_SET(sd, &rfds);

    /* Spin, waiting for connections. */
    setRegProcessorRunning(1);
    while( isCLIFlagSet(CLI_MULTICAST) ) 
    {
        /* Wait for a connection. */
        srcaddrlen = sizeof(srcaddr);
        memset( &srcaddr, 0, srcaddrlen );
        memset( buf, 0, MAX_LEN + 1);
        header = 0;
        payloadSize = 0;
        payload = NULL;

        tv.tv_sec = 2;
        tv.tv_usec = 0;
        retval = select(1, &rfds, NULL, NULL, &tv);

        /* Ignore select errors or timer not expired. */
        if ( retval < 0 )
        {
            piboxLogger(LOG_TRACE5, "select: %s\n", strerror(errno));
            continue;
        }

        bytesread = recvfrom(sd, buf, MAX_LEN, MSG_DONTWAIT, (void *)&srcaddr, &srcaddrlen);
        if (bytesread < 0) 
        {
            if ( (errno == EAGAIN) || (errno == EWOULDBLOCK) )
            {
                // No message? Try again.
                // piboxLogger(LOG_ERROR, "Multicast receiver timeout\n");
                continue;
            }
            piboxLogger(LOG_ERROR, "Cannot accept connection: %s\n", strerror(errno) );
            continue;
        }
        piboxLogger(LOG_INFO, "=== Inbound message received.\n");

        /* Get the header */
        ptr = buf;
        header = *((int *)ptr);
        piboxLogger(LOG_INFO, "header = %08x\n", header);

        /* Get the payload size */
        ptr += 4;
        payloadSize = *((int *)ptr);
        piboxLogger(LOG_INFO, "payloadSize = %08x\n", payloadSize);

        /* Copy the payload */
        ptr += 4;
        payload = calloc(1, payloadSize);
        memcpy(payload, (char *)ptr, payloadSize);

        /* Find out who sent the message */
        if ( inet_ntop( AF_INET, &srcaddr.sin_addr, ipaddr, INET_ADDRSTRLEN ) == NULL )
        { 
            piboxLogger(LOG_ERROR, "Can't find senders IPV4 address - dropping message.\n");
            continue;
        }
        piboxLogger( LOG_INFO, 
            "Accepted multicast connection from [%s:%d]\n", ipaddr, ntohs( srcaddr.sin_port ));

        /* Queue the message. */
        piboxLogger(LOG_INFO, "Queueing Inbound Registration\n");
        flags |= PIBOX_MULTICAST;
        queueMsg( header, payloadSize, ipaddr, payload, -1, flags);
    }

    close(sd);
    piboxLogger(LOG_INFO, "%s: regProcessor thread is exiting.\n", PROG);
    setRegProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startRegProcessor
 * Prototype:  void startRegProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownRegProcessor().
 *========================================================================*/
void
startRegProcessor()
{
    int rc;

    /* Get my IP address before starting to process inbound message */
    getMyIP();

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&regProcessorThread, NULL, &regProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create regProcessor thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started regProcessor thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownRegProcessor
 * Prototype:  void shutdownRegProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownRegProcessor()
{
    int timeOut = 1;

    // Free up my IP address list.
    if ( regifaddr != NULL )
        freeifaddrs(regifaddr);

    while ( isRegProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_INFO, "%s: Timed out waiting on regProcessor thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(regProcessorThread);
    piboxLogger(LOG_INFO, "%s: regProcessor has shut down.\n", PROG);
}

