/*******************************************************************************
 * PiBox service daemon
 *
 * utils.h:  Handle log messages
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef UTILS_H
#define UTILS_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef UTILS_C
extern int   getVideoLength( char *filename );
extern char *findVideo( char *filename );
extern int   isThisMyIP( char *ipaddr );
extern char *dotToDash( char *buf );
extern int   parse(char *line, char **argv, int max);
extern void  reboot( void );
extern void  keyboard( void );
extern int   displayRotate( void );
#endif /* !UTILS_C */
#endif /* !UTILS_H */
