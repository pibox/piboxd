/*******************************************************************************
 * PiBox service daemon
 *
 * queueProcessor.c:  Process queued messages (re: start/stop processes).
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef QUEUEPROCESSOR_H
#define QUEUEPROCESSOR_H

#include <pthread.h>
#include <semaphore.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

#define REALM        "PiBox"
#define PW_F         "/etc/lighttpd/.passwd"
#define MK_PASSWD    "/usr/bin/pibox-genpw.sh -u %s -p %s -r %s -o %s"
#define WEBSVC       "/etc/init.d/S50lighttpd restart"
#define PWSTAMP_F    "/etc/lighttpd/DEFAULT_CHANGED"
#define PWSED        "sed -i '/%s/d' %s"

/*========================================================================
 * Variables
 *=======================================================================*/
#ifdef QUEUEPROCESSOR_C
sem_t           queueSem;
#else
extern pthread_mutex_t queueMutex;
extern sem_t           queueSem;
#endif /* !QUEUEPROCESSOR_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef QUEUEPROCESSOR_C

extern void startQueueProcessor( void );
extern void wakeQueueProcessor( void );
extern void shutdownQueueProcessor( void );

#endif /* !QUEUEPROCESSOR_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !QUEUEPROCESSOR_H */
