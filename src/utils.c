/*******************************************************************************
 * PiBox service daemon
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#define _GNU_SOURCE  // To get definition of NI_MAXHOST
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <signal.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <pibox/utils.h>

#include "piboxd.h"

/*========================================================================
 * Name:   getVideoLength
 * Prototype:  int getVideoLength( char *filename )
 *
 * Description:
 * Use ffmpeg to determine the length of a file.  It should be in a format known to ffmpeg.
 * Command: 
 *   ffmpeg -i <file> 2>&1 | grep "Duration:" | sed 's/  Duration: //' | cut -f1 -d"," | cut -f1 -d"."
 *
 * Returns:
 * The number of seconds the video runs or -1 if the length can't be determined.
 *========================================================================*/
int
getVideoLength( char *filename )
{
    // TBD
    // Use popen() to call ffmpeg command and read output
    // parse hh:mm:ss into seconds
    // return
    // deal with
    // 1. no such file
    // 2. no duration found
    // 3. bad duration format

    // For now:
    return -1;
}

/*========================================================================
 * Name:   findVideo
 * Prototype:  int getVideo( char *filename )
 *
 * Description:
 * Search USB mount points for the named file.
 *
 * Returns:
 * Path string to file.  Caller must free string.
 *========================================================================*/
char *
findVideo( char *filename )
{
    // TBD
    // For now:
    return NULL;
}

/*
 *========================================================================
 * Name:   isThisMyIP
 * Prototype:  int isThisMyIP( char * )
 *
 * Description:
 * Test if the character string passed in matches any of our IP addresses.
 *
 * Returns:
 * 0 if no match, 1 if the character string matches.
 *========================================================================
 */
int
isThisMyIP( char *ipaddr )
{
    struct ifaddrs *ifaddr, *ifa;
    int family, rc;
    char host[NI_MAXHOST];
    int match = 0;

    if ( !piboxValidIPAddress( ipaddr ) )
        return 0;

    if (getifaddrs(&ifaddr) == -1) 
    {
        piboxLogger(LOG_ERROR, "Failed to get my own IP addresses.");
        return 0;
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;

        rc=-1;
        family = ifa->ifa_addr->sa_family;
        if (family == AF_INET || family == AF_INET6) 
        {
            // This is how we get the character string of the IP address.
            rc = getnameinfo(ifa->ifa_addr,
                           (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                                 sizeof(struct sockaddr_in6),
                           host, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
        }
        if ( rc==0 )
        {
            // Got an IP string, now compare it with the input string.
            if ( strncmp(ipaddr, host, strlen(ipaddr)) == 0 )
            {
                match = 1;
                break;
            }
        }
    }

    // Cleanup and return.
    freeifaddrs(ifaddr);
    return (match);
}

/*
 *========================================================================
 * Name:   dotToDash
 * Prototype:  void dotToDash( char * )
 *
 * Description:
 * Convert "." to "-" in the supplied buffer.
 *========================================================================
 */
void
dotToDash( char *buf )
{
    char *ptr = buf;
    while (*ptr != '\0' )
    {
        if (*ptr == '.')
            *ptr = '-';
        ptr++;
    }
}

/*
 * ========================================================================
 * Name:   parse
 * Prototype:  int parse( char *, char **, int )
 *
 * Description:
 * Parse a character string into an array of character tokens.
 *
 * Input Arguments:
 * char *line       The string to parse
 * char **argv      The parsed tokens
 * int  max         Maximum number of arguments to parse
 *
 * Returns:
 * Number of arguments in argv.
 *
 * Note:
 * Borrowed from http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/shell.c
 * ========================================================================*/
int
parse(char *line, char **argv, int max)
{
    int idx = 0;
    int quotes = 0;

    /* if not the end of line ....... */ 
    while (*line != '\0') 
    {       
        /* Newlines or null characters mean we're done. */
        if (*line == '\n' || *line == '\0')
        {
            *line = '\0';     
            break;
        }

        /* replace white spaces with 0 */
        while (*line == ' ' || *line == '\t')
            *line++ = '\0';     

        /* save the argument position */
        if ( idx < max )
        {
            if ( *line == '"' )
            {
                /* Move past the first double quote */
                line++;
                quotes=1;
            }
            *argv++ = line;
        }

        /* skip the argument until ... */
        if ( quotes )
        {
            /* we find a matching closing double quote */
            while (*line != '\0' && *line != '"' && *line != '\n') 
                line++;

            /* If we found the quote, move past it. */
            if ( *line == '"' )
                line++;

            /* Reset quotes marker */
            quotes=0;
        }
        else
        {
            /* we find a space, tab or newline */
            while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') 
                line++;
        }

        if ( ++idx == max )
            break;
    }

    /* mark the end of argument list  */
    *argv = '\0';

    return idx;
}

/*
 *========================================================================
 * Name:   reboot
 * Prototype:  void reboot( char * )
 *
 * Description:
 * Exec the reboot command.
 *
 * Notes:
 * We will not be reaping this child.
 *========================================================================
 */
void
reboot( void )
{
    pid_t   childPid;
    char    *args[2];

    childPid = fork();
    if ( childPid == 0 )
    {
        /*
         * Wait a tick for the parent to return to normal before
         * we pull the rug out from under it.
         */
        piboxLogger(LOG_INFO, "Reboot command: %s\n", REBOOT_CMD);
        sleep(1);
        args[0] = strdup(REBOOT_CMD);
        args[1] = NULL;
        execv(REBOOT_CMD, args);
        fprintf(stderr, "Failed to reboot system.\n");
        exit(1);
    }

    return;
}

/*
 *========================================================================
 * Name:   keyboard
 * Prototype:  void keyboard(void)
 *
 * Description:
 * Handle a keyboard (or general input) event.  This is used to serialize
 * handling of multiple events from a single device.
 *========================================================================
 */
void
keyboard( void )
{
    pid_t   childPid;
    char    *args[2];
    int status;

    childPid = fork();
    if ( childPid == 0 )
    {
        /*
         * Call the keyboard-fe.sh script which handles enabling
         * special keys under Xorg for supported keyboards.
         */
        piboxLogger(LOG_INFO, "Keyboard plug event - calling %s\n", KEYBOARD_CMD);
        sleep(1);
        args[0] = strdup(KEYBOARD_CMD);
        args[1] = NULL;
        execv(KEYBOARD_CMD, args);
        fprintf(stderr, "Failed to run %s.\n", KEYBOARD_CMD);
        exit(1);
    }

    /* Wait for the script to complete. We ignore results. */
    waitpid( childPid, &status, 0);

    return;
}

/*
 *========================================================================
 * Name:   displayRotate
 * Prototype:  void displayRotate(void)
 *
 * Description:
 * Handle a display rotate request.  This calls an external script
 * to handle the rotation.  The script needs to edit firmware config
 * files so must be called by a privileged daemon, such as piboxd.
 *
 * Returns:
 * The return code from external script.
 *
 * Notes:
 * xrandr doesn't work with fbturbo.
 * We use a shell script, pirotate, to update firmware
 * config files and, if needed, touchscreen configurations.
 * piboxd provides pirotate.sh but it depends on files
 * installed by other packages.
 *========================================================================
 */
int
displayRotate( void )
{
    pid_t   childPid;
    char    *args[2];
    int status;

    childPid = fork();
    if ( childPid == 0 )
    {
        /*
         * Call the keyboard-fe.sh script which handles enabling
         * special keys under Xorg for supported keyboards.
         */
        piboxLogger(LOG_INFO, "Display rotate event - calling %s\n", PIROTATE_CMD);
        sleep(1);
        args[0] = strdup(PIROTATE_CMD);
        args[1] = NULL;
        execv(PIROTATE_CMD, args);
        fprintf(stderr, "Failed to run %s.\n", PIROTATE_CMD);
        exit(1);
    }

    /* Wait for the script to complete. */
    piboxLogger(LOG_INFO, "Waiting on %s to complete.\n", PIROTATE_CMD);
    waitpid( -1, &status, 0);
    piboxLogger(LOG_INFO, "%s completed, exit status = %d.\n", WEXITSTATUS(status));

    /* Return the exit status of script. */
    return WEXITSTATUS(status);
}
