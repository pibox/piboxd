/*******************************************************************************
 * PiBox service daemon
 *
 * main.c:  Setup and initialization
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MAIN_C

// System headers
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/time.h>
#include <piboxnet/pnc.h>

// Program headers
#include "piboxd.h"

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;

        case SIGINT:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;

        case SIGQUIT:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;
    }
}

/*
 * ========================================================================
 * Handle shutdown processing outside of daemon
 * ========================================================================
 */
static void
shutdownMainLoop(int signo)
{
    daemonEnabled = 0;
}

/*
 * ========================================================================
 * Setup SMB automount timer.
 * ========================================================================
 */
static void
smbSetup()
{
    struct sigaction sa; 
    struct itimerval timer; 

    if ( ! (cliOptions.flags & CLI_ROOT) )
    {
        piboxLogger(LOG_WARN, "Not running as root - skipping smb setup.\n");
        return;
    }

    /* Install timer_handler as the signal handler for SIGALRM.  */ 
    memset (&sa, 0, sizeof (sa)); 
    sa.sa_handler = &enableSMB;
    sigemptyset(&sa.sa_mask);
    sigaction (SIGALRM, &sa, NULL); 

    /* Configure the timer to expire after 10 sec...  */ 
    timer.it_value.tv_sec = 10; 
    timer.it_value.tv_usec = 0; 

    /* ... and every 10 sec after that.  */ 
    timer.it_interval.tv_sec = 10; 
    timer.it_interval.tv_usec = 0; 

    /* Start the timer. */ 
    setitimer (ITIMER_REAL, &timer, NULL); 
}

/*
 * ========================================================================
 * Disable SMB automount timer.
 * ========================================================================
 */
static void
smbDisable()
{
    struct itimerval timer; 

    if ( ! (cliOptions.flags & CLI_ROOT) )
    {
        piboxLogger(LOG_WARN, "Not running as root - skipping smb disable.\n");
        return;
    }

    /* Disarm SMB timer. */
    timer.it_value.tv_sec = 0; 
    timer.it_value.tv_usec = 0; 
    timer.it_interval.tv_sec = 0; 
    timer.it_interval.tv_usec = 0; 

    /* Start the timer. */ 
    setitimer (ITIMER_REAL, &timer, NULL); 
}

/*
 * ========================================================================
 * Handle daemonizing of the application.
 * See http://www.enderunix.org/docs/eng/daemon.php
 * ========================================================================
 */
static void
daemonize( void )
{
    int                 pid, fd;
    char                pidStr[7];
    struct sigaction    sa;
    int                 s = 0;
    sigset_t            set;
    FILE                *pd;

    pid = fork();

    /* On error, print a message and exit */
    if ( pid<0 )
    {
        fprintf(stderr, "%s: Failed to daemonize: %s\n", PROG, strerror(errno));
        exit(1);
    }

    /* If this is the parent, we exit immediately. */
    if ( pid>0 )
    {
        /* Save the child's PID. */
        fprintf(stderr, "%s: Saving PID to %s\n", PROG, PIDFILE);
        pd = fopen(PIDFILE, "w");
        fprintf(pd, "%d", pid);
        fclose(pd);

        fprintf(stderr, "%s: parent process is exiting.\n", PROG);
        exit(0);
    }

    /* The child needs some work to make it a daemon. */
    fprintf(stderr, "%s: Setting up daemon\n", PROG);

    /* Get our own process group and detach the controlling terminal. */
    setsid();

    /* File creation mask - for safety sake */
    umask(027);

    /* Change to a meaningful working directory. */
    mkdir(RUN_DIR, 0700);
    if ( chdir(RUN_DIR) == -1 )
        fprintf(stderr, "Failed to change to %s\n", RUN_DIR);

    /* Set up a lock file so only one instance can run. */
    cliOptions.lockfd = open(LOCK_FILE,O_RDWR|O_CREAT,0640);
    if ( cliOptions.lockfd<0 )
    {
        fprintf(stderr, "%s: Failed to get lock file.  Exiting.\n", PROG);
        exit(1);
    }

    if (lockf(cliOptions.lockfd,F_TLOCK,0)<0)
    {
        /* only first instance continues */
        fprintf(stderr, "%s: is already running.\n", PROG);
        exit(0);
    }

    /* Save child pid to lockfile */
    sprintf(pidStr,"%d\n",getpid());
    if ( write(cliOptions.lockfd, pidStr, strlen(pidStr)) == -1 )
        fprintf(stderr, "Failed to save child pid to lockfile.\n");

    /* Setup for basic signal handling */
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    s = pthread_sigmask(SIG_BLOCK, &set, NULL);
    if (s != 0)
    {
        fprintf(stderr, "%s: Failed to block SIGUSR1 in main thread.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }

    /* Close inherited descriptors, then get our own. */
    for (fd=getdtablesize(); fd>=0; --fd)
        close(fd);
    fd = open("/dev/null", O_RDWR);    stdin  = fdopen(fd, "r"); /* stdin  */
    fd = open("/dev/console", O_RDWR); stdout = fdopen(fd, "w"); /* stdout */
    fd = dup(fd);                      stderr = fdopen(fd, "w"); /* stderr */
}

/*
 * ========================================================================
 * Name:   waitForExit
 * Prototype:  static void waitForExit( void )
 *
 * Description:
 * Use a unix-domain socket for use in signaling a shutdown.
 * ========================================================================
 */
static void
waitForExit( void )
{
    struct sockaddr_un      local;
    int                     sd;
    fd_set                  rfds;
    struct timeval          tv;

    /* create unix socket */
    sd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sd<0) 
    {
        piboxLogger(LOG_ERROR, "Cannot open domain socket: %s\n", strerror(errno));
        return;
    }

    /* Set up unix socket address */
    memset(&local,0,sizeof(struct sockaddr_un));
    local.sun_family=AF_UNIX;
    strcpy(local.sun_path, SERVER_SOCKET_PATH);

    /* unlink path to make sure socket creation succeeds. */
    unlink(SERVER_SOCKET_PATH);
 
    /* bind to receive address */
    if (bind(sd,(struct sockaddr *) &local, strlen(local.sun_path) + sizeof(local.sun_family)) != 0) 
    {
        piboxLogger(LOG_ERROR, "Domain socket bind failure: %s\n", strerror(errno));
        close(sd);
        return;
    }

    /* Listen for the exit signal. */
    if ( listen(sd, 1) == -1 )
    {
        piboxLogger(LOG_ERROR, "Domain socket listen failure: %s\n", strerror(errno));
        close(sd);
        return;
    }

    /* Watch the socket activity. */
    FD_ZERO(&rfds);
    FD_SET(sd, &rfds);

    while(daemonEnabled)
    {
        /*
         * Wait for signal handler to tell use we're exiting.
         */
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        select(sd+1, &rfds, NULL, NULL, &tv);
    }

    close(sd);
    unlink(SERVER_SOCKET_PATH);
    return;
}

/*
 * ========================================================================
 * Name:   main
 * Prototype:  static void main( int, char ** )
 *
 * Description:
 * Program initialization
 *
 * Input Arguments:
 * int       argc   Number of command line arguments
 * char      **argv Command line arguments
 * ========================================================================
 */
int
main( int argc, char **argv)
{
    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    loadConfig();

    // Daemonize and setup signal handling
    daemonEnabled = 1;
    if ( cliOptions.flags & CLI_DAEMON )
        daemonize();
    else
    {
        /* Handle signals */
        signal(SIGQUIT, shutdownMainLoop);
        signal(SIGTERM, shutdownMainLoop);
        signal(SIGHUP, shutdownMainLoop);
        signal(SIGINT, shutdownMainLoop);
    }

    // Schedule SMB mount handling.
    if ( isCLIFlagSet(CLI_SMB) )
    {
        smbSetup();
    }

    // Setup logging
    if ( isCLIFlagSet(CLI_SYSLOG) )
        piboxLoggerSetFlags(LOG_F_SYSLOG);
    else
        piboxLoggerSetFlags(LOG_F_TIMESTAMP);
    piboxLoggerInit( cliOptions.logFile );
    piboxLoggerVerbosity( cliOptions.verbose );

    // Init network library
    pncInit( isCLIFlagSet(CLI_TEST), NULL );

    // Start threads
    if ( isCLIFlagSet(CLI_SMB) )
    {
        startSMBProcessor();
    }
    if ( isCLIFlagSet(CLI_IOT) )
    {
        startRegProcessor(); // Process recently received IoT requests.
    }
    startQueueProcessor();   // Process recently received local requests.
    startTimerProcessor();   // Process active streams, mostly to shut them down.
    startMsgProcessor();     // Handling inbound messages and put them on a queue.
    startEthProcessor();     // Watch for eth0 plug events.

    // Spin
    piboxLogger(LOG_INFO, "Spinning...\n");
    waitForExit();

    piboxLogger(LOG_INFO, "Exiting...\n");

    shutdownEthProcessor();

    // Shutdown SMB mount handling.
    if ( isCLIFlagSet(CLI_SMB) )
    {
        smbDisable();
    }

    // Gracefully cleanup
    unsetCLIFlag( CLI_SERVER );
    unsetCLIFlag( CLI_MULTICAST );
    if ( isCLIFlagSet(CLI_IOT) )
    {
        shutdownRegProcessor();
    }
    shutdownMsgProcessor();
    shutdownQueueProcessor();
    shutdownTimerProcessor();
    if ( isCLIFlagSet(CLI_SMB) )
    {
        shutdownSMBProcessor();
    }
    piboxLogger(LOG_INFO, "Precleanup: Q_IN   = %d\n", queueSize(Q_IN));
    piboxLogger(LOG_INFO, "Precleanup: Q_PROC = %d\n", queueSize(Q_PROC));
    piboxLogger(LOG_INFO, "Clearing msg queue\n");

    piboxLogger(LOG_INFO, "Killing streams\n");
    killStreams();
    piboxLogger(LOG_INFO, "Clearing msg queue\n");
    clearMsgQueue();

    piboxLogger(LOG_INFO, "Postcleanup: Q_IN   = %d\n", queueSize(Q_IN));
    piboxLogger(LOG_INFO, "Postcleanup: Q_PROC = %d\n", queueSize(Q_PROC));
    piboxLoggerShutdown();

    // b'bye!
    return(0);
}
