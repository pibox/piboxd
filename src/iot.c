/*******************************************************************************
 * PiBox service daemon
 *
 * iot.c:  Manage IoT devices
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define IOT_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <uuid/uuid.h>
#include <curl/curl.h>

#include "piboxd.h"

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Static functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Public functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   findRegistration
 * Prototype:  int findRegistration( const char *ipaddr )
 *
 * Description:
 * Test if a device is currently registered.
 *
 * Arguments:
 * char *ipaddr  The IP address of the device trying to register.
 *
 * Returns:
 * 1 if the registration already exists.
 * 0 if the registration does not exist.
 *
 * Notes:
 * Registration implies that the nodes IP is found in the registration
 * directory.
 *
 * This should really use a curl request to get the configuration directory
 * from the REST API.
 *========================================================================*/
int
findRegistration( const char *ipaddr )
{
    DIR *dirp;
    struct dirent *filep;

    /* Open registration dir */
    if ((dirp = opendir(IOT_DIR)) == NULL )
    {
        piboxLogger(LOG_ERROR, "Could not open %s.\n", IOT_DIR);
        return(0);
    }

    /* Iterate filenames to find IP address. */
    do {
        errno = 0;
        if ((filep = readdir(dirp)) != NULL) 
        {
            if (strcmp(filep->d_name, ipaddr) == 0)
            {
                closedir(dirp);
                return(1);
            }
        }
    } while (filep != NULL);

    closedir(dirp);
    return(0);
}

/*========================================================================
 * Name:   registerDevice
 * Prototype:  int registerDevice( PIBOX_MSG_T *msg )
 *
 * Description:
 * This isn't really a registration.  It's a response to a multicast request
 * for a monitor to return a UUID.  If the requesting device is not registered
 * then a UUID is generated and returned to the device.
 *========================================================================*/
void
registerDevice( PIBOX_MSG_T *msg )
{
    char        buf[128];
    char        errbuf[CURL_ERROR_SIZE];
    long        http_code = 0;
    CURL        *curl;
    CURLcode    res;
    const char  *uuidNode = NULL;
    char        uuidStr[37];
    char        uuidBuf[43];
    uuid_t      uuid;
    char        ipaddr[37];

    if ( msg == NULL )
        return;

    // Grab IP address
    sprintf(ipaddr, "%s", msg->tag);

    //Initializing the CURL module
    curl = curl_easy_init();
    if ( !curl ) 
    {
        piboxLogger(LOG_ERROR, "Could not initialize curl.\n");
        return;
    }

    // Check for existing registration
    if ( findRegistration(ipaddr) )
    {
        /* Do nothing - registration already exists! */
        piboxLogger(LOG_ERROR, "Registration already exists for %s.\n", ipaddr);
        return;
    }

    // Generate UUID
    uuid_generate_time_safe( uuid );
    uuid_unparse( uuid, uuidStr );
    uuidNode = uuidStr;
    sprintf( uuidBuf, "%s", uuidNode );

    /*
     * Setup ACK to device, including uuid, but only wait for 20 seconds for a response.
     * After all - the device should be pretty close to the monitor, relatively speaking.
     */
    sprintf(buf, "http://%s/register?uuid=%s", ipaddr, uuidBuf);
    piboxLogger(LOG_INFO, "Contacting: %s\n", buf);
    curl_easy_setopt(curl, CURLOPT_URL, buf);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);
    errbuf[0] = 0;

    // Make the request to the device.
    res = curl_easy_perform(curl);
    if (CURLE_OK == res) 
    {
        // We contacted the device.  Make sure our query was okay.
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
        switch (http_code)
        {
            // 200 means "Already configured by you"
            case 200:
                piboxLogger(LOG_INFO, "Received 200 status code: Already configured.\n");
                break;

            // 202 means "New configuration request accepted"
            case 202: 
                piboxLogger(LOG_INFO, "Received 202 status code: UUID accepted.\n");
                break;

            // 400 means "UUID is missing."
            case 400:
                piboxLogger(LOG_INFO, "Received 400 status code: UUID is missing.\n");
                break;

            // 403 means "Device is not in pairing mode."
            case 403:
                piboxLogger(LOG_INFO, "Received 403 status code: Not in pairing mode.\n");
                break;

            // 406 means "UUID is invalid."
            case 406:
                piboxLogger(LOG_INFO, "Received 406 status code: UUID is invalid.\n");
                break;

            default:
                piboxLogger(LOG_ERROR, "Did not received 200 status code\n");
                break;
        }
    }
    else
    {
        // We failed to contact node.  Note this in the logs.
        if ( strlen(errbuf) > 0 )
            piboxLogger(LOG_ERROR, "%s%s", errbuf, (errbuf[strlen(errbuf)-1] != '\n') ? "\n" : "");
        else
            piboxLogger(LOG_ERROR, "%s", curl_easy_strerror(res));
    }
    curl_easy_cleanup(curl);
}
