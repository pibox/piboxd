/*******************************************************************************
 * PiBox service daemon
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <pibox/utils.h>

#include "piboxd.h"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* Daemon mode. */
            case 'd':
                cliOptions.flags |= CLI_DAEMON;
                break;

            /* -i: Enable Instrumentation */
            case 'i':
                cliOptions.flags |= CLI_INSTRUMENT;
                break;

            /* Enable configuration file. */
            case 'f':
                cliOptions.flags |= CLI_FILE;
                cliOptions.filename = optarg;
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* -I: Disable IoT use */
            case 'I':
                unsetCLIFlag( CLI_IOT );
                break;

            /* -r: Enable use of syslog, overriding console output. */
            case 'r':
                cliOptions.flags |= CLI_SYSLOG;
                break;

            /* -s: Disable SMB use */
            case 's':
                unsetCLIFlag( CLI_SMB );
                break;

            /* -S: Disable stream (webcam) use */
            case 'S':
                unsetCLIFlag( CLI_STREAM );
                break;

            /* -t: Stream timeout (in seconds). */
            case 't':
                cliOptions.timeout = atoi(optarg);
                break;

            /* -T: Use test files. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   loadConfig
 * Prototype:  void loadConfig( void )
 *
 * Description:
 * Read the configuration file and set options accordingly.
 *========================================================================*/
void
loadConfig( void )
{
    char            *tsave     = NULL;
    char            *tok       = NULL;
    struct stat     stat_buf;
    FILE            *fd;
    char            buf[MAXBUF];

    // Check that the configuration file exists.
    if ( stat(cliOptions.filename, &stat_buf) != 0 )
    {
        fprintf(stderr, "No such configuration file: %s\n", cliOptions.filename);
        return;
    }

    // Open the config file.
    fd = fopen(cliOptions.filename, "r");
    if ( fd == NULL )
    {
        fprintf(stderr, "Failed to open %s: %s\n", cliOptions.filename, strerror(errno));
        return;
    }
    
    // Parse file.
    while ( fgets(buf, MAXBUF-1, fd) != NULL )
    {
        fprintf(stderr, "Config file line: %s\n", buf);
        tok = strtok_r(buf, ":", &tsave);

        // Verbose output (verbose is a library variable).
        if ( strcmp(tok, "verbose") == 0 )
        {
            /* Set debug output level */
            tok = strtok_r(NULL, ":", &tsave);
            cliOptions.verbose = atoi(tok);
        }

        // Enable logging to local file.
        else if ( strcmp(tok, "debugfile") == 0 )
        {
            // Set debug file
            tok = strtok_r(NULL, ":", &tsave);
            cliOptions.logFile = g_strdup(tok);
            piboxStripNewline( cliOptions.logFile );
            cliOptions.flags |= CLI_LOGTOFILE;
        }

        // Disable SMB use
        else if ( strcmp(tok, "NOSMB") == 0 )
        {
            unsetCLIFlag( CLI_SMB );
            continue;
        }

        // Disable stream (webcam) use
        else if ( strcmp(tok, "NOSTREAM") == 0 )
        {
            unsetCLIFlag( CLI_STREAM );
            continue;
        }

        // Disable IoT use
        else if ( strcmp(tok, "NOIOT") == 0 )
        {
            unsetCLIFlag( CLI_IOT );
            continue;
        }

        // The mount command to use for SMB
        else if ( strcmp(tok, "SMBMOUNT") == 0 )
        {
            tok = strtok_r(NULL, ":", &tsave);
            cliOptions.smbMount = g_malloc0( strlen(tok) + 1 );
            sprintf( cliOptions.smbMount, "%s", tok );
            continue;
        }

        // The webcam command
        else if ( strcmp(tok, "WEBCAM") == 0 )
        {
            tok = strtok_r(NULL, ":", &tsave);
            cliOptions.webcam = g_malloc0( strlen(tok) + 1 );
            sprintf( cliOptions.webcam, "%s", tok );
            continue;
        }
    }

    fclose(fd);
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Are we running as root? */
    if ( getuid() == 0 )
        cliOptions.flags |= CLI_ROOT;

    /* Enable the servers */
    cliOptions.flags |= CLI_SERVER;
    cliOptions.flags |= CLI_MULTICAST;
    cliOptions.flags |= CLI_SMB;
    cliOptions.flags |= CLI_STREAM;
    cliOptions.flags |= CLI_IOT;

    /* Set the preferences file name */
    cliOptions.flags    |= CLI_FILE;
    cliOptions.filename  = F_CFG;

    /* Set the default stream timeout */
    cliOptions.timeout   = STREAM_TO;
}


/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

