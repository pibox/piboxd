/*******************************************************************************
 * PiBox service daemon
 *
 * cli.h:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define CLI_FILE        0x00001    // Enable file configuration
#define CLI_SERVER      0x00002    // Server Enabled - if not set, system is exiting.
#define CLI_DAEMON      0x00004    // Daemon mode
#define CLI_LOGTOFILE   0x00008    // Enable log to file
#define CLI_INSTRUMENT  0x00010    // Enable instrumentation
#define CLI_TEST        0x00020    // Enable test mode (read data files locally)
#define CLI_ROOT        0x00040    // Running as root
#define CLI_MULTICAST   0x00080    // Multicast server enabled
#define CLI_SMB         0x00100    // Enable SMB use
#define CLI_STREAM      0x00200    // Enable stream (webcam) use
#define CLI_IOT         0x00400    // Enable IoT registration
#define CLI_SYSLOG      0x00800    // Enable logging to syslog

#define RUN_DIR         "/var/run/piboxd"
#define LOCK_FILE       "/var/lock/piboxd.lock"
#define F_CFG           "/etc/piboxd.cfg"

// Default to 30 seconds for a stream to timeout
#define STREAM_TO       30

typedef struct _cli_t {
    int     flags;      // Enable/disable features
    char    *filename;  // If CLI_FILE, this is the filename to read
    int     verbose;    // Sets the verbosity level for the application
    char    *logFile;   // Name of local file to write log to
    int     lockfd;     // Lock file file descriptor
    int     timeout;    // Configured timeout for streams
    char    *smbMount;  // Command to use for doing smb mounts
    char    *webcam;    // Command to use for running webcam
} CLI_T;


/*========================================================================
 * Text strings 
 *=======================================================================*/

/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "diIrsSTf:l:t:v:"
#define USAGE \
"\n\
piboxd [ -diT | -f <filename> | -l <filename> | -t <timeout> | -v <level> | -h? ]\n\
where\n\
\n\
    -d              Daemon mode \n\
    -f              Configuration file (defaults to /etc/piboxd.cfg) \n\
    -i              Enable instrumentation (outputs debug instrumentation data) \n\
    -I              Disable IoT use \n\
    -r              Enable logging to syslog (overrides console output) \n\
    -s              Disable SMB use \n\
    -S              Disable stream (webcam) use \n\
    -T              Use test files (for debugging only) \n\
    -l filename     Enable local logging to named file \n\
    -t timeout      Set the stream timeout in seconds \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
\n\
"


/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void loadConfig( void );
void parseArgs(int argc, char **argv);
void initConfig( void );
void validateConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void loadConfig( void );
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern void validateConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
