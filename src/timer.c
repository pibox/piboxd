/*******************************************************************************
 * PiBox service daemon
 *
 * timer.c:  Expire streams with no recent heartbeats.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TIMER_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/timerfd.h>
#include <sys/select.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "piboxd.h"

static int timerIsRunning = 0;
static pthread_mutex_t timerProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t timerProcessorThread;
static int timerfd = -1;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isTimerProcessorRunning
 * Prototype:  int isTimerProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of timerIsRunning variable.
 *========================================================================*/
static int
isTimerProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &timerProcessorMutex );
    status = timerIsRunning;
    pthread_mutex_unlock( &timerProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setTimerProcessorRunning
 * Prototype:  int setTimerProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of timerIsRunning variable.
 *========================================================================*/
static void
setTimerProcessorRunning( int val )
{
    pthread_mutex_lock( &timerProcessorMutex );
    timerIsRunning = val;
    pthread_mutex_unlock( &timerProcessorMutex );
}

/*
 * ========================================================================
 * Name:   timerSetup
 * Prototype:  void timerSetup( void )
 *
 * Description:
 * Setup timer to expire queue entries.
 *
 * Notes:
 * Select() on timerfd to know when to run the timer processor.
 * ========================================================================
 */
static void
timerSetup()
{
    struct itimerspec timer = {
        {3,0},
        {3,0}
    };

    piboxLogger(LOG_TRACE1, "Entered.\n");

    /* Start the timer. */
    timerfd = timerfd_create(CLOCK_REALTIME, 0);
    if (timerfd == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to create timer fd: %s\n", strerror(errno));
        return;
    }
    piboxLogger(LOG_TRACE1, "Created timer.\n");

    if (timerfd_settime(timerfd, 0, &timer, NULL) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to setup timer: %s\n", strerror(errno));
    }
    piboxLogger(LOG_INFO, "Initialized timer.\n");
}

/*
 * ========================================================================
 * Name:   timerTeardown
 * Prototype:  void timerTeardown( void )
 *
 * Description:
 * Teardown timer that expires queue entries.
 * ========================================================================
 */
static void
timerTeardown()
{
    struct itimerspec timer;

    if ( timerfd == -1 )
        return;

    /* Clear timer configuration. */
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_nsec = 0;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_nsec = 0;

    if (timerfd_settime(timerfd, 0, &timer, NULL) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to teardown timer: %s\n", strerror(errno));
    }

    timerfd = -1;
}

/*========================================================================
 * Name:   timerProcessor
 * Prototype:  void timerProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from timer and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * This loop runs every 100ms but only runs the queue every 3 seconds.
 * This allows the thread to exit quickly when the daemon is shutting down
 * while giving remote users plenty of time to send heartbeats for their streams.
 *========================================================================*/
static void *
timerProcessor( void *arg )
{
    fd_set          rfds;
    struct timeval  tv;
    int             retval;
    char            buf[8];

    piboxLogger(LOG_TRACE1, "Entered.\n");

    if ( timerfd == -1 )
    {
        piboxLogger(LOG_ERROR, "timerd is not set, can't run timerProcessor.\n");
        return(0);
    }

    /* Run the queue periodically to expire abandoned streams. */
    setTimerProcessorRunning(1);
    while( isCLIFlagSet(CLI_SERVER) )
    {
        /* Watch the timer fd for activity. */
        FD_ZERO(&rfds);
        FD_SET(timerfd, &rfds);

        /*
         * Reset select()'s 3+ second timeout on each run.
         * This is just a bit longer than the timer but gives us an escape if the timer is disabled.
         */
        tv.tv_sec = 3;
        tv.tv_usec = 500000;
        retval = select(timerfd+1, &rfds, NULL, NULL, &tv);

        /* Ignore select errors or timer not expired. */
        if ( retval < 0 )
        {
            piboxLogger(LOG_TRACE5, "select: %s\n", strerror(errno));
            continue;
        }

        /* Clear the timer */
        read(timerfd, buf, 8);

        /* Expire processes where needed. */
        piboxLogger(LOG_TRACE1, "Calling expireProcQueue().\n");
        expireProcQueue();
    }

    piboxLogger(LOG_INFO, "%s: Timer thread is exiting.\n", PROG);
    setTimerProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startTimerProcessor
 * Prototype:  void startTimerProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownTimerProcessor().
 *========================================================================*/
void
startTimerProcessor( void )
{
    int rc;

    if ( timerfd != -1 )
        return;

    /* Create a timer to run the processor. */
    piboxLogger(LOG_INFO, "Starting timer.\n");
    timerSetup();

    /* Create a thread to expire streams. */
    rc = pthread_create(&timerProcessorThread, NULL, &timerProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create timer thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }

    piboxLogger(LOG_INFO, "%s: Started timer thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownTimerProcessor
 * Prototype:  void shutdownTimerProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownTimerProcessor( void )
{
    int timeOut = 1;

    timerTeardown();
    while ( isTimerProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_INFO, "%s: Timed out waiting on timer thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(timerProcessorThread);
    piboxLogger(LOG_INFO, "%s: timerProcessor shut down.\n", PROG);
}
