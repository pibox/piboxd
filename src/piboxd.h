/*******************************************************************************
 * I/O Management Module
 *
 * main.h:  Setup and initialization controller
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MAIN_H
#define MAIN_H

#include <unistd.h>
#include <time.h>
#include <glib.h>
#include <netinet/in.h>
#include <pibox/log.h>

/*========================================================================
 * Type definitions
 *=======================================================================*/

/*
 * Data structure used in message passing between applications on PiBox.
 */
typedef struct _pibox_msg {
    struct _pibox_msg   *next;
    unsigned int        header;         // 4 byte header: method (16h) | action (8l) | type (8l)
    int                 size;           // size of request payload
    char                tag[37];        // Unique identifier for message/request
    char                *payload;       // payload, such as filename to read from
    int                 active;         // Not used
    pid_t               pid;            // Process running for this request
    time_t              timestamp;      // Last contact time
    int                 timeout;        // In minutes
    int                 insd;           // Socket from which message came, if required.
    unsigned int        flags;          // Routing info, like "this is a multicast message"
} PIBOX_MSG_T;

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "piboxd"
// #define TRUE        1
// #define FALSE       0
#define MAXBUF      4096

// Generic return codes
#define N_OK        1
#define N_PASS      2
#define N_FAIL      3

/*
 * ==============================================================================
 * Message types, actions and subactions need to be migrated to piboxlib/pmsg.h
 * They should be there already - just delete them here and retest everything!
 * ==============================================================================
 */
#if 0
// Message types (byte 1 of PIBOX_MSG_T header)
#define MT_STREAM    1
#define MT_HEARTBEAT 2
#define MT_NET       3
#define MT_PW        4
#define MT_DEVICE    5		// DEPRECATED: REST API handles IoT device queries
#define MT_SYS       6
#define MT_IRONMAN   7

// Message actions (byte 2 of PIBOX_MSG_T header): MT_STREAM
#define MA_START     1
#define MA_END       2
#define MA_START_LOW 3

// Message actions (byte 2 of PIBOX_MSG_T header): MT_NET
#define MA_GETIFLIST        1   // Retrieve list of interfaces
#define MA_GETIF            2   // Get specified IF configuration (IPv4, gateway, etc.)
#define MA_GETDNS           3   // Retrieve DNS configuration
#define MA_GETNETTYPE       4   // Are we a wireless client or an access point?
#define MA_GETAP            5   // Get Access Point SSID, channel, password
#define MA_GETWIRELESS      6   // Get Wireless SSID, security type and password
#define MA_SETIPV4          7   // Set IPV4 configuration (IPv4, nameserver, gateway, etc.)
#define MA_SETAP            8   // Set Access Point SSID, channel, password
#define MA_SETWIRELESS      9   // Set Wireless SSID, security type and password
#define MA_SETNETTYPE       10  // Set network type: wireless client or access point
#define MA_GETMAC           11  // Retrieve MAC address for specified interface
#define MA_GETIP            12  // Retrieve IP address for specified interface
#define MA_MFLIST           13  // Write MAC address filter list
#define MA_RESTART          14  // Restart network

// MAC Filter (MA_MFLIST) sub types
#define MS_GET              1   // Get mac filter list (hostapd.accept)
#define MS_SET              2   // Set mac filter list (hostapd.accept)

// Message actions (byte 2 of PIBOX_MSG_T header): MT_PW
#define MA_SAVE     1
#define MA_GET      2
#define MA_DEL      3

// Message actions (byte 2 of PIBOX_MSG_T header): MT_DEVICE
#define MA_GETALL   1
#define MA_GET      2
#define MA_SET      3

// Message actions (byte 2 of PIBOX_MSG_T header): MT_SYS
#define MA_REBOOT   1

// Message actions (byte 2 of PIBOX_MSG_T header): MT_IRONMAN
#define MA_PAIR_IOT     1
#define MA_PAIR_JARVIS  2
#endif

/*
 *=======================================================================
 * End of message types/actions/subactions
 *=======================================================================
 */

#define Q_IN        1       // The inbound message queue
#define Q_PROC      2       // The processed message queue

// Flags used in PIBOX_MSG_T
#define PIBOX_MULTICAST     0x00000001

// Command used for rebooting the system.
#define REBOOT_CMD      "/sbin/reboot"

// Command used for keyboard plug events.
#define KEYBOARD_CMD    "/usr/bin/keyboard-fe.sh"

// Command used for display rotate events.
#define PIROTATE_CMD    "/usr/bin/pirotate.sh"

// PID file: Also referenced by the associated init script.
#define PIDFILE         "/var/run/piboxd.pid"

// Domain socket path
#define SERVER_SOCKET_PATH  "/var/run/piboxd.sock"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef MAIN_C
int     daemonEnabled = 0;
char    errBuf[256];
#else
extern int      daemonEnabled;
extern char     errBuf[];
#endif /* MAIN_C */

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include "cli.h"
#include "utils.h"
#include "msgProcessor.h"
#include "regProcessor.h"
#include "msgQueue.h"
#include "queueProcessor.h"
#include "stream.h"
#include "timer.h"
#include "smb.h"
#include "net.h"
#include "iot.h"
#include "eth.h"

#endif /* !MAIN_H */

