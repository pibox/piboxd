/*******************************************************************************
 * PiBox service daemon
 *
 * iot.h:  Manage IoT devices
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef IOT_H
#define IOT_H

/*
 * ========================================================================
 * Typedefs
 * =======================================================================
 */

/* Were IoT device registrations stored. */
#define IOT_DIR     "/etc/ironman/iot"

/*
 * ========================================================================
 * Globals
 * =======================================================================
 */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef IOT_C
extern int      findRegistration( PIBOX_MSG_T *msg );
extern void     registerDevice( PIBOX_MSG_T *msg );
#endif /* IOT_C */

#endif /* !IOT_H */
