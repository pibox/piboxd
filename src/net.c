/*******************************************************************************
 * PiBox service daemon
 *
 * net.c:  Network file processing functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define NET_C

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <glib.h>
#include <piboxnet/pnc.h>
#include <pibox/utils.h>

#include "piboxd.h"

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Getters: retrieve data from network files.
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   buildIFNameString
 * Prototype:  void buildIFNameString( gpointer item, gpointer user_data )
 *
 * Description:
 * Build the interface name string to send back to the caller.
 *========================================================================*/
void
buildIFNameString( gpointer item, gpointer user_data )
{
    gchar **buf = (gchar **)user_data;
    gchar *newbuf = NULL;

    piboxLogger(LOG_INFO, "Item: %s\n", (gchar *)item);
    if ( *buf == NULL )
    {
        piboxLogger(LOG_INFO, "Buf is null.\n");
        *buf = g_strconcat((gchar *)item, (gchar *)" ", NULL);
    }
    else
    {
        piboxLogger(LOG_INFO, "adding item.\n");
        newbuf = g_strconcat(*buf, (gchar *)item, " ", NULL);
        g_free(*buf);
        *buf = newbuf;
    }
}

/*========================================================================
 * Name:   getIFList
 * Prototype:  void getIFList( PIBOX_MSG_T * )
 *
 * Description:
 * Get the list of interfaces available on the current system.
 * Write them to the socket associated with the message as 
 * a series of space delimited names.
 *========================================================================*/
void
getIFList( PIBOX_MSG_T *msg )
{
    GSList *physicalInterfaces  = NULL;
    gchar *buf = NULL;
    gchar *newbuf = NULL;
    int bytes = 0;

    piboxLogger(LOG_INFO, "Entered getIFLIst.\n");

    // Get the list of physical interfaces.
    piboxLogger(LOG_INFO, "Calling loadNet.\n");
    pncLoadNet();
    physicalInterfaces = pncGetNetInterfaces();
    g_slist_foreach(physicalInterfaces, buildIFNameString, &buf);

    if ( buf != NULL )
    {
        piboxLogger(LOG_INFO, "buf is not empty.\n");
        newbuf = g_strconcat(buf, "\n", NULL);
        g_free(buf);
        buf = newbuf;
        piboxLogger(LOG_INFO, "buf = %s\n", (char *)buf);
    }
    else
        piboxLogger(LOG_INFO, "buf = empty\n");

    // Send back the names of the interfaces.
    if ( msg->insd != -1 )
    {
        piboxLogger(LOG_INFO, "getIFList: Writing to socket.\n");
        bytes = write( msg->insd, (char *)buf, strlen(buf) );
        if ( bytes != strlen(buf) )
            piboxLogger(LOG_ERROR, "write to socket failed.\n");
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");

    // Clean up
    piboxLogger(LOG_INFO, "getIFList: free buf.\n");
    if ( buf != NULL )
        g_free(buf);
    piboxLogger(LOG_INFO, "getIF: clearNetInterfaces.\n");
    pncClearNetInterfaces();
}

/*========================================================================
 * Name:   getIF
 * Prototype:  void getIF( PIBOX_MSG_T * )
 *
 * Description:
 * Get the configuration of the named interface.
 * Write the configuration to the socket associated with the message as 
 * a series of newline delimited fields.
 *========================================================================*/
void
getIF( PIBOX_MSG_T *msg )
{
    gchar *buf = NULL;
    gchar *newbuf = NULL;
    gchar *field = NULL;
    int bytes = 0;

    piboxLogger(LOG_INFO, "Entered getIF.\n");
    buf = g_malloc0(1);

    // Get the list of logical interfaces.
    piboxLogger(LOG_INFO, "Calling loadNet.\n");
    pncLoadNet();
    pncLoadInterfaces();
    piboxLogger(LOG_INFO, "getIF: Searching for %s\n", msg->payload);
    if ( pncGetInterfaceState(msg->payload) != -1 )
    {
        field = pncGetInterfaceField(msg->payload, PNC_ID_IF_NAME);
        if ( field != NULL )
        {
            newbuf = g_strconcat(buf, "NAME:", field, "\n", NULL);
            g_free(buf);
            buf = newbuf;
            g_free(field);
        }

        field = pncGetInterfaceField(msg->payload, PNC_ID_IF_NETTYPE);
        if ( field != NULL )
        {
            newbuf = g_strconcat(buf, "NETWORKTYPE:", field, "\n", NULL);
            g_free(buf);
            buf = newbuf;
            g_free(field);
        }

        field = pncGetInterfaceField(msg->payload, PNC_ID_IF_ADDRTYPE);
        if ( field != NULL )
        {
            newbuf = g_strconcat(buf, "ADDRESSTYPE:", field, "\n", NULL);
            g_free(buf);
            buf = newbuf;
            g_free(field);
        }

        field = pncGetInterfaceField(msg->payload, PNC_ID_IF_ADDR);
        if ( field != NULL )
        {
            newbuf = g_strconcat(buf, "ADDRESS:", field, "\n", NULL);
            g_free(buf);
            buf = newbuf;
            g_free(field);
        }

        field = pncGetInterfaceField(msg->payload, PNC_ID_IF_MASK);
        if ( field != NULL )
        {
            newbuf = g_strconcat(buf, "NETMASK:", field, "\n", NULL);
            g_free(buf);
            buf = newbuf;
            g_free(field);
        }

        field = pncGetInterfaceField(msg->payload, PNC_ID_IF_GATEWAY);
        if ( field != NULL )
        {
            newbuf = g_strconcat(buf, "GATEWAY:", field, "\n", NULL);
            g_free(buf);
            buf = newbuf;
            g_free(field);
        }

        if ( pncGetInterfaceState(msg->payload) == 1 )
        {
            newbuf = g_strconcat(buf, "ENABLED:1", "\n", NULL);
            g_free(buf);
            buf = newbuf;
        }
        else
        {
            newbuf = g_strconcat(buf, "ENABLED:0", "\n", NULL);
            g_free(buf);
            buf = newbuf;
        }
        piboxLogger(LOG_INFO, "buf = %s\n", buf);
    }
    else
    {
        piboxLogger(LOG_INFO, "buf = empty\n");
        newbuf = g_strconcat(buf, "NOCONFIG:", "\n", NULL);
        g_free(buf);
        buf = newbuf;
    }

    // Send back the interface configuration or a notice it doesn't exist.
    if ( msg->insd != -1 )
    {
        piboxLogger(LOG_INFO, "getIF: Writing to socket.\n");
        bytes = write( msg->insd, (char *)buf, strlen(buf) );
        if ( bytes == -1 )
            piboxLogger(LOG_ERROR, "error writing to socket: %s\n", strerror(errno));
        else if ( bytes != strlen(buf) )
            piboxLogger(LOG_ERROR, "short write to socket failed.\n");
        piboxLogger(LOG_INFO, "getIF: Done writing to socket.\n");
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");

    // Clean up
    piboxLogger(LOG_INFO, "getIF: free buf.\n");
    if ( buf != NULL )
        g_free(buf);
    piboxLogger(LOG_INFO, "getIF: clearInterfaces.\n");
    pncClearInterfaces();
    piboxLogger(LOG_INFO, "getIF: clearNetInterfaces.\n");
    pncClearNetInterfaces();
}

/*========================================================================
 * Name:   getMAC
 * Prototype:  void getMAC( PIBOX_MSG_T * )
 *
 * Description:
 * Finds the MAC address for the specified interface and returns it.
 *========================================================================*/
void
getMAC( PIBOX_MSG_T *msg )
{
    struct ifreq ifr;;
    int sock, j, k;
    char mac[32];
    char *name;
    int bytes = 0;

    // Pull the interface name from the payload.
    piboxLogger(LOG_INFO, "getMAC: Searching for %s\n", msg->payload);
    name = msg->payload;

    // Use that to find its mac address with an ioctl.
    memset(mac, 0, 32);
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if ( sock == -1 )
        piboxLogger(LOG_ERROR, "Couldn't get socket for mac address.\n");
    else
    {   
        strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name)-1);
        ifr.ifr_name[ sizeof(ifr.ifr_name)-1 ] = '\0';
        if ( ioctl(sock, SIOCGIFHWADDR, &ifr) == -1 )
            piboxLogger(LOG_ERROR, "Failed ioctl to get mac address.\n");
        else
        {
            for (j=0, k=0; j<6; j++)
            {
                k+=snprintf(mac+k, sizeof(mac)-k-1, j ? ":%02X" : "%02X",
                    (int)(unsigned int)(unsigned char)ifr.ifr_hwaddr.sa_data[j]);
            }
            mac[sizeof(mac)-1]='\0';
        }
    }
    if ( strlen(mac) == 0 )
        sprintf(mac, " ");
    piboxLogger(LOG_INFO, "%s MAC: %s\n", name, mac);

    // Send back the MAC address or a blank field if it can't be found.
    if ( msg->insd != -1 )
    {
        bytes = write( msg->insd, (char *)mac, strlen(mac) );
        if ( bytes == -1 )
            piboxLogger(LOG_ERROR, "error writing to socket: %s\n", strerror(errno));
        else if ( bytes != strlen(mac) )
            piboxLogger(LOG_ERROR, "short write to socket failed.\n");
        bytes = write( msg->insd, "\n", 1 );
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");
    close(sock);
}

/*========================================================================
 * Name:   getIP
 * Prototype:  void getIP( PIBOX_MSG_T * )
 *
 * Description:
 * Finds the IP address for the specified interface and returns it.
 *========================================================================*/
void
getIP( PIBOX_MSG_T *msg )
{
    struct ifreq ifr;;
    int sock;
    char ip[16];
    char *name;
    int bytes = 0;
    struct sockaddr_in *ipaddr = NULL;

    // Pull the interface name from the payload.
    piboxLogger(LOG_INFO, "getIP: Searching for %s\n", msg->payload);
    name = msg->payload;

    // Use that to find its mac address with an ioctl.
    memset(ip, 0, 16);
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if ( sock == -1 )
        piboxLogger(LOG_ERROR, "Couldn't get socket for ip address.\n");
    else
    {   
        strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name)-1);
        ifr.ifr_name[ sizeof(ifr.ifr_name)-1 ] = '\0';
        if ( ioctl(sock, SIOCGIFADDR, &ifr) == -1 )
            piboxLogger(LOG_ERROR, "Failed ioctl to get ip address.\n");
        else
            ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
    }
    if ( ipaddr == NULL )
        sprintf(ip, " ");
    else
        sprintf(ip, "%s", inet_ntoa(ipaddr->sin_addr));
    piboxLogger(LOG_INFO, "%s IP: %s\n", name, ip);

    // Send back the IP address or a blank field if it can't be found.
    if ( msg->insd != -1 )
    {
        bytes = write( msg->insd, (char *)ip, strlen(ip) );
        if ( bytes == -1 )
            piboxLogger(LOG_ERROR, "error writing to socket: %s\n", strerror(errno));
        else if ( bytes != strlen(ip) )
            piboxLogger(LOG_ERROR, "short write to socket failed.\n");
        bytes = write( msg->insd, "\n", 1 );
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");
    close(sock);
}

/*========================================================================
 * Name:   buildDNSString
 * Prototype:  void buildDNSString( gpointer item, gpointer user_data )
 *
 * Description:
 * Build the DNS string to send back to the caller.
 *========================================================================*/
void
buildDNSString( gpointer item, gpointer user_data )
{
    gchar **buf = (gchar **)user_data;
    gchar *newbuf = NULL;
    if ( item == NULL )
        return;
    if ( *buf == NULL )
        return;
    piboxLogger(LOG_INFO, "Item: %s\n", (gchar *)item);
    newbuf = g_strconcat(*buf, g_strchomp( (gchar *)item ), " ", NULL);
    g_free(*buf);
    *buf = newbuf;
}

/*========================================================================
 * Name:   getDNS
 * Prototype:  void getDNS( PIBOX_MSG_T * )
 *
 * Description:
 * Get the list of nameservers.
 * Write them to the socket associated with the message as 
 * a series of space delimited IP addresses.
 *========================================================================*/
void
getDNS( PIBOX_MSG_T *msg )
{
    GSList *nameservers  = NULL;
    gchar *buf = NULL;
    gchar *newbuf = NULL;
    int bytes = 0;

    piboxLogger(LOG_INFO, "Entered getDNS.\n");
    buf = g_malloc0(1);

    // Get the list of physical interfaces.
    piboxLogger(LOG_INFO, "Calling loadResolv.\n");
    pncLoadResolv();
    nameservers = pncGetNameservers();
    g_slist_foreach(nameservers, buildDNSString, &buf);

    if ( strlen(buf) > 1 )
    {
        newbuf = g_strconcat(buf, "\n", NULL);
        g_free(buf);
        buf = newbuf;
        piboxLogger(LOG_INFO, "buf = %s\n", (char *)buf);
    }
    else
        piboxLogger(LOG_INFO, "buf = empty\n");

    // Send back the names of the interfaces.
    if ( (strlen(buf)>1) && (msg->insd != -1) )
    {
        piboxLogger(LOG_INFO, "getDNS: Writing to socket.\n");
        bytes = write( msg->insd, (char *)buf, strlen(buf) );
        if ( bytes != strlen(buf) )
            piboxLogger(LOG_ERROR, "write to socket failed.\n");
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");

    // Clean up
    piboxLogger(LOG_INFO, "getDNS: free buf.\n");
    if ( buf != NULL )
        g_free(buf);
    piboxLogger(LOG_INFO, "getDNS: clearResolv.\n");
    pncClearResolv();
}

/*========================================================================
 * Name:   getNetType
 * Prototype:  void getNetType( PIBOX_MSG_T * )
 *
 * Description:
 * Read the current network type (wireless client or access point) 
 * and send it back.
 *========================================================================*/
void
getNetType( PIBOX_MSG_T *msg )
{
    gchar *mode = "wireless\n";
    int bytes;
    char *filename;

    // We need a newline or PHP on the other end won't see full message.
    filename = pncGetFilename( PNC_ID_NETTYPE );
    if ( filename != NULL )
    {
        switch ( pncReadNetType( filename ) )
        {
            case PNC_ID_WIRELESS: mode = "wireless\n"; break;
            case PNC_ID_WAP     : mode = "wap\n"; break;
            default             : mode = "wireless\n"; break;
        }
        g_free(filename);
    }

    // Send back the mode.
    if ( msg->insd != -1 )
    {
        piboxLogger(LOG_INFO, "getMode: Writing to socket: %s", mode);
        bytes = write( msg->insd, (char *)mode, strlen(mode) );
        if ( bytes != strlen(mode) )
            piboxLogger(LOG_ERROR, "write to socket failed.\n");
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");
}

/*========================================================================
 * Name:   setNetType
 * Prototype:  void setNetType( PIBOX_MSG_T * )
 *
 * Description:
 * Save the current network type (wireless client or access point).
 *========================================================================*/
void
setNetType( PIBOX_MSG_T *msg )
{
    gchar *mode = msg->payload;
    char *filename = NULL;

    filename = pncGetFilename( PNC_ID_NETTYPE );
    if ( filename != NULL )
    {
        if ( strcasecmp(mode,"client") == 0 )
            pncSaveNetType(filename, PNC_ID_WIRELESS);
        else if ( strcasecmp(mode,"wap") == 0 )
            pncSaveNetType(filename, PNC_ID_WAP);
        else 
            piboxLogger(LOG_INFO, "setMode: invalid mode %s; ignoring.\n", mode);
        g_free( filename );
    }
}

/*========================================================================
 * Name:   setIPV4
 * Prototype:  void setIPV4( PIBOX_MSG_T * )
 *
 * Description:
 * Save interfaces configuration data.
 *
 * Notes:
 * Order of fields for payload is as follows.
 * 1. Interface (name of interface)
 * 2. Enabled state (yes/no)
 * 3. Configuration type (DHCP/Static)
 * 4. IP Address
 * 5. Netmask
 * 6. Gateway
 * 7. DNS 0
 * 8. DNS 1
 * 9. DNS 2
 * Missing fields are either NULL or contain the text string "-1".
 *========================================================================*/
void
setIPV4( PIBOX_MSG_T *msg )
{
    char *field;
    char *saveptr;
    char *ifaceName = NULL;
    char *estate = NULL;
    char *ctype = NULL;
    char *ipaddr = NULL;
    char *netmask = NULL;
    char *gateway = NULL;
    char *dns0 = NULL;
    char *dns1 = NULL;
    char *dns2 = NULL;
    char *filename = NULL;
    int  idx = 0;

    piboxLogger(LOG_INFO, "setIPV4 entered\n");

    // tokenize the payload and find valid fields to update.
    field = strtok_r(msg->payload, ":", &saveptr);
    while(field != NULL)
    {
        switch(idx)
        {
            // Interface (name of interface)
            case 0:
                // Validate interface name
                pncLoadNet();
                if ( !pncNetInterfaceValid(field) )
                {
                    piboxLogger(LOG_INFO, "No such interface: %s\n", field);
                    return;
                }
                ifaceName = field;
                break;

            // Enabled state (yes/no)
            case 1:
                estate = field;
                break;

            // Configuration type (DHCP/Static)
            case 2:
                ctype = field;
                break;

            // IP Address
            case 3:
                if ( piboxValidIPAddress(field) )
                    ipaddr = field;
                break;

            // Netmask
            case 4:
                if ( piboxValidIPAddress(field) )
                    netmask = field;
                break;

            // Gateway
            case 5:
                if ( piboxValidIPAddress(field) )
                    gateway = field;
                break;

            // DNS 0
            case 6:
                if ( piboxValidIPAddress(field) )
                    dns0 = field;
                break;

            // DNS 1
            case 7:
                if ( piboxValidIPAddress(field) )
                    dns1 = field;
                break;

            // DNS 2
            case 8:
                if ( piboxValidIPAddress(field) )
                    dns2 = field;
                break;
        }

        // Move to next field
        idx++;
        field = strtok_r(NULL, ":", &saveptr);
    }

    piboxLogger(LOG_INFO, "Interface updates: \n"
            "Name: %s\n"
            "Address Type: %s\n"
            "Enabled: %s\n"
            "Address: %s\n"
            "Netmask: %s\n"
            "Gateway: %s\n"
            "DNS 0: %s\n"
            "DNS 1: %s\n"
            "DNS 2: %s\n",
            ifaceName, 
            ctype, 
            estate, 
            ipaddr, 
            netmask, 
            gateway, 
            dns0, 
            dns1, 
            dns2
            );

    // Validate configuration
    if ( ctype == NULL )
    {
        piboxLogger(LOG_INFO, "Missing config type.  Ignoring.\n");
        return;
    }
    if ( (strcasecmp(ctype, "static")==0) && ((ipaddr==NULL) || (netmask==NULL) || (gateway==NULL)) )
    {
        piboxLogger(LOG_INFO, "Static config is incomplete.  Ignoring.\n");
        return;
    }

    // If setting to enabled, create or update the entry.
    pncLoadInterfaces();
    if ( (estate!=NULL) && (strcasecmp(estate, "yes")==0) )
    {
        piboxLogger(LOG_INFO, "Enabled state is YES\n");

        // Update the named interface
        if ( pncGetInterfaceState(ifaceName) != -1 )
            piboxLogger(LOG_INFO, "Found existing entry for %s\n", ifaceName);
        else
        {
            // Create an entry for the interface.
            piboxLogger(LOG_INFO, "Creating new entry for %s\n", ifaceName);
            pncNewInterface(ifaceName);
            pncSetInterfaceField(ifaceName, PNC_ID_IF_NETTYPE, "inet");
        }

        pncSetInterfaceState(ifaceName, 1);
        pncSetInterfaceField(ifaceName, PNC_ID_IF_ADDRTYPE, ctype);
        if ( strcasecmp(ctype,"static") == 0 )
        {
            pncSetInterfaceField(ifaceName, PNC_ID_IF_ADDR, ipaddr);
            pncSetInterfaceField(ifaceName, PNC_ID_IF_MASK, netmask);
            pncSetInterfaceField(ifaceName, PNC_ID_IF_GATEWAY, gateway);
        }
    }
    else
    {
        // Not enabled, so remove entry from file.
        piboxLogger(LOG_INFO, "Not enabled\n");
        pncRemoveInterface(ifaceName);
    }

    // Write interfaces to disk.
    piboxLogger(LOG_INFO, "Saving interfaces\n");
    filename = pncGetFilename( PNC_ID_INTERFACES );
    if ( filename != NULL ) 
    {
        pncSaveInterface(filename);
        g_free(filename);
    }

    // Save DNS  
    piboxLogger(LOG_INFO, "Saving DNS updates\n");
    filename = pncGetFilename( PNC_ID_RESOLV );
    if ( filename != NULL ) 
    {
        pncLoadResolv();
        if ( strcasecmp(ctype,"static") == 0 )
        {
            piboxLogger(LOG_INFO, "%s: static; dns = %s %s %s\n", 
                __FUNCTION__, (dns0!=NULL)?dns0:"NULL", (dns1!=NULL)?dns1:"NULL", (dns2!=NULL)?dns2:"NULL");
            pncSetResolvField(dns0, dns1, dns2);
        }
        else
        {
            piboxLogger(LOG_INFO, "%s: dhcp; resetting dns\n", __FUNCTION__);
            pncSetResolvField(NULL, NULL, NULL);
        }
        pncSaveDNS(filename);
        g_free(filename);
    }
}

/*========================================================================
 * Name:   getAP
 * Prototype:  void getAP( PIBOX_MSG_T * )
 *
 * Description:
 * Retrieve host access point configuration data, specifically the 
 * SSID, channel and password.
 *========================================================================*/
void
getAP( PIBOX_MSG_T *msg )
{
    char   *ssid    = NULL;
    char   *channel = NULL;
    char   *pw      = NULL;
    char   *buf     = NULL;
    char   *filename= NULL;
    int    bytes;

    pncLoadHostAP();

    // Grab SSID
    ssid = pncGetHostAPField( PNC_ID_HAP_SSID );
    if ( ssid == NULL )
    {
        ssid = g_malloc0(2);
        memset(ssid, 0, 1);
    }

    // Grab channel
    channel = pncGetHostAPField( PNC_ID_HAP_CHANNEL );
    if ( channel == NULL )
    {
        channel = g_malloc0(2);
        memset(channel, 0, 1);
    }

    // Grab Password
    filename = pncGetHostAPField( PNC_ID_HAP_WPA_PSK_FILE );
    if ( filename != NULL )
    {
        pw = pncGetHostAPPassword( filename );
        g_free(filename);
    }
    if ( pw == NULL ) 
    {
        pw = g_malloc0(2);
        memset(pw, 0, 1);
    }

    buf = g_malloc0(strlen(ssid) + strlen(channel) + strlen(pw) + 4);
    sprintf(buf, "%s:%s:%s\n", ssid, channel, pw);
    free(pw);
    free(channel);
    free(ssid);

    // Send back the configuration
    if ( (strlen(buf)>1) && (msg->insd != -1) )
    {
        piboxLogger(LOG_INFO, "getWireless: Writing to socket: %s, %d\n", buf, msg->insd);
        bytes = write( msg->insd, (char *)buf, strlen(buf) );
        if ( bytes != strlen(buf) )
        {
            piboxLogger(LOG_ERROR, "write to socket failed.\n");
            goto wpaCleanup;
        }
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");

wpaCleanup:
    free(buf);
    pncClearHostAP();
}

/*========================================================================
 * Name:   setAP
 * Prototype:  void setAP( PIBOX_MSG_T * )
 *
 * Description:
 * Save host access point configuration data.
 *
 * Note:
 * Inbound message format is:
 * ssid:channel:pw:baseaddress<:interface>
 * The interace is optional.
 *========================================================================*/
void
setAP( PIBOX_MSG_T *msg )
{
    char *filename = NULL;
    char *field;
    char *saveptr = NULL;
    int  idx = 0;
    int  apidx = 0;

    piboxLogger(LOG_INFO, "setAP entered\n");
    pncLoadNet();
    piboxLogger(LOG_INFO, "loaded physical network interface names\n");
    pncLoadHostAP();
    piboxLogger(LOG_INFO, "loaded hostapd file\n");
    pncLoadInterfaces();
    piboxLogger(LOG_INFO, "loaded interfaces file\n");
    field = strtok_r(msg->payload, ":", &saveptr);
    while(field != NULL)
    {
        switch(idx)
        {
            case 0: apidx = PNC_ID_HAP_SSID; break;
            case 1: apidx = PNC_ID_HAP_CHANNEL; break;
            case 2: apidx = PNC_ID_HAP_PW; break;
            case 3: apidx = PNC_ID_HAP_NET; break;
            case 4: apidx = PNC_ID_HAP_INTERFACE; break;
        }
        piboxLogger(LOG_INFO, "setAP processing apidx: %d\n", apidx);
        pncSetHostAPField(apidx, field);

        // Move to next field
        idx++;
        field = strtok_r(NULL, ":", &saveptr);
    }

    // Save the updates
    piboxLogger(LOG_INFO, "setAP saving updates.\n");
    filename = pncGetFilename( PNC_ID_HOSTAPDCONF );
    if ( filename != NULL )
    {
        pncSaveHostAP(filename);
        g_free(filename);
    }
    pncClearHostAP();
}

/*========================================================================
 * Name:   getWireless
 * Prototype:  void getWireless( PIBOX_MSG_T * )
 *
 * Description:
 * Retrieve wpa_supplicant.conf data, specifically the 
 * SSID, Security type and password.
 *========================================================================*/
void
getWireless( PIBOX_MSG_T *msg )
{
    gchar  *ssid = NULL;
    gchar  *sect = NULL;
    gchar  *pw   = NULL;
    gchar  *buf  = NULL;
    int    bytes;

    pncLoadWPA();

    // Grab SSID
    ssid = pncGetWPAField( PNC_ID_WPA_SSID );
    if ( ssid == NULL ) 
        ssid = g_strdup("");

    // Grab Security type
    sect = pncGetWPAField( PNC_ID_WPA_KEYMGMT );
    if ( sect == NULL )
        sect = g_strdup("");

    // Grab Password
    pw = pncGetWPAField( PNC_ID_WPA_PSK );
    if ( pw == NULL ) 
        pw = g_strdup("");

    buf = g_malloc0(strlen(ssid) + strlen(sect) + strlen(pw) + 4);
    sprintf(buf, "%s:%s:%s\n", ssid, sect, pw);
    g_free(ssid);
    g_free(sect);
    g_free(pw);

    // Send back the configuration
    if ( (strlen(buf)>1) && (msg->insd != -1) )
    {
        piboxLogger(LOG_INFO, "getWireless: Writing to socket: %s, %d\n", buf, msg->insd);
        bytes = write( msg->insd, (char *)buf, strlen(buf) );
        if ( bytes != strlen(buf) )
        {
            piboxLogger(LOG_ERROR, "write to socket failed.\n");
            goto wpaCleanup;
        }
    }
    else
        piboxLogger(LOG_INFO, "Skipping write to socket.\n");

wpaCleanup:
    g_free(buf);
    pncClearWPA();
}

/*========================================================================
 * Name:   setWireless
 * Prototype:  void setWireless( PIBOX_MSG_T * )
 *
 * Description:
 * Save wifi (internet connection, re: wpa_supplicant) configuration data.
 *========================================================================*/
void
setWireless( PIBOX_MSG_T *msg )
{
    char *filename = NULL;
    char *field;
    char *saveptr = NULL;
    int  idx = 0;
    int  doUpdate = 0;
    int  wpaIdx = -1;
    char fields = 0x7;

    piboxLogger(LOG_INFO, "Entered\n");

    /*
     * Validate payload - must have
     * 1. two colons
     * 2. First character can't be a colon.
     */
    field = msg->payload;
    while (*field != '\0')
    {
        if ( *field == ':' )
            idx++;
        field++;
    }
    if ( idx != 2 )
    {
        piboxLogger(LOG_ERROR, "Payload doesn't have right number of fields (has %d). Ignoring.\n", idx);
        return;
    }
    if ( *(msg->payload) == ':' )
    {
        piboxLogger(LOG_ERROR, "Missing SSID. Ignoring.\n");
        return;
    }

    idx=0;
    pncLoadWPA();
    piboxLogger(LOG_INFO, "payload: %s\n", msg->payload);
    field = strtok_r(msg->payload, ":", &saveptr);
    while(field != NULL)
    {
        piboxLogger(LOG_INFO, "field: %s\n", field);
        doUpdate = 0;
        switch(idx)
        {
            // SSID
            case 0:
                piboxLogger(LOG_INFO, "SSID: *%s*\n", field);
                if ( strlen(field)>=4 )
                {
                    wpaIdx = PNC_ID_WPA_SSID;
                    fields &= 0x6;
                    doUpdate++;
                }
                else
                {
                    piboxLogger(LOG_ERROR, "SSID too short (minimum 4 characters). WPA config not saved.\n");
                }
                break;

            // Security
            case 1:
                piboxLogger(LOG_INFO, "Security: *%s*\n", field);
                if ( strcasestr((char *)field, "Personal") || strcasestr((char *)field, "Enterprise"))
                {
                    wpaIdx = PNC_ID_WPA_PROTO;
                    fields &= 0x5;
                    doUpdate++;
                }
                else
                {
                    piboxLogger(LOG_ERROR, "Invalid security type. WPA config not saved.\n");
                }
                break;

            // PW
            case 2:
                piboxLogger(LOG_INFO, "Password: *%s*\n", field);
                if ( strlen(field)>=7 )
                {
                    wpaIdx = PNC_ID_WPA_PSK;
                    fields &= 0x3;
                    doUpdate++;
                }
                else
                {
                    piboxLogger(LOG_ERROR, "Password too short (minimum 7 characters).  WPA config not saved.\n");
                }
                break;

            // In case we get extra fields, ignore them.
            default:
                piboxLogger(LOG_INFO, "Unknown field: *%s*\n", field);
                break;
        }
        if ( doUpdate )
        {
            pncSetWPAField(wpaIdx, field);
        }
        else
        {
            /* One of the fields was invalid.  Reset WPA fields internally and return. */
            pncLoadWPA();
            return;
        }

        // Move to next field
        idx++;
        wpaIdx = -1;
        field = strtok_r(NULL, ":", &saveptr);
    }

    if ( fields > 0 )
    {
        piboxLogger(LOG_ERROR, "Missing at least one field.  WPA config not saved.\n");
        pncLoadWPA();
        return;
    }

    // Save the updates
    filename = pncGetFilename( PNC_ID_WPASUPPLICANT );
    if ( filename != NULL )
    {
        pncSaveWPA(filename);
        g_free(filename);

        /* reload wpa config; This should test return values, but for now this is enough. */
        system( WPA_RELOAD );
    }
    pncClearWPA();
}

/*========================================================================
 * Name:   getMACList
 * Prototype:  void getMACList(PIBOX_MSG_T *msg);
 *
 * Description:
 * Retrieve the list of MAC addresses and return to caller.
 *========================================================================*/
void
getMACList(PIBOX_MSG_T *msg)
{
    struct stat stat_buf;
    char *filename = NULL;
    char *buf = NULL;
    FILE *fd;
    int bytes;
    int rc;

    piboxLogger(LOG_INFO, "entered setMACList\n");
    if ( isCLIFlagSet( CLI_TEST) )
        filename = HOSTAPACCEPT_FD;
    else
        filename = HOSTAPACCEPT_F;

    piboxLogger(LOG_INFO, "Retrieving mac list.\n");
    if ( stat(filename, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "getMACList: no such file: %s\n", filename);
        buf = (char *)malloc(1);
        memset(buf, 0, 1);
    }
    else
    {
        buf = (char *)malloc(stat_buf.st_size+1);
        fd = fopen(filename, "r");
        rc = fread(buf, sizeof(char), stat_buf.st_size, fd);
        if ( rc == 0 )
        {
            if ( ferror(fd) )
                piboxLogger(LOG_ERROR, "Error reading file: %s\n", strerror(errno));
        }
        else
            buf[stat_buf.st_size]='\0';
        fclose(fd);
    }

    bytes = write( msg->insd, (char *)buf, strlen(buf) );
    if ( bytes != strlen(buf) )
        piboxLogger(LOG_ERROR, "write to socket failed.\n");
    free(buf);
}

/*========================================================================
 * Name:   setMACList
 * Prototype:  void setMACList(PIBOX_MSG_T *msg);
 *
 * Description:
 * Save the list of MAC addresses used for MAC filtering by hostapd.
 *========================================================================*/
void
setMACList(PIBOX_MSG_T *msg)
{
    char *filename = NULL;
    FILE *fd;

    piboxLogger(LOG_INFO, "entered setMACList\n");
    // Check for the payload
    if ( msg->payload == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing MAC filter list.  Dropping request.\n");
        return;
    }
    if ( strlen(msg->payload) <18 )
    {
        piboxLogger(LOG_ERROR, "Invalid MAC filter list.  Dropping request.\n");
        return;
    }

    if ( isCLIFlagSet( CLI_TEST) )
        filename = HOSTAPACCEPT_FD;
    else
        filename = HOSTAPACCEPT_F;

    piboxLogger(LOG_INFO, "Writing mac list: %s\n", msg->payload);
    fd = fopen(filename, "w");
    fwrite(msg->payload, strlen(msg->payload), 1, fd);
    fclose(fd);
}

/*========================================================================
 * Name:   networkRestart
 * Prototype:  void networkRestart(void)
 *
 * Description:
 * Restart the local network.
 *========================================================================*/
void 
networkRestart(void )
{
    char *buf;
    int length;
    int status;

    piboxLogger(LOG_INFO, "entered networkRestart\n");

    // Restart the web service
    length = strlen(NETSVC) + 1;
    buf = (char *)malloc(length);
    memset(buf, 0, length);
    sprintf(buf, NETSVC);
    piboxLogger(LOG_INFO, "Network service restart command: %s\n", buf);
    status = system(buf);
    free(buf);
    if( WEXITSTATUS(status) != 0 )
        piboxLogger(LOG_ERROR, "Failed to restart network service.\n");
    else
        piboxLogger(LOG_INFO, "Restarted network service\n");
}

