#!/bin/bash -p

XEONPKG=0
PIPLAYERPKG=0
PISENTRY=0
AUTOKIOSK=0

# Allow setting toolchain, staging and opkg directory from command line.
while getopts ":apSxo:s:t:" Option
do
    case $Option in
    t) TC=$OPTARG;;
    s) STAGING=$OPTARG;;
    o) OPKG_DIR=$OPTARG;;
    x) XEONPKG=1;;
    p) PIPLAYERPKG=1;;
    S) PISENTRY=1;;
    a) AUTOKIOSK=1;;
    *) echo "./cross.sh [ -apSx | -o dir | -s dir | -t dir ]"; exit 0;;
    esac
done

# Note: STAGING_DIR is the <buildroot>/output/staging generated from the "make buildroot" for PiBox.
if [ "$STAGING" != "" ] 
then
	export STAGING_DIR=$STAGING
else
	if [ "$BR" != "" ] 
	then
		export STAGING_DIR=$BR/output/staging
	else
		echo "You must set BR to the top of your buildroot tree or STAGING to where the staging tree lives."
		exit 1
	fi
fi

# Toolchain should be the toolchain built with PiBox and installed as an RPM.
if [ "$TC" != "" ] 
then
	export TOOLCHAIN=$TC
else
	echo "You must set TC to where the cross toolchain lives."
	exit 1
fi

# Need to specify where the opkg tools are installed.
if [ "$OPKG_DIR" = "" ] 
then
	echo "You must set OPKG_DIR to where the opkg-build program lives."
	exit 1
fi

if [ -f config.log ]
then
	make distclean
fi

# Get the toolchain prefix for the compiler tools
TCPREFIX=$(cd ${TC}/bin && ls -1 *gcc | sed 's/-gcc//')

echo "Toolchain     : $TOOLCHAIN"
echo "Staging tree  : $STAGING_DIR"
echo "OPKG directory: $OPKG_DIR"
echo "Toolchain prefix: $TCPREFIX"

autoreconf -i

PKG_CONFIG_PATH="" \
    PKG_CONFIG_LIBDIR=${STAGING_DIR}/usr/lib/pkgconfig:${STAGING_DIR}/usr/share/pkgconfig \
    PKG_CONFIG_SYSROOT_DIR=$STAGING_DIR \
    PATH=$TOOLCHAIN/bin:$PATH \
    CFLAGS="-g --sysroot=$STAGING_DIR -I$STAGING_DIR/usr/lib/glib-2.0/include -I$STAGING/usr/lib/gtk-2.0/include" \
	LDFLAGS="-L$STAGING_DIR/usr/lib -L$STAGING_DIR/lib --sysroot=$STAGING_DIR/" \
	./configure --host="${TCPREFIX}"

PATH=$TOOLCHAIN/bin:$PATH \
	CFLAGS="-g -I$STAGING_DIR/usr/include " \
	LDFLAGS="-L$STAGING_DIR/usr/lib -L$STAGING_DIR/lib --sysroot=$STAGING_DIR/" \
	make 

OPKG_DIR=$OPKG_DIR DESTDIR=`pwd`/install AUTOKIOSK=$AUTOKIOSK XEON=$XEONPKG PIPLAYER=$PIPLAYERPKG PISENTRY=$PISENTRY make install pkg

echo 
echo "----------------------------------------------"
echo "opkg has been built under the opkg directory:"
echo "----------------------------------------------"
echo 
ls -l opkg/*.opk
