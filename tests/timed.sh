#!/bin/bash 
# Timed test: runs extended messaging tests.
# ------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
SEC=15
DELAY=2

# Find the server script.
SERVER=
if [ -f server.sh ]; then SERVER=server.sh; fi
if [ -f tests/server.sh ]; then SERVER=tests/server.sh; fi
if [ ! -f $SERVER ]; then echo "Can't find server.sh"; exit 1; fi

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-v | -s sec | -d delay ] [-- <args>]"
    echo "where"
    echo "-s sec      Number of seconds to run the test (default: $SEC)."
    echo "-d delay    Delay, in seconds, between test runs (default: $DELAY)."
    echo "<args>      All arguments after the -- are passed to the server.sh script."
    echo "If no additional arguments, the following test is run on each iteration:"
    echo "  ./server.sh -a"
}

# Check how much time has elapsed and, if necessary, disable further testing.
function checkExpire
{
    currentTime=`date +%s`
    elapsedTime=`expr $currentTime - $startTime`

    if [ $elapsedTime -gt $SEC ]
    then
        ACTIVE=0
    fi
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":d:s:" Option
do
    case $Option in
    s)  SEC=$OPTARG;;
    d)  DELAY=$OPTARG;;
    *)  doHelp; exit 0;;
    esac
done
shift $((OPTIND-1))
ARGS=$*
if [ "$ARGS" = "" ]
then
	ARGS="-a"
fi

# echo "SEC  =$SEC"
# echo "DELAY=$DELAY"
# echo "ARGS =$ARGS"

#--------------------------------------------------------------
# Tests
#--------------------------------------------------------------

echo "Running for $SEC seconds with a delay of $DELAY between tests."
echo ""

startTime=`date +%s`
ACTIVE=1
count=1
while [ "$ACTIVE" = "1" ]
do
    echo "--------------------------------------------------------------------------------"
    echo "Iteration: $count "
    echo "------------------"
    $SERVER $ARGS
    checkExpire
    sleep $DELAY
    count=`expr $count + 1`
done
