#!/bin/bash 
LOG=/tmp/file.out
RC=$1
DATA="<p>Sent $RC</p>"
if [ "$2" != "" ]
then
    DATA="`cat $2`"
fi
nc -o $LOG -l 80 -c "echo -e \"HTTP/1.1 $RC OK\r\n$(date)\r\n\r\n\";echo -e \"$DATA\""
# cat $LOG
rm -f $LOG

