#!/bin/bash 
# Test the msgProcessor server with good or bad data.
# ------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
DATAFILE=data.out
MULTICAST_IP="234.1.42.3"
MULTICAST_PORT=13911
PIBOX_PORT=13910
VERBOSE=3
KEEP=0
DOKILL=0
NCOUT=
VALGRIND=
MINISERVER=miniserver.sh
LOGFILE=/tmp/piboxdtest.log
NOIOT=""
NOSMB=""

# Test names
SERVER=
SERVERN=
PROCS=
PROCE=
PROCN=
TWOCAM=
HEARTBEAT=
HEARTBEATN=
HEARTBEATE=
GIFLIST=
GIF=
GDNS=
GMODE=
SMODE=
SIPV4=
GWIRELESS=
SWIRELESS=
GAP=
SAP=
MULTICAST=
IOT=
RESET=
IDS=

# Enable core dumps
ulimit -c unlimited

# If needed, run as root user
SUDOCMD="sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib"
SUDO=

#--------------------------------------------------------------
# Signal Traps
#--------------------------------------------------------------
# trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
    killServer 1
}

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Generic message formatter
function msg
{
    echo "### $1"
}

function pass
{
    msg "PASS"
}

function fail
{
    msg "FAIL: $1"
    msg "LOGFILE:"
    cat $LOGFILE
    msg "END LOGFILE"
}

function checkCore
{
    if [ -f "core.$PID" ]
    then
        msg "piboxd generated a core file: core.$PID"
    fi
}

# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-aISs | -v level | -d val | -i ids | -t name | -t ... ]"
    echo "where"
    echo "-a          Run all tests in the order listed below"
    echo "-d val      Enable valgrind to run the server; space separated list of options in double quotes"
    echo "            Available options:"
    echo "            1: no arguments to valgrind"
    echo "            2: enable --leak-check=full"
    echo "            3: enable --show-reachable=yes"
    echo "-s          Run server as root user"
    echo "-k          Keep the server running"
    echo "-K          Kill any running server"
    echo "-I          Disable IoT processing in server."
    echo "-S          Disable SMB processing in server."
    echo "-v level    Enable verbose output with the specified level."
    echo "            Defaults to 3 (should not be set below this or some tests will fail)"
    echo "-i ids      Comma separated list of subtests"
    echo "-t name     name of test to enable, can be specified multiple times"
    echo "            Available tests:"
    echo "            Name           Description"
    echo "            server         Test inbound message server"
    echo "            servern        Test inbound message server (negative tests)"
    echo "                           Subtests: 1 2 3 4 5 6 7 8"
    echo "            procs          Test start message processor"
    echo "            proce          Test end message processor"
    echo "            procn          Test message processor (negative tests)"
    echo "                           Subtests: 1 2 3 4"
    echo "            twocam         Try to start two webcam streams."
    echo "            heartbeat      Pass a heartbeat to an existing process."
    echo "            heartbeatn     Pass a heartbeat to a non-existing process."
    echo "            heartbeate     Send heartbeat then let message expire."
    echo "            giflist        Get interface list."
    echo "            gif            Get interface."
    echo "            gdns           Get DNS."
    echo "            gmode          Get network mode (wireless client or access point)."
    echo "            smode          Set network mode (wireless client or access point)."
    echo "            sipv4          Save interface configuration."
    echo "                           Subtests: 1 2 3 4 5 6 7 8 9"
    echo "            gwireless      Get wireless client configuration."
    echo "            swireless      Save wireless client configuration."
    echo "                           Subtests: 1 2 3 4 5"
    echo "            gap            Get access point configuration."
    echo "                           Subtests: 1 2 3"
    echo "            sap            Save access point configuration."
    echo "            multicast      Test multicast input (no validation)."
}

# Check the server log file for a specific message.
# first argument is string to look for
# returns:
# 0 on success (string was found in the log)
# 1 on failure (string was not found in the log)
function logged
{
    local str=$1
    local src=$2
    if [ "$src" = "" ]
    then
        grep "$str" $LOGFILE >/dev/null 2>&1
    else
        grep "$str" $src >/dev/null 2>&1
    fi
    return $?
}

# Kill the server that is being tested.
# If requested (first arg), exit with the specified exit code.
function killServer
{
    echo "Killing server: $PID"
    if [ "$SUDO" != "" ]
    then
        sudo killall piboxd
    else
        kill $PID
    fi
    PROC=`ps xa | grep piboxd | grep -v grep`
    CNT=1
    while [ "$PROC" != "" -a $CNT -lt 10 ] 
    do
        sleep 1
        CNT=$(($CNT+1))
        PROC=`ps xa | grep piboxd | grep -v grep`
    done
    if [ $1 -ne -1 ]
    then
        checkCore
        exit $1
    fi
}

# Generate file that contains a message to daemon.
function genMsg
{
    # Value should be a hex string
    header=$1

    # Value should be character string
    filename=$2
    
    # If requested, find Net interfaces
    if [ "$DOIF" != "" ]
    then
        netif=`ls -1 /sys/class/net | egrep -v "lo|virbr*" | head -1`
    fi

    # Header
    if [ "$BADHDR" != "" ]
    then
        echo "blah" > servertest.dat
    else
        printf "0: %s" $header | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r > servertest.dat
    fi

    # Size
    if [ "$NOSIZE" = "" ]
    then
        if [ "$DOIF" != "" ]
        then
            size=`echo $netif | wc -c`
        else
            size=`echo $filename | wc -c`
        fi
        size=`expr $size - 1`
        if [ "$BADSIZE" != "" ]
        then
            case $BADSIZE in
            1) size=255;;
            2) size=`expr $size - 1`;;
            3) size=0;;
            esac
        fi
        printf "8: %.8x" $size | sed -E 's/8: (..)(..)(..)(..)/8: \4\3\2\1/' | xxd -r >> servertest.dat
    fi

    # Generate a UUID or use one passed to us
    UUID="3e55b823-e785-49df-ae5e-5d4605bb21f2"
    if [ "$SETUUID" = "1" ]
    then
        UUID=$(uuidgen)
    else 
        if [ "$SETUUID" != "" ]
        then
            UUID=$SETUUID
        fi
    fi

    # Tag
    if [ "$NOTAG" = "" ]
    then
        if [ "$BADTAG" != "" ]
        then
            echo -ne 3e55b823 >> servertest.dat
        else
            echo -ne "$UUID" >> servertest.dat
        fi
    fi

    # Filename
    if [ "$NOFILENAME" = "" ]
    then
        # If bad is requested, then don't include the filename at all.
        if [ "$BADFILE" = "" ]
        then
            echo -ne $filename >> servertest.dat
        fi
    fi

    # Net interfaces
    if [ "$DOIF" != "" ]
    then
        echo -ne $netif >> servertest.dat
    fi

    # Add raw data as payload.
    if [ "$RAWDATA" != "" ]
    then
        size=`echo "$RAWDATA" | wc -c`
        size=`expr $size - 1`
        printf "8: %.8x" $size | sed -E 's/8: (..)(..)(..)(..)/8: \4\3\2\1/' | xxd -r >> servertest.dat
        echo -ne "$RAWDATA" >> servertest.dat
    fi

    [ $VERBOSE -gt 3 ] && hexdump -C < servertest.dat
}

# Send the message to the daemon
function sendIt
{
    local RC

    if [ "$NCOUT" != "" ]
    then
        SOUT="`nc -4 -x $DATAFILE localhost $PIBOX_PORT < servertest.dat 2>&1`"
    else
        SOUT="`nc -4 localhost $PIBOX_PORT < servertest.dat 2>&1`"
    fi
    RC=$?
    echo "$SOUT" | grep -i "connection refused" >/dev/null 2>&1
    if [ $? -eq 0 ]
    then
        msg "Failed to connect to server."
        return
    fi
    conreset=0
    echo "$SOUT" | grep -i "connection reset by peer" >/dev/null 2>&1
    if [ $? -eq 0 ]
    then
        conreset=1
    fi

    if [ "$RCBAD" == "" ]
    then
        case $RC in
        0) if [ "$NCOUT" != "" ]
           then 
               echo "$SOUT" > $DATAFILE
           else 
               if [ "$SOUT" != "" ]; then echo "$SOUT"; fi
           fi
           ;;
        *) if [ $conreset -eq 1 ] 
           then 
               if [ $VERBOSE -gt 3 ]; then msg "Connection reset by peer - message may be truncated."; fi
           else
               msg "Test failed with rc = $RC"
               if [ $VERBOSE -gt 3 ]; then if [ -f $DATAFILE ]; then cat $DATAFILE; fi; fi
           fi
           ;;
        esac
    else
        case $RC in
        0) msg "No error but there should be!";;
        *) if [ "$NCOUT" != "" ]
           then 
               echo "$SOUT" > $DATAFILE
           else 
               if [ "`echo "$SOUT"  | sed 's/Ncat: Connection reset by peer\.//'`" != "" ]
               then 
                   echo "$SOUT"  | sed 's/Ncat: Connection reset by peer\.//'
               fi
           fi
           ;;
        esac
    fi

    # Save output, if requested, in DATAFILE for test to analyze
    if [ "$SAVERETURN" != "" ]
    then
        echo "$SOUT" | sed 's/Ncat: Connection reset by peer.//' > $DATAFILE
    fi

    rm servertest.dat

    # Pause a bit for the server to catch up before we kill it off.
    if [ "$DOSLEEP" = "" ]
    then
        sleep 3
    else
        sleep $DOSLEEP
    fi
}

# Send a multicast message
function sendMulticast
{
    local RC

    [ $VERBOSE != 0 ] && msg "Sending to $MULTICAST_IP:$MULTICAST_PORT"
    if [ "$NCOUT" != "" ]
    then
        SOUT=`cat servertest.dat | socat STDIO UDP4-DATAGRAM:$MULTICAST_IP:$MULTICAST_PORT,range=192.168.101.0/24`
    else
        SOUT=`cat servertest.dat | socat STDIO UDP4-DATAGRAM:$MULTICAST_IP:$MULTICAST_PORT,range=192.168.101.0/24`
    fi
    return
    rm servertest.dat
}

# Manage mini web server
function runWebServer
{
    RC=$1
    DATA=$2
    if [ ! -f tests/$MINISERVER ]
    then
        echo "Can't find tests/$MINISERVER."
        exit 1
    fi
    [ $VERBOSE == 1 ] && echo sudo tests/$MINISERVER $RC $DATA
    sudo tests/$MINISERVER $RC $DATA &>/dev/null &
    sleep 3
}
function killWebServer
{
    sudo killall nc &>/dev/null
    sudo killall $MINISERVER &>/dev/null
}
function genRegistration
{
    cat << EOF > $1
{
    \\"identity\\":\\"0e92b74c-e7ff-11e5-ba13-0011506f6a67\\",
    \\"features\\":
    {
        \\"damper\\":\\"set\\",
        \\"damper-display\\":\\"Vent Damper\\",
        \\"damper-set\\":\\"0,25,50,75,100\\",
        \\"damper-value\\":\\"25\\"
    }
}
EOF
}

# Reset the server 
function reset
{
    # Stop any running webcam stream.
    genMsg 00000201 webcam
    RCSKIP=1
    sendIt
    unset RCSKIP
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":aIkKSsv:d:i:t:" Option
do
    case $Option in
    t)  testname=`echo $OPTARG | tr a-z A-Z`
        eval $testname=1
        ;;
    i)  IDS=`echo "$OPTARG"|sed 's/,/ /'`;;
    s)  SUDO=$SUDOCMD;;
    I)  NOIOT="-I ";;
    S)  NOSMB="-s ";;
    k)  KEEP=1;;
    K)  DOKILL=1;;
    v)  VERBOSE=$OPTARG;;
    d)  VALGRIND="valgrind"
        for MYOPT in `echo $OPTARG`
        do
            case $MYOPT in
            1) ;;
            2) VALGRIND="$VALGRIND --leak-check=full --suppressions=tests/gtk.suppression --gen-suppressions=all";;
            3) VALGRIND="$VALGRIND --show-reachable=yes";;
            *) ;;
            esac
        done
        ;;
    a)  SERVER=1
        SERVERN=1
        PROCS=1
        PROCE=1
        PROCN=1
        TWOCAM=1
        HEARTBEAT=1
        HEARTBEATN=1
        HEARTBEATE=1
        GIFLIST=1
        GIF=1
        GDNS=1
        GMODE=1
        SMODE=1
        SIPV4=1
        GWIRELESS=1
        SWIRELESS=1
        GAP=1
        SAP=1
        MULTICAST=1
        RESET=1
        ;;
    *)  doHelp; exit 0;;
    esac
done

# If requested, kill any running instances of the server
if [ $DOKILL -eq 1 ]
then
    sudo killall piboxd
    exit 0
fi

#--------------------------------------------------------------
# Start the server
#--------------------------------------------------------------
if [ ! -f src/piboxd ]
then
    echo "src/piboxd missing.  Have you compiled it yet?"
    exit 1
fi
if [ "$SUDO" != "" ]
then
    echo "Running server as root."
fi

# Start the server with some testable settings
# -t - set timeout for streams, just for testing (default is too high)
if [ $VERBOSE -gt 3 ]
then
    SUDO=$SUDOCMD
    $SUDO $VALGRIND src/piboxd ${NOIOT} ${NOSMB} -t 12 -T -v$VERBOSE 2>&1 | tee $LOGFILE &
else
    $SUDO $VALGRIND src/piboxd ${NOIOT} ${NOSMB} -t 12 -T -v$VERBOSE >$LOGFILE 2>&1 &
fi
PID=$!
CMD="$(ps -p ${PID} -o args | tail -1)"
echo "Server PID = ${PID}"
echo "Command    = ${CMD}"
sleep 2

#--------------------------------------------------------------
# Tests
#--------------------------------------------------------------

#--------------------------------------------------------------
# Test message server, which receives inbound messages
#--------------------------------------------------------------
if [ "$SERVER" != "" ]
then
    msg "SERVER: missing action field"

    # Send an invalid stream request message to the server.
    # This is partially valid but lacks a proper action field
    # So it passes the message processor but the 
    # queue processor won't process it.

    genMsg 00000001 abcd
    sendIt
    # It passes if it doesn't crash.
    pass
fi

if [ "$SERVERN" != "" ]
then
    if [ "$IDS" = "" ]
    then
        IDS="1 2 3 4 5 6 7"
        SERVERNRESET=1
    fi

    # Send invalid messages to the server
    # The message processor should reject these messages.
    # re: The queue processor will never see it.
    # If these don't crash then they pass.

    # short message (only header arrives)
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Short message"
        NOSIZE=1
        NOTAG=1
        NOFILENAME=1
        genMsg 00000101 blah
        unset NOSIZE
        unset NOTAG
        unset NOFILENAME
        sendIt
        pass
    fi

    # Size too long 
    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Size too long"
        BADSIZE=1
        genMsg 00000101 blah
        unset BADSIZE
        sendIt
        pass
    fi

    # Size too short 
    subtest=`echo $IDS | grep 3`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Size too short"
        BADSIZE=2
        genMsg 00000101 blah
        unset BADSIZE
        RCBAD=1
        sendIt
        unset RCBAD
        pass
    fi

    # Size is zero
    subtest=`echo $IDS | grep 4`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Size is zero"
        BADSIZE=3
        genMsg 00000101 blah
        unset BADSIZE
        RCBAD=1
        sendIt
        unset RCBAD
        pass
    fi

    # Invalid message type
    subtest=`echo $IDS | grep 5`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Invalid message type"
        genMsg 000001ff blah
        RCBAD=1
        sendIt
        unset RCBAD
        pass
    fi

    # Invalid header - this makes it to the queueProcessor
    subtest=`echo $IDS | grep 6`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Invalid header"
        genMsg 00000000 blah
        RCBAD=1
        sendIt
        unset RCBAD
        pass
    fi

    # Bad tag
    subtest=`echo $IDS | grep 7`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Bad tag"
        BADTAG=1
        genMsg 00000101 blah
        unset BADTAG
        sendIt
        pass
    fi

    # Bad file
    subtest=`echo $IDS | grep 8`
    if [ "$subtest" != "" ]
    then
        msg "SERVERN: Bad file"
        BADFILE=1
        genMsg 00000101 blah
        unset BADFILE
        sendIt
        pass
    fi

    if [ "$SERVERNRESET" = "1" ]
    then
        unset IDS
    fi
fi

#--------------------------------------------------------------
# Test message processor, which manages streams
#--------------------------------------------------------------
if [ "$PROCS" != "" ]
then
    # Send a webcam start message 
    # This will fail if mjpeg-streamer is not found.
    # Validation: Look for error message from server in LOGFILE

    msg "PROCS: webcam start"
    genMsg 00000101 webcam
    sendIt

    # Validate
    logged "Failed to spawn mjpg-streamer stream for webcam"
    if [ $? -eq 1 ]; then fail "Did not attempt to start webcam."; killServer 1; fi
    pass
fi

if [ "$PROCE" != "" ]
then
    # Send a webcam stop message 
    # This only works if mjpeg-streamer can be started.

    msg "PROCE: webcam end"
    genMsg 00000201 webcam
    sendIt

    # Validate
    if [ "$PROCS" != "" ]
    then
        logged "Killed stream process"
        if [ $? -eq 1 ]; then fail "Did not attempt to stop webcam."; killServer 1; fi
        pass
    else
        logged "ERROR Can't end stream, No matching tag:"
        if [ $? -eq 1 ]; then fail "Did not attempt to stop webcam."; killServer 1; fi
        pass
    fi
fi

if [ "$PROCN" != "" ]
then
    if [ "$IDS" = "" ]
    then
        IDS="1 2 3 4"
        PROCNRESET=1
    fi

    # Invalid Type
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        msg "PROCN: invalid type"
        genMsg 000002ff webcam
        sendIt
        logged "Invalid msg type"
        if [ $? -eq 1 ]; then fail "Failed to catch invalid type."; killServer 1; fi
        pass
    fi


    # Invalid action
    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        msg "PROCN: invalid action"
        genMsg 0000ff01 webcam
        sendIt
        logged "ERROR Invalid stream action"
        if [ $? -eq 1 ]; then fail "Failed to catch invalid action."; killServer 1; fi
        pass
    fi


    # Unsupported stream, start action (can't set timeout)
    subtest=`echo $IDS | grep 3`
    if [ "$subtest" != "" ]
    then
        msg "PROCN: unsupported stream (start action)"
        genMsg 00000101 badstream
        sendIt
        logged "ERROR Can't set timeout for stream"
        if [ $? -eq 1 ]; then fail "Failed to catch unsupported stream."; killServer 1; fi
        pass
    fi

    # Unsupported stream, end action (can't set timeout)
    subtest=`echo $IDS | grep 4`
    if [ "$subtest" != "" ]
    then
        msg "PROCN: unsupported stream (end action)"
        genMsg 00000201 badstream
        sendIt
        logged "ERROR Can't set timeout for stream"
        if [ $? -eq 1 ]; then fail "Failed to catch unsupported stream."; killServer 1; fi
        pass
    fi

    if [ "$PROCNRESET" = "1" ]
    then
        unset IDS
    fi
fi

if [ "$TWOCAM" != "" ]
then
    # Send a webcam start message twice, with a slight pause between requests.
    # 0x00000101
    # 0x00000006
    # 3e55b823-e785-49df-ae5e-5d4605bb21f2
    # webcam

    msg "TWOCAM: two stream test"

    DOSLEEP=1

    # Send first requeste
    genMsg 00000101 webcam
    sendIt

    # Send second requeste
    genMsg 00000101 webcam
    sendIt
    logged "INFO Killed stream process."
    if [ $? -eq 1 ]; then fail "Failed to handle multiple stream request."; killServer 1; fi
    pass

    reset
    unset DOSLEEP
fi

if [ "$HEARTBEAT" != "" ]
then
    msg "HEARTBEAT: start stream, send heartbeat for it"

    DOSLEEP=1
    SETUUID=1

    # Start a process and then send a heartbeat to it.
    genMsg 00000101 webcam
    sendIt

    # Send heartbeat
    SETUUID=$UUID
    genMsg 00000002 webcam
    sendIt
    logged "INFO Set timestamp for: $UUID"
    if [ $? -eq 1 ]; then fail "Failed to handle heartbeat request."; killServer 1; fi
    pass

    # Reset the server
    reset

    unset DOSLEEP
    unset SETUUID
fi

if [ "$HEARTBEATN" != "" ]
then
    msg "HEARTBEATN: send heartbeat for non-existant stream"

    # Reset any webcam process.
    reset
    sleep 1

    # Send a heartbeat with no process running.
    genMsg 00000002 webcam
    sendIt
    logged "ERROR Failed to set timestamp, no such tag: $UUID"
    if [ $? -eq 1 ]; then fail "Failed to catch heartbeat for non-existant stream."; killServer 1; fi
    pass
fi

if [ "$HEARTBEATE" != "" ]
then
    msg "HEARTBEATE: let stream expire"

    # Start a process and then send a heartbeat to it and let it expire.
    genMsg 00000101 webcam
    sendIt
    sleep 2
    # Send heartbeat
    genMsg 00000002 webcam
    sendIt

    msg "HEARTBEATE: Waiting for the expiration..."
    sleep 14
    logged "INFO Expiring stream: 3e55b823-e785-49df-ae5e-5d4605bb21f2"
    if [ $? -eq 1 ]; then fail "Failed to expire stream."; killServer 1; fi
    pass

    # Reset the server
    reset
fi

if [ "$GIFLIST" != "" ]
then
    [ $VERBOSE != 0 ] && msg "GIFLIST: get interface list"

    NOSIZE=1
    NOTAG=1
    NOFILENAME=1
    genMsg 00000103 
    unset NOSIZE
    unset NOTAG
    unset NOFILENAME
    NCOUT=1
    sendIt
    unset NCOUT

    # Unit testing uses the fake network interfaces.
    ls -1 data/net | while read line
    do
        # msg "Looking for interface ${line} in logfile."
        logged "$line"
        if [ $? -eq 1 ]; then fail "Failed to find interface: $line"; killServer 1; fi
    done
    pass
    rm $DATAFILE
fi

if [ "$GIF" != "" ]
then
    [ $VERBOSE != 0 ] && msg "GIF: get interface"

    NOTAG=1
    NOFILENAME=1
    DOIF=1
    genMsg 00000203 

    unset NOTAG
    unset NOFILENAME
    unset DOIF
    NCOUT=1
    sendIt
    unset NCOUT

    # The response will be NOCONFIG because the interface names on the test 
    # host won't match what is expected in the test configuration files.
    logged "NOCONFIG:" $DATAFILE
    if [ $? -eq 1 ]; then fail "Failed in request interface configuration"; killServer 1; fi
    pass
    rm $DATAFILE
fi

if [ "$GDNS" != "" ]
then
    [ $VERBOSE != 0 ] && msg "GDNS: get DNS configuration"

    NOTAG=1
    NOFILENAME=1
    genMsg 00000303 
    unset NOTAG
    unset NOFILENAME
    NCOUT=1
    RCBAD=1
    sendIt
    unset RCBAD
    unset NCOUT
    logged "192.168.101.10" $DATAFILE
    if [ $? -eq 1 ]; then fail "Failed to find DNS setting"; killServer 1; fi
    pass
    rm $DATAFILE
fi

if [ "$GMODE" != "" ]
then
    [ $VERBOSE != 0 ] && msg "GMODE: get network mode setting"

    NOTAG=1
    NOFILENAME=1
    genMsg 00000403 
    unset NOTAG
    unset NOFILENAME
    RCBAD=1
    NCOUT=1
    sendIt
    unset RCBAD
    unset NCOUT

    logged "wap" $DATAFILE
    if [ $? -eq 1 ]; then fail "Failed to find mode setting"; killServer 1; fi
    pass
    rm $DATAFILE
fi

if [ "$SMODE" != "" ]
then
    [ $VERBOSE != 0 ] && msg "SMODE: set network mode setting"

    NOSIZE=1
    NOTAG=1
    NOFILENAME=1
    NCOUT=1

    # client mode
    msg "SMODE: client mode"
    rm -f data/nettype.bak
    RAWDATA="client"
    genMsg 00000a03 
    sendIt
    sleep 1
    logged "client" data/nettype.back
    if [ $? -eq 1 ]; then fail "Missing client mode"; killServer 1; fi
    pass

    # wap mode
    msg "SMODE: wireless mode"
    rm -f data/nettype.bak
    RAWDATA="wap"
    genMsg 00000a03 
    sendIt
    sleep 1
    logged "wap" data/nettype.back
    if [ $? -eq 1 ]; then fail "Missing wap mode"; killServer 1; fi
    pass

    # invalid mode
    msg "SMODE: invalid mode"
    rm -f data/nettype.bak
    RAWDATA="xxx"
    genMsg 00000a03 
    sendIt
    sleep 1
    if [ -f data/nettype.bak ]; then fail "Saved invalid mode."; killServer 1; fi
    pass
    rm -f data/nettype.bak

    unset RAWDATA
    unset NOTAG
    unset NOFILENAME
    unset NCOUT
fi

if [ "$SIPV4" != "" ]
then
    if [ "$IDS" = "" ]
    then
        IDS="1 2 3 4 5 6 7 8 9"
        SERVERNRESET=1
    fi

    [ $VERBOSE != 0 ] && msg "SIPV4: interface configurations"
    
    # Get valid interface name from this host.
    # ifaceName=`ls -1 /sys/class/net | grep -v lo | head -1`
    ifaceName=`ls -1 data/net | head -1`
    
    NOSIZE=1
    NOTAG=1
    NOFILENAME=1
    NCOUT=1
    
    # Format of payload
    # 1. Interface (name of interface)
    # 2. Enabled state (yes/no)
    # 3. Configuration type (DHCP/Static)
    # 4. IP Address
    # 5. Netmask
    # 6. Gateway
    # 7. DNS 0
    # 8. DNS 1
    # 9. DNS 2
    
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        # Enabled using DHCP (no static configuration)
        msg "SIPV4 1: Enabled using DHCP (no static configuration)"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:yes:DHCP:-1:-1:-1:-1:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        logged "auto $ifaceName" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "DHCP Enabled: Missing auto line for $ifaceName"; killServer 1; fi
        logged "iface $ifaceName inet dhcp" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "DHCP Enabled: Missing iface line for $ifaceName"; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        # Disabled 
        msg "SIPV4 2: Disabled after DHCP configuration"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:no:DHCP:-1:-1:-1:-1:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        cat data/interfaces.bak
        logged "auto $ifaceName" data/interfaces.bak 
        if [ $? -eq 0 ]; then fail "$ifaceName is enabled but shouldn't be"; killServer 1; fi
        pass
    
        # Enabled with static fields
        msg "SIPV4 2: Enabled with static configuration"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:yes:Static:192.168.101.8:255.255.255.0:192.168.101.1:192.168.101.10:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1
    
        # Verify
        logged "auto $ifaceName" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "Static Enabled: Missing auto line for $ifaceName"; killServer 1; fi
        logged "iface $ifaceName inet static" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "Static Enabled: Missing iface line for $ifaceName"; killServer 1; fi
        logged "address 192.168.101.8" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "Static Enabled: Missing address line"; killServer 1; fi
        logged "netmask 255.255.255.0" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "Static Enabled: Missing netmask line"; killServer 1; fi
        logged "gateway 192.168.101.1" data/interfaces.bak 
        if [ $? -eq 1 ]; then fail "Static Enabled: Missing gateway line"; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 3`
    if [ "$subtest" != "" ]
    then
        # Enabled, missing IP address
        msg "SIPV4 3: Enabled, missing IP address"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:yes:Static:-1:255.255.255.0:192.168.101.1:192.168.101.10:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        if [ -f data/interfaces.bak ]; then fail "Saved with incomplete static configuration."; killServer 1; fi
        pass
    fi
    
    subtest=`echo $IDS | grep 4`
    if [ "$subtest" != "" ]
    then
        # Enabled, all 3 DNS entries available
        msg "SIPV4 4: Enabled, all 3 DNS entries available"
        rm -f data/interfaces.bak
        rm -f data/resolv.conf.bak
        RAWDATA="$ifaceName:yes:Static:192.168.101.8:255.255.255.0:192.168.101.1:192.168.101.10:8.8.8.8:8.8.4.4"
        genMsg 00000703 
        sendIt
        sleep 1

        logged "nameserver 192.168.101.10" data/resolv.conf.bak 
        if [ $? -ne 0 ]; then fail "Missing DNS 0 line"; killServer 1; fi
        logged "nameserver 8.8.8.8" data/resolv.conf.bak 
        if [ $? -ne 0 ]; then fail "Missing DNS 1 line"; killServer 1; fi
        logged "nameserver 8.8.4.4" data/resolv.conf.bak 
        if [ $? -ne 0 ]; then fail "Missing DNS 2 line"; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 5`
    if [ "$subtest" != "" ]
    then
        # Enabled, invalid IP
        msg "SIPV4 5: Enabled, invalid IP"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:yes:Static:x:255.255.255.0:192.168.101.1:192.168.101.10:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        if [ -f data/interfaces.bak ]; then fail "Saved with invalid IP address."; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 6`
    if [ "$subtest" != "" ]
    then
        # Enabled, invalid netmask
        msg "SIPV4 6: Enabled, invalid netmask"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:yes:Static:192.168.101.1:x:192.168.101.1:192.168.101.10:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        if [ -f data/interfaces.bak ]; then fail "Saved with invalid netmask."; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 7`
    if [ "$subtest" != "" ]
    then
        # Enabled, invalid gateway
        msg "SIPV4 7: Enabled, invalid gateway"
        rm -f data/interfaces.bak
        RAWDATA="$ifaceName:yes:Static:192.168.101.1:255.255.255.0:x:192.168.101.10:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        if [ -f data/interfaces.bak ]; then fail "Saved with invalid gateway."; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 8`
    if [ "$subtest" != "" ]
    then
        # Enabled, invalid DNS
        msg "SIPV4 8: Enabled, invalid DNS"
        rm -f data/interfaces.bak
        rm -f data/resolv.conf.bak
        RAWDATA="$ifaceName:yes:Static:192.168.101.8:255.255.255.0:192.168.101.1:x:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        logged "nameserver" data/resolv.conf.bak 
        if [ $? -eq 0 ]; then fail "Saved resolv.conf with invalid DNS."; killServer 1; fi
        pass
    fi

    subtest=`echo $IDS | grep 9`
    if [ "$subtest" != "" ]
    then
        # Enabled with DHCP plus DNS
        msg "SIPV4 9: Enabled with DHCP plus DNS"
        rm -f data/interfaces.bak
        rm -f data/resolv.conf.bak
        RAWDATA="$ifaceName:yes:DHCP:-1:-1:-1:192.168.101.10:-1:-1"
        genMsg 00000703 
        sendIt
        sleep 1

        logged "nameserver" data/resolv.conf.bak 
        if [ $? -eq 0 ]; then fail "Saved resolv.conf when using DHCP."; killServer 1; fi
        pass
    fi

    unset NOSIZE
    unset NOTAG
    unset NOFILENAME
    unset RAWDATA
    unset NCOUT
fi

if [ "$GWIRELESS" != "" ]
then
    [ $VERBOSE != 0 ] && msg "GWIRELESS: get wireless configuration"
    NOTAG=1
    NOFILENAME=1
    genMsg 00000603 
    unset NOTAG
    unset NOFILENAME
    NCOUT=1
    sendIt
    unset NCOUT

    ssid=`cat $DATAFILE | cut -f1 -d":"`
    sect=`cat $DATAFILE | cut -f2 -d":"`
    pw=`cat $DATAFILE | cut -f3 -d":"`
    if [ "$ssid" = "" ]; then fail "Missing ssid."; killServer 1; fi
    if [ "$sect" = "" ]; then fail "Missing security type."; killServer 1; fi
    if [ "$pw" = "" ]; then fail "Missing password."; killServer 1; fi
    pass
fi

if [ "$SWIRELESS" != "" ]
then
    [ $VERBOSE != 0 ] && msg "SWIRELESS: save wireless configuration"

    if [ "$IDS" = "" ]
    then
        IDS="1 2 3 4 5"
        SWIRELESSRESET=1
    fi

    NOSIZE=1
    NOTAG=1
    NOFILENAME=1
    NCOUT=1

    # Format of payload:
    # SSID
    # Security type
    # Password

    # Valid fields
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        msg "SWIRELESS: valid fields"
        rm -f data/wpa_supplicant.conf.bak
        RAWDATA="MIKEH:WPA & WPA2 Personal:mypw000"
        genMsg 00000903 
        sendIt
        sleep 1
        line=`grep "ssid=" data/wpa_supplicant.conf.bak|head -1`
        val=`echo $line | cut -f2 -d"=" | sed 's/\"//g'`
        if [ "$val" != "MIKEH" ]; then fail "Missing or invalid ssid (val=$val)."; killServer 1; fi
        line=`grep "key_mgmt=" data/wpa_supplicant.conf.bak`
        val=`echo $line | cut -f2 -d"="`
        if [ "$val" != "WPA-PSK" ]; then fail "Missing or invalid security type (val=$val)."; killServer 1; fi
        line=`grep "psk=" data/wpa_supplicant.conf.bak`
        val=`echo $line | cut -f2 -d"=" | sed 's/\"//g'`
        if [ "$val" != "mypw000" ]; then fail "Missing or invalid password (val=$val)."; killServer 1; fi
        pass
    fi

    # Blank fields
    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        msg "SWIRELESS: blank fields"
        rm -f data/wpa_supplicant.conf.bak
        RAWDATA=":WPA & WPA2 Personal:mypw000"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with blank SSID."; killServer 1; fi
        rm -f data/wpa_supplicant.conf.bak

        RAWDATA="MIKEH::mypw000"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with blank security type."; killServer 1; fi
        rm -f data/wpa_supplicant.conf.bak

        RAWDATA="MIKEH:WPA & WPA2 Personal:"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with blank password."; killServer 1; fi
        pass
    fi

    # Missing fields
    subtest=`echo $IDS | grep 3`
    if [ "$subtest" != "" ]
    then
        msg "SWIRELESS: missing fields"
        rm -f data/wpa_supplicant.conf.bak
        RAWDATA="MIKEH:WPA & WPA2 Personal"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA when missing 1 field."; killServer 1; fi
        rm -f data/wpa_supplicant.conf.bak

        RAWDATA="MIKEH"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA when missing 2 fields."; killServer 1; fi
        pass
    fi

    # Invalid fields
    subtest=`echo $IDS | grep 4`
    if [ "$subtest" != "" ]
    then
        msg "SWIRELESS: invalid fields"
        rm -f data/wpa_supplicant.conf.bak
        RAWDATA="MIK:WPA & WPA2 Personal:mypw000"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with short SSID."; killServer 1; fi
        rm -f data/wpa_supplicant.conf.bak

        RAWDATA="MIKEH:Xersonal:mypw000"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with invalid security type."; killServer 1; fi
        rm -f data/wpa_supplicant.conf.bak

        RAWDATA="MIKEH:Personal:mypw"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with short password."; killServer 1; fi
        pass
    fi

    # Extra fields
    subtest=`echo $IDS | grep 5`
    if [ "$subtest" != "" ]
    then
        msg "SWIRELESS: extra fields"
        rm -f data/wpa_supplicant.conf.bak
        RAWDATA="MIKEH:Personal:mypw000:blah"
        genMsg 00000903 
        sendIt
        sleep 1
        if [ -f data/wpa_supplicant.conf.bak ]; then fail "Saved WPA with extra fields."; killServer 1; fi
        pass
    fi

    if [ "$SWIRELESSNRESET" = "1" ]
    then
        unset IDS
    fi

    unset NOSIZE
    unset NOTAG
    unset NOFILENAME
    unset RAWDATA
    unset NCOUT
fi

if [ "$GAP" != "" ]
then
    [ $VERBOSE != 0 ] && msg "GAP: get access point configuration"

    if [ "$IDS" = "" ]
    then
        IDS="1 2 3"
        GAPRESET=1
    fi

    NOTAG=1
    NOSIZE=1
    NOFILENAME=1
    NCOUT=1

    # Recover from previous tests, if necessary.
    if [ -f data/hostapd.conf- ]; then mv data/hostapd.conf- data/hostapd.conf; fi
    if [ -f data/hostapd-psk- ]; then mv data/hostapd-psk- data/hostapd-psk; fi

    # Valid file content
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE != 0 ] && msg "GAP: Valid file content"
        genMsg 00000503 
        sendIt
        ssid=`cat $DATAFILE | cut -f1 -d":"`
        channel=`cat $DATAFILE | cut -f2 -d":"`
        pw=`cat $DATAFILE | cut -f3 -d":"`
        if [ "$ssid" = "" ]; then echo "Missing ssid."; killServer 1; fi
        if [ "$channel" = "" ]; then echo "Missing channel."; killServer 1; fi
        if [ "$pw" = "" ]; then echo "Missing password."; killServer 1; fi
        pass
    fi

    # File missing: hostapd.conf
    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE != 0 ] && msg "GAP: missing hostapd.conf"
        mv data/hostapd.conf data/hostapd.conf-
        genMsg 00000503 
        sendIt
        ssid=`cat $DATAFILE | cut -f1 -d":"`
        channel=`cat $DATAFILE | cut -f2 -d":"`
        pw=`cat $DATAFILE | cut -f3 -d":"`
        if [ "$ssid" != "" ]; then echo "Got ssid but shouldn't have."; killServer 1; fi
        if [ "$channel" != "" ]; then echo "Got channel but shouldn't have."; killServer 1; fi
        if [ "$pw" = "" ]; then echo "Missing password"; killServer 1; fi
        pass
        mv data/hostapd.conf- data/hostapd.conf
    fi

    # File missing: hostapd-psk
    subtest=`echo $IDS | grep 3`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE != 0 ] && msg "GAP: missing hostapd-psk"
        mv data/hostapd-psk data/hostapd-psk-
        genMsg 00000503 
        sendIt
        ssid=`cat $DATAFILE | cut -f1 -d":"`
        channel=`cat $DATAFILE | cut -f2 -d":"`
        pw=`cat $DATAFILE | cut -f3 -d":"`
        if [ "$ssid" = "" ]; then echo "Missing ssid."; killServer 1; fi
        if [ "$channel" = "" ]; then echo "Missing channel."; killServer 1; fi
        if [ "$pw" != "" ]; then echo "Got password but shouldn't have."; killServer 1; fi
        pass
        mv data/hostapd-psk- data/hostapd-psk
    fi

    if [ "$GAPRESET" = "1" ]
    then
        unset IDS
    fi

    unset NOTAG
    unset NOFILENAME
    unset NCOUT
fi

if [ "$SAP" != "" ]
then
    [ $VERBOSE != 0 ] && msg "SAP: save access point configuration"

    NOSIZE=1
    NOTAG=1
    NOFILENAME=1
    NCOUT=1

    # Format of payload:
    # SSID
    # Channel
    # Password
    # Base Address

    # Valid fields
    rm -f data/hostapd.conf.bak
    expected_ssid="MIKEH"
    expected_addr="192.168.122.1"
    expected_pw="mypw000"
    expected_ch="9"
    RAWDATA="${expected_ssid}:${expected_ch}:${expected_pw}:${expected_addr}"
    genMsg 00000803 
    sendIt
    sleep 1
    if [ ! -f "data/hostapd.conf.bak" ]; then fail "Missing hostapd.conf.bak."; killServer 1; fi
    if [ ! -f "data/hostapd-psk" ]; then fail "Missing hostapd-psk."; killServer 1; fi
    if [ ! -f "data/interfaces.bak" ]; then fail "Missing interfaces.bak."; killServer 1; fi
    ssid="$(grep -i "^ssid=" data/hostapd.conf.bak| cut -f2 -d"=")"
    channel="$(grep -i "^channel=" data/hostapd.conf.bak| cut -f2 -d"=")"
    addr="$(grep -A1 "^iface wlan0" data/interfaces.bak | tail -1 | sed 's/.*address //')"
    pw="$(cat data/hostapd-psk | cut -f2 -d" ")"
    if [ "${ssid}"    != "${expected_ssid}" ]; then fail "Wrong ssid saved to hostapd.conf.bak."; killServer 1; fi
    if [ "${channel}" != "${expected_ch}"   ]; then fail "Wrong channel saved to hostapd.conf.bak."; killServer 1; fi
    if [ "${addr}"    != "${expected_addr}" ]; then fail "Wrong addr saved to interfaces.bak."; killServer 1; fi
    if [ "${pw}"      != "${expected_pw}"   ]; then fail "Wrong ssid saved to hostapd-psk."; killServer 1; fi
    pass

    # Cleanup
    rm -f data/hostapd.conf.bak
    rm -f data/interfaces.bak

    unset NOSIZE
    unset NOTAG
    unset NOFILENAME
    unset RAWDATA
    unset NCOUT
fi

if [ "$MULTICAST" != "" ]
then
    [ $VERBOSE != 0 ] && msg "MULTICAST: Test multicast message acceptance"

    if [ "$IDS" = "" ]
    then
        IDS="1 2"
        MULTICASTRESET=1
    fi

    NOSIZE=1
    NOTAG=1
    NOFILENAME=1
    NCOUT=1
    MCFILE=/tmp/mc.$$

    # IoT Registration
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        genRegistration $MCFILE
        runWebServer 202 $MCFILE
        # genMsg 0000efcf 
        genMsg 00000101 
        sendMulticast
        sleep 1
        killWebServer
        rm -f $MCFILE
        logged "Received 202 status code: UUID accepted"
        if [ $? -eq 1 ]; then fail "Failed to process multicast IoT registration."; killServer 1; fi
        pass
    fi

    # Jarvis registration
    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        runWebServer 200
        genMsg 00000201 
        sendMulticast
        sleep 1
        killWebServer
        logged "Sent ACK to Jarvis"
        if [ $? -eq 1 ]; then fail "Failed to process multicast Jarvis registration."; killServer 1; fi
        pass
    fi

    if [ "$MULTICASTRESET" = "1" ]
    then
        unset IDS
    fi

    # Cleanup from test
    unset NOSIZE
    unset NOTAG
    unset NOFILENAME
    unset NCOUT
    sleep 2
fi

#--------------------------------------------------------------
# Reset the daemon for another round of tests.
#--------------------------------------------------------------
if [ "$RESET" != "" ]
then
    # Disable any active webcam stream
    reset
fi

# General cleanup
rm -f servertest.dat $DATAFILE

# echo "LOGFILE = $LOGFILE"
# ls -l $LOGFILE

# Kill the server
if [ $KEEP -eq 0 ]
then
    killServer -1
    checkCore
fi

echo "Done."
