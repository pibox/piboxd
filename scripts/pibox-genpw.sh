#!/bin/bash
# Generate an md5 password for digest authentication.
#--------------------------------------------------------------

#--------------------------------------------------------------
# Init
#--------------------------------------------------------------
USER=
PW=
REALM=
OUTPUT=

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------

# Provide command line usage assistance
doHelp()
{
    echo ""
    echo "$0 -u user -p pw -r realm -o file"
    echo "where"
    echo "-u user     Username for the password."
    echo "-p pw       Password for the user."
    echo "-r realm    Realm for the digest"
    echo "-o file     Output file"
    echo ""
}

log()
{
    logger -t "$0" "$*"
}

die()
{
    log "$*"
    exit 1
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":u:p:r:o:" Option
do
    case $Option in
    u) USER=${OPTARG};;
    p) PW=${OPTARG};;
    r) REALM=${OPTARG};;
    o) OUTPUT=${OPTARG};;
    *) doHelp; exit 0;;
    esac
done

[[ -n "${USER}"   ]] || die "User not specified."
[[ -n "${PW}"     ]] || die "Password not specified."
[[ -n "${REALM}"  ]] || die "Realm not specified."
[[ -n "${OUTPUT}" ]] || die "Output file not specified."

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------

# Remove user from file, if it exists and if user is already in it.
[[ -e "${OUTPUT}" ]] && sed -i '/'${USER}'/d' "${OUTPUT}"
printf "%s:%s:%s\n" "${USER}" "${REALM}" "$(printf "%s" "${USER}:${REALM}:${PW}" | md5sum | awk '{print $1}')" >> ${OUTPUT}
[[ -f "${OUTPUT}" ]] || die "Failed to create ${OUTPUT}"
exit 0
