## Synopsis

Piboxd is a daemon application that handles long running or privileged operations for PiBox applications.  It includes support for the following features.

1. Protocol Requests and Responses
1. Stream Processing
1. SMB automounting
1. Network Management
1. IoT Registration
1. IoT Messaging

### Protocol Requests and Responses

Piboxd implements a byte-oriented message protocol.  

![PiBox Message Format](/docs/images/PacketFormat-large.png "PiBox Message Format")

PiBox uses a very simplistic packet protocol.  A 4-byte header holds an 8 bit type fields with possible actions and subactions (the other 8 bits are for future expansion).  Subactions are associated with specific actions and actions are associated with specific types. Types are context defined.

Following the header is a 32bit payload size field.  This is the length of the payload that follows.  A payload is not required so this field can be zero.  

The tag and filename fields are only used in some contexts, specifically with requests for web cam video.  The tag is a 36 character UUID using the printf format %08x-%04x-%04x-%04x-%012x.  The filename is always "webcam" to request the webcam be started.  Other contexts ignore these as separate fields and simply use them as a combined payload field.

The payload is a byte-field that is payload-size length.  Packet handlers read in this much data or time out waiting for the data.  Once the packet is read it is stuffed into an appropriate data structure and passed to the type and action handlers.

#### Message Types and Actions

Specific message types and actions are listed in the [Message Flow](https://www.graphics-muse.org/wiki/pmwiki.php/RaspberryPi/MessageFlow) document on the wiki.

### Stream Processing

Piboxd can be used to start webcam streaming.  The webcam is run via an mjpeg-streamer process that runs in the background and streams data on port 9090.  There are low-resolution and high-resolution options for the webcam.  The default for the Web UI is to run in low resolution with an option to switch to high resolution.  The default for the console based webcam viewer is to run in high resolution mode.

### SMB Mounts

Piboxd supports mounting SMB servers automatically.  A thread is run every 10 seconds to query for available servers. If found, they are mounted under /media/smb.  SMB mounts are used by the PiBox Media System to make video databases available to network nodes.

### Network Management

Piboxd is dynamically linked against the PiBox Network Config library (libpnc).  This allows piboxd to proxy network management requests using the available protocols.  This includes configuring wired and wireless devices for DHCP or static addressing and routing and configurating of an access point.

### IoT Registration

Piboxd supports registration requests from IoT devices.  Registration involves generation of a custom encryption key that is used by both ends of the communiction for all messages after the initial registration request.

### IoT Messaging

Piboxd supports IoT device messaging, which allows state information to be sent in both directions.  On the PiBox side the state information is stored in a flat file database for use by display tools.  On the IoT side state information can be used to query or update the state of the device.

## Usage

Piboxd can log to a file using the -v and -l options.

Piboxd can be configured using /etc/piboxd.cfg.  The configuration file provides options for setting the commands to use for various features along with logging options.  Some features can be disabled using the configuration file, reducing the overhead of piboxd platforms that do not need those features.  Configuration file examples are available in the data directory.

## Build

Piboxd can be built and tested on a local Linux box using autoconf.  A cross compiled version can be built using the cross.sh wrapper script.

### Local Build and Test

To build locally for testing, use the following commands.

    autoreconf -i
    ./configure
    make
    src/piboxd -T -v3

The last command will run piboxd in test mode and use verbosity level three for debugging.  

For more information on command line options use the following command.

    src/piboxd -?

### Cross compile and packaging

To cross compile the application use the cross.sh script.  To get a simple usage statement for this script use the following command.

    ./cross.sh -?

Use of the cross compile script requires specifying three components.

1. The path to the cross compiler toolchain directory.
1. The path to the PiBox root file system staging directory.
1. The path to the opkg tools on the host system.

The first two components can be found under the respective build trees of the PiBox Development Platform build.  They may also be distributed as part of the packaged PiBox Development Platform.  Either way, the argument for these options is a directory path.

The last component requires installation of the opkg tools on the host.  These can be built from the PiBox Development Platform using the following target.

    make opkg
    make opkg-install

Additional information for building can be found in the INSTALL file.

## Installation

Piboxd is packaged in an opkg format.  After cross compiling look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/piboxd_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/piboxd_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

